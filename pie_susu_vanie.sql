/*
 Navicat Premium Data Transfer

 Source Server         : local
 Source Server Type    : MySQL
 Source Server Version : 50624
 Source Host           : 127.0.0.1:3306
 Source Schema         : pie_susu_vanie

 Target Server Type    : MySQL
 Target Server Version : 50624
 File Encoding         : 65001

 Date: 06/02/2018 18:10:55
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for akun
-- ----------------------------
DROP TABLE IF EXISTS `akun`;
CREATE TABLE `akun`  (
  `kode_rekening` varchar(10) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL DEFAULT '',
  `nama_rekening` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `klasifikasi` char(1) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  PRIMARY KEY (`kode_rekening`) USING BTREE,
  INDEX `fk_akun_akun_klasifikasi_1`(`klasifikasi`) USING BTREE
) ENGINE = MyISAM CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of akun
-- ----------------------------
INSERT INTO `akun` VALUES ('1.1', 'Kas', 'A');
INSERT INTO `akun` VALUES ('3.1', 'Modal', 'C');
INSERT INTO `akun` VALUES ('4.1', 'Pendapatan Usaha', 'D');
INSERT INTO `akun` VALUES ('5.1', 'Beban Gaji', 'F');
INSERT INTO `akun` VALUES ('5.2', 'Beban Bahan Baku', 'F');
INSERT INTO `akun` VALUES ('5.3', 'Beban Listrik', 'F');
INSERT INTO `akun` VALUES ('5.4', 'Beban Air', 'F');

-- ----------------------------
-- Table structure for akun_klasifikasi
-- ----------------------------
DROP TABLE IF EXISTS `akun_klasifikasi`;
CREATE TABLE `akun_klasifikasi`  (
  `klasifikasi` char(1) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `deskripsi` varchar(25) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `normal` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `inisial` smallint(6) NOT NULL,
  PRIMARY KEY (`klasifikasi`) USING BTREE,
  INDEX `_WA_Sys_DebitCredit_07C12930`(`normal`) USING BTREE,
  INDEX `_WA_Sys_CodeInitial_07C12930`(`inisial`) USING BTREE,
  INDEX `_WA_Sys_Description_07C12930`(`deskripsi`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of akun_klasifikasi
-- ----------------------------
INSERT INTO `akun_klasifikasi` VALUES ('A', 'Aktiva', 'Debit', 1);
INSERT INTO `akun_klasifikasi` VALUES ('B', 'Kewajiban', 'Kredit', 2);
INSERT INTO `akun_klasifikasi` VALUES ('C', 'Modal', 'Kredit', 3);
INSERT INTO `akun_klasifikasi` VALUES ('D', 'Pendapatan', 'Kredit', 4);
INSERT INTO `akun_klasifikasi` VALUES ('F', 'Biaya', 'Debit', 6);

-- ----------------------------
-- Table structure for auto_number
-- ----------------------------
DROP TABLE IF EXISTS `auto_number`;
CREATE TABLE `auto_number`  (
  `group` varchar(32) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `number` int(11) DEFAULT NULL,
  `optimistic_lock` int(11) DEFAULT NULL,
  `update_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`group`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of auto_number
-- ----------------------------
INSERT INTO `auto_number` VALUES ('55a313327d8e6bb6008aa83ee9d7e266', 1, NULL, 1517911256);

-- ----------------------------
-- Table structure for cart
-- ----------------------------
DROP TABLE IF EXISTS `cart`;
CREATE TABLE `cart`  (
  `sessionId` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `cartData` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  PRIMARY KEY (`sessionId`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for detail_order
-- ----------------------------
DROP TABLE IF EXISTS `detail_order`;
CREATE TABLE `detail_order`  (
  `id_detail_cart` int(11) NOT NULL AUTO_INCREMENT,
  `id_order` varchar(10) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `id_produk` varchar(10) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `harga` decimal(15, 2) DEFAULT NULL,
  `jumlah` decimal(15, 2) DEFAULT NULL,
  PRIMARY KEY (`id_detail_cart`) USING BTREE,
  INDEX `fk_detail_order_order_1`(`id_order`) USING BTREE,
  INDEX `fk_detail_order_produk_1`(`id_produk`) USING BTREE,
  CONSTRAINT `fk_detail_order_order_1` FOREIGN KEY (`id_order`) REFERENCES `order` (`id_order`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `fk_detail_order_produk_1` FOREIGN KEY (`id_produk`) REFERENCES `produk` (`id_produk`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for image_slider
-- ----------------------------
DROP TABLE IF EXISTS `image_slider`;
CREATE TABLE `image_slider`  (
  `id_slider` int(11) NOT NULL AUTO_INCREMENT,
  `image` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  PRIMARY KEY (`id_slider`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of image_slider
-- ----------------------------
INSERT INTO `image_slider` VALUES (1, '5a7556189ec17.png');
INSERT INTO `image_slider` VALUES (2, '5a7555ff41c5a.jpg');
INSERT INTO `image_slider` VALUES (3, '5a75583c31648.jpg');

-- ----------------------------
-- Table structure for jurnal
-- ----------------------------
DROP TABLE IF EXISTS `jurnal`;
CREATE TABLE `jurnal`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_transaksi` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL DEFAULT '0',
  `keterangan` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `tanggal` date DEFAULT NULL,
  `images` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `FK_JOURNAL_BUKTI`(`id_transaksi`) USING BTREE,
  CONSTRAINT `jurnal_ibfk_1` FOREIGN KEY (`id_transaksi`) REFERENCES `transaksi` (`id_transaksi`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for jurnal_detail
-- ----------------------------
DROP TABLE IF EXISTS `jurnal_detail`;
CREATE TABLE `jurnal_detail`  (
  `id_jurnal` int(11) DEFAULT NULL,
  `kode_akun` varchar(10) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `debit` decimal(15, 2) DEFAULT NULL,
  `credit` decimal(15, 2) DEFAULT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `fk_jurnal_detail_jurnal_1`(`id_jurnal`) USING BTREE,
  INDEX `fk_jurnal_detail_akun_1`(`kode_akun`) USING BTREE,
  CONSTRAINT `fk_jurnal_detail_akun_1` FOREIGN KEY (`kode_akun`) REFERENCES `akun` (`kode_rekening`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `jurnal_detail_ibfk_1` FOREIGN KEY (`id_jurnal`) REFERENCES `jurnal` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for kategori_produk
-- ----------------------------
DROP TABLE IF EXISTS `kategori_produk`;
CREATE TABLE `kategori_produk`  (
  `id_kategori` varchar(10) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `nama_kategori` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  PRIMARY KEY (`id_kategori`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of kategori_produk
-- ----------------------------
INSERT INTO `kategori_produk` VALUES ('D', 'Donut');
INSERT INTO `kategori_produk` VALUES ('PS', 'Pie Susu');

-- ----------------------------
-- Table structure for migration
-- ----------------------------
DROP TABLE IF EXISTS `migration`;
CREATE TABLE `migration`  (
  `version` varchar(180) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `apply_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`version`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of migration
-- ----------------------------
INSERT INTO `migration` VALUES ('m000000_000000_base', 1517841564);
INSERT INTO `migration` VALUES ('m160516_095943_init', 1517841571);
INSERT INTO `migration` VALUES ('m161109_124936_rename_cart_table', 1517841572);
INSERT INTO `migration` VALUES ('m161119_153348_alter_cart_data', 1517841574);

-- ----------------------------
-- Table structure for order
-- ----------------------------
DROP TABLE IF EXISTS `order`;
CREATE TABLE `order`  (
  `id_order` varchar(10) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `id_pelanggan` varchar(10) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `tgl_order` date DEFAULT NULL,
  `total_harga` decimal(15, 2) DEFAULT NULL,
  `bukti_pembayaran` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `no_rekening` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `bank` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `metode_pembayaran` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `nama_akun` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `status_pembayaran` int(1) DEFAULT NULL,
  `validasi` int(1) DEFAULT NULL,
  `tanggal_validasi` date DEFAULT NULL,
  `user_validasi` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_order`) USING BTREE,
  INDEX `fk_order_pelanggan_1`(`id_pelanggan`) USING BTREE,
  INDEX `fk_order_user_1`(`user_validasi`) USING BTREE,
  CONSTRAINT `fk_order_pelanggan_1` FOREIGN KEY (`id_pelanggan`) REFERENCES `pelanggan` (`id_pelanggan`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `fk_order_user_1` FOREIGN KEY (`user_validasi`) REFERENCES `user` (`id_user`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for pelanggan
-- ----------------------------
DROP TABLE IF EXISTS `pelanggan`;
CREATE TABLE `pelanggan`  (
  `id_pelanggan` varchar(10) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `nama` varchar(25) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `alamat` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `no_telp` varchar(12) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `email` varchar(25) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `images` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `username` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `password` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  PRIMARY KEY (`id_pelanggan`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of pelanggan
-- ----------------------------
INSERT INTO `pelanggan` VALUES ('PLGN000001', NULL, 'askhadbkadb akdjbad ', '2345678765', 'asada@sdsd.sd', NULL, 'test', '$2y$13$OwHFaEWN06JUzquE1s2yo.rQgT2VXPZglsCPxnUTLoHigCI4G/9yO');

-- ----------------------------
-- Table structure for produk
-- ----------------------------
DROP TABLE IF EXISTS `produk`;
CREATE TABLE `produk`  (
  `id_produk` varchar(10) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `id_kategori` varchar(10) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `nama` varchar(25) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `harga` decimal(15, 2) DEFAULT NULL,
  `size` tinyint(1) DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 1,
  `images` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `deskripsi` text CHARACTER SET latin1 COLLATE latin1_swedish_ci,
  PRIMARY KEY (`id_produk`) USING BTREE,
  INDEX `fk_produk_kategori_produk_1`(`id_kategori`) USING BTREE,
  CONSTRAINT `fk_produk_kategori_produk_1` FOREIGN KEY (`id_kategori`) REFERENCES `kategori_produk` (`id_kategori`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of produk
-- ----------------------------
INSERT INTO `produk` VALUES ('Aliquam.', 'D', 'Tianna Schoen', 2871560.00, 1, 1, NULL, 'ddfdfdsf sdf sdfs df dsf dsf ds');
INSERT INTO `produk` VALUES ('Corrupti.', 'D', 'Aditya Runolfsdottir', 994.00, 2, 1, NULL, 'ddfdfdsf sdf sdfs df dsf dsf ds');
INSERT INTO `produk` VALUES ('Delectus.', 'D', 'Cloyd Klocko', 10392.00, 1, 1, NULL, 'ddfdfdsf sdf sdfs df dsf dsf ds');
INSERT INTO `produk` VALUES ('Deleniti.', 'D', 'Candida Bahringer', 6651148.00, 2, 1, NULL, 'ddfdfdsf sdf sdfs df dsf dsf ds');
INSERT INTO `produk` VALUES ('Deserunt.', 'D', 'Mrs. Patsy Keeling', 729311.00, 2, 1, NULL, 'ddfdfdsf sdf sdfs df dsf dsf ds');
INSERT INTO `produk` VALUES ('Et ut.', 'D', 'Savanah Gleason I', 6.00, 2, 1, NULL, 'ddfdfdsf sdf sdfs df dsf dsf ds');
INSERT INTO `produk` VALUES ('Ex.', 'D', 'Brittany Corwin', 48620.00, 2, 1, NULL, 'ddfdfdsf sdf sdfs df dsf dsf ds');
INSERT INTO `produk` VALUES ('Minima.', 'D', 'Shirley Jacobson', 0.00, 2, 1, NULL, 'ddfdfdsf sdf sdfs df dsf dsf ds');
INSERT INTO `produk` VALUES ('Officiis.', 'D', 'Destini Koch', 353304605.00, 2, 1, NULL, 'ddfdfdsf sdf sdfs df dsf dsf ds');
INSERT INTO `produk` VALUES ('Quaerat.', 'D', 'Helena Hoeger Jr.', 839353.00, 2, 1, NULL, 'ddfdfdsf sdf sdfs df dsf dsf ds');
INSERT INTO `produk` VALUES ('Qui eaque.', 'D', 'Mr. Darrell Terry III', 6422.00, 2, 1, NULL, 'ddfdfdsf sdf sdfs df dsf dsf ds');
INSERT INTO `produk` VALUES ('Qui illum.', 'D', 'Mrs. Liza Okuneva DVM', 0.00, 2, 1, NULL, 'ddfdfdsf sdf sdfs df dsf dsf ds');
INSERT INTO `produk` VALUES ('Quibusdam.', 'D', 'Amy Wuckert Sr.', 776772.00, 2, 1, NULL, 'ddfdfdsf sdf sdfs df dsf dsf ds');
INSERT INTO `produk` VALUES ('Quo odio.', 'D', 'Jalen Balistreri', 272894591.00, 2, 1, NULL, 'ddfdfdsf sdf sdfs df dsf dsf ds');
INSERT INTO `produk` VALUES ('Rerum aut.', 'D', 'Birdie Lowe', 48030.00, 2, 1, NULL, 'ddfdfdsf sdf sdfs df dsf dsf ds');
INSERT INTO `produk` VALUES ('Saepe.', 'D', 'Mrs. Elyse Senger', 73268.00, 2, 1, NULL, 'ddfdfdsf sdf sdfs df dsf dsf ds');
INSERT INTO `produk` VALUES ('STRAW001', 'PS', 'Pie Susu Strawbery', 18000.00, 1, 1, '5a75611923b26.jpg', 'Pie Susu dengan rasa strawbery');
INSERT INTO `produk` VALUES ('Suscipit.', 'D', 'Prof. Niko Kunde', 2.00, 2, 1, NULL, 'ddfdfdsf sdf sdfs df dsf dsf ds');
INSERT INTO `produk` VALUES ('Velit.', 'D', 'Gudrun Kuhn', 27178164.00, 2, 1, NULL, 'ddfdfdsf sdf sdfs df dsf dsf ds');

-- ----------------------------
-- Table structure for saran
-- ----------------------------
DROP TABLE IF EXISTS `saran`;
CREATE TABLE `saran`  (
  `id_saran` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `mobile_phone` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `tanggal` date DEFAULT NULL,
  `email` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `saran` text CHARACTER SET latin1 COLLATE latin1_swedish_ci,
  PRIMARY KEY (`id_saran`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of saran
-- ----------------------------
INSERT INTO `saran` VALUES (1, 'Anom', '083121212', '2018-02-01', 'test@test.com', 'Adadad dADD adad DAD daDA daD');

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user`  (
  `id_user` int(11) NOT NULL AUTO_INCREMENT,
  `nama_user` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `username` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `password` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `akses` varchar(10) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  PRIMARY KEY (`id_user`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO `user` VALUES (1, 'admin', 'admin', 'admin', 'admin');
INSERT INTO `user` VALUES (2, 'pimpinan', 'pimpinan', 'pimpinan', 'pimpinan');

SET FOREIGN_KEY_CHECKS = 1;
