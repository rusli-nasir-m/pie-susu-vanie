<?php
/**
 * Created by PhpStorm.
 * User: AcenetDev
 * Date: 2/1/2018
 * Time: 4:51 PM
 */

namespace frontend\controllers;


use common\models\KategoriProduk;
use common\models\Pelanggan;
use common\models\Produk;
use yii\web\Controller;
use Faker;

class DummyController extends Controller
{

    /**
     * Function actionIndex for
     * Usage: actionIndex()
     **/
    public function actionIndex($row = 10,$iterate =1)
    {
        $start = microtime(true);
        $faker = Faker\Factory::create();
        $datas = [];
        for($j=1;$j<=$iterate;$j++){
            for($i=1;$i<=$row;$i++){
                $pelanggan = new Pelanggan();
                $kategoriProduk = new KategoriProduk();
                $produk = new Produk();

                $pelanggan->id_pelanggan =$faker->text(10);
                $pelanggan->nama = $faker->name();
                $pelanggan->email = $faker->email;
                $pelanggan->alamat = $faker->address;
                $pelanggan->no_telp = $faker->randomNumber();
                $pelanggan->username = $faker->userName;
                $pelanggan->password = $faker->password;
                $pelanggan->save();

                $produk->id_produk  = $faker->text(10);
                $produk->nama = $faker->name;
                $produk->harga = $faker->randomNumber();
                $produk->deskripsi = $faker->randomLetter;
                $produk->save();
            }
        }

        $time_elapsed_us = microtime(true) - $start;
        echo ($row*$iterate).' = '.$time_elapsed_us.' <br>';
    }
    
}