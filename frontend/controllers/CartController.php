<?php
/**
 * Created by PhpStorm.
 * User: AcenetDev
 * Date: 1/31/2018
 * Time: 1:33 AM
 */

namespace frontend\controllers;


use common\models\DetailOrder;
use common\models\DxProduk;
use common\models\Order;
use common\models\Produk;
use common\models\ProdukOld;
//use common\widgets\shoppingCart\Cart;
use common\widgets\hcCart\Cart;
use common\cclass\Cart as CartData;
use common\widgets\shoppingCart\models\CartItemInterface;
use common\widgets\shoppingCart\widgets\CartGrid;
use frontend\models\CheckOrderForm;
use Yii;
use yii\base\Model;
use yii\bootstrap\Html;
use yii\data\ArrayDataProvider;
use yii\db\QueryBuilder;
use yii\di\Instance;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\Session;

class CartController extends Controller
{

    public function actionIndex()
    {
        $dataProvider = new ArrayDataProvider([
            'allModels' => Yii::$app->cart->getPositions(),
        ]);

        return $this->render('cart', [
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionAdd($id, $quantity = 1)
    {
        $model = $this->findModel($id);

        Yii::$app->cart->put($model, $quantity);
		Yii::$app->session->setFlash('success', $model->nama . ' Berhasil ditambahkan kedalam keranjang belanja.');
        //return $this->redirect(['index']);
        return $this->redirect(Yii::$app->request->referrer);
    }

    public function actionRemove($id)
    {
        $model = $this->findModel($id);

        Yii::$app->cart->remove($model);

        return $this->redirect(['index']);
    }

    public function actionClear()
    {
        Yii::$app->cart->removeAll();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Product model based on its slug.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Product the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = DxProduk::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException(Yii::t('frontend', 'The requested page does not exist.'));
        }
    }

    /**
     * Function actionUpdate for
     * Usage: actionUpdate()
     **/
    public function actionUpdate($id, $quantity)
    {
        $product = Produk::findOne($id);
        if ($product) {
            \Yii::$app->cart->update($product, $quantity);
            $this->redirect(['index']);
        }
    }

    /**
     * Function actionDelete for
     * Usage: actionDelete()
     **/
    public function actionDelete($id)
    {
        $product = Produk::findOne($id);
        if ($product) {
            \Yii::$app->cart->delete($product);
            $this->redirect(['index']);
        }
    }

    /**
     * Function  for
     * Usage: ()
     **/
    public function actionCheckOut()
    {

        $cartPositions = Yii::$app->cart->getPositions();

        if (!$cartPositions or $cartPositions === null or Yii::$app->user->isGuest) {
            Yii::$app->session->setFlash('error','Maaf anda harus registrasi/login terlebih dahulu untuk melanjutkan proses');

            return $this->redirect(['index']);
        }
        $user = Yii::$app->user->getIdentity();
        $model = new Order();
        $model->nama = $user->nama;
        $model->alamat = $user->alamat;
        $model->no_telp = $user->no_telp;
        $model->kota = $user->kota;
        $model->provinsi = $user->provinsi;
        $model->kode_pos = $user->kode_pos;
        $model->ongkir = param('biaya_pengiriman');

        $dataProvider = new ArrayDataProvider([
            'allModels' => $cartPositions,
        ]);

        return $this->render('check-out', [
            'model' => $model,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Public function actionProsesOrder() For
     **/
    public function actionProsesOrder()
    {
        $cartPositions = Yii::$app->cart->getPositions();

        if (!$cartPositions or $cartPositions === null or Yii::$app->user->isGuest && !Yii::$app->request->isPost) {
            Yii::$app->session->setFlash('error','Maaf anda harus registrasi/login terlebih dahulu untuk melanjutkan proses');
            return $this->redirect(['index']);
        }

        $dataProvider = new ArrayDataProvider([
            'allModels' => $cartPositions,
        ]);
        $transaction = Yii::$app->db->beginTransaction();
        $model = new Order();
        $model->load(Yii::$app->request->post());
//        if ($model->load(Yii::$app->request->post())) {
        $model->total_harga = Yii::$app->cart->getCost();
        $model->tgl_order = date('Y-m-d H:i');
        $model->id_pelanggan = Yii::$app->user->id;

        if ($model->validate() && $orderSave = $model->save()) {
            $detailOrderArr = [];
            foreach ($cartPositions as $row){
                $detailOrderArr[] = [
                    $model->id_order,$row->id_produk,$row->getPrice(),$row->getQuantity()
                ];
            }
            $db = Yii::$app->db;
            $builder = new QueryBuilder($db);
            $batch = $builder->batchInsert(DetailOrder::tableName(),[
                'id_order','id_produk','harga','jumlah'],$detailOrderArr);

            if($orderSave && $db->createCommand($batch)->execute()){
                $transaction->commit();
                Yii::$app->cart->removeAll();
            }else{

                $transaction->rollBack();
                goto roolback;
            }
            return $this->render('orderSuccess', [
                'model' => $model,
            ]);
        }else{
            Yii::$app->session->addFlash('error-order',$model->getErrors());
        }
//        }
        roolback:
        $this->redirect(['check-out']);
    }



    /**
     * Function  for
     * Usage: ()
     **/
    public function actionPayment()
    {
        return $this->render('payment');
    }

    /**
     * Function  for
     * Usage: ()
     **/
    public function actionKonfirmasiPembayaran($id = null)
    {
        $model = new Order();
        if($id){
            $model = Order::findOne($id);
        }

        $model->setScenario('update');
        $post = Yii::$app->request->post();
        if ($post){
            $model->load($post);
            if(!$id){
                $model2 = Order::findOne($model->id_order);
                $model2->setScenario('update');
                $model2->load($post);
                $model2->status_pembayaran = 1;
                $model2->tgl_pembayaran = date('Y-m-d h:i:s');
                if($model2->save()){
                    goto suucessKonf;
                }else{
                    echo Html::errorSummary($model2);
                };
            }else{
                $model->status_pembayaran = 1;
                $model->tgl_pembayaran = date('Y-m-d h:i:s');
                if ($model->save()){

                }else{
                    echo Html::errorSummary($model);
                }
suucessKonf:
                return $this->render('konfirmasiBerhasil',[
                    'model' => $model,
                ]);
            }
        }

        return $this->render('konfirmasi',[
            'model' => $model,
        ]);
    }

    /**
     * Public function actionCheckOrder() For
     **/
    public function actionCheckOrder()
    {
        $model = new CheckOrderForm();
        if ($model->load(Yii::$app->request->post())) {
            $model->checkOrder();
        }

        return $this->render('check-order', [
            'dataProvider' => $model->getOrder(),
            'model' => $model,
        ]);
    }

    /**
     * Public function actionDetail($id) For
     **/
    public function actionDetail($id)
    {
        $model = Order::findOne($id);

        return $this->render('detail',[
            'model' => $model
        ]);
    }

}