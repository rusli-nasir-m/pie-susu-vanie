<?php
/**
 * Created by PhpStorm.
 * User: ASUS
 * Date: 16/04/2018
 * Time: 12:09
 * Project: pie-susu-vanie
 * @var $this \yii\web\View
 * @var $model \frontend\models\UserFront
 */

use yii\bootstrap\ActiveForm;
use yii\bootstrap\Html;

$this->title = 'Profile';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="site-signup">
    <div class="text-center">
        <h1><?= Html::encode($this->title) ?></h1>
    </div>
    <div class="row">
        <div class="col-lg-6 col-lg-offset-3">
            <?php $form = ActiveForm::begin(['id' => 'form-signup']); ?>
            <?= $form->field($model, 'nama')->textInput(['autofocus' => true]) ?>
            <?= $form->field($model, 'alamat')->textarea() ?>
            <?= $form->field($model, 'no_telp') ?>
            <?= $form->field($model, 'email') ?>
            <?= $form->field($model, 'kota') ?>
            <?= $form->field($model, 'provinsi') ?>
            <?= $form->field($model, 'kode_pos') ?>
            <?= $form->field($model, 'username') ?>
            <?= $form->field($model, 'password')->passwordInput() ?>


            <div class="form-group text-center">
                <?= Html::submitButton('Update', ['class' => 'btn btn-success', 'name' => 'signup-button']) ?>
            </div>
            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>
