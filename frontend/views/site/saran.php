<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\Saran */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;

$this->title = 'Saran';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-contact">
    <h1><?= Html::encode($this->title) ?></h1>
    <div class="row">
        <div class="col-md-12">
            <?= \yii\widgets\ListView::widget([
                'dataProvider' => $list,
                'pager' => false,
                'layout' => "{items}",
                'itemView' => function($model){
                    /**
                     * @var $model \common\models\Saran
                     */
                    $nama = $model->nama;
                    $mobile_phone = $model->mobile_phone;
                    $tanggal = $model->tanggal;
                    $email = $model->email;
                    $saran = $model->saran;

                    return "
                        <table style='border-bottom:2px solid #7d7d7d;'>
                            <tr>
                                <td>
                                    <strong>$nama</strong><br>
                                    <p>$saran</p>
                                </td>                                
                            </tr>
                        </table>
                    ";
                }
            ])?>
        </div>
    </div>
    <br>
    <hr>
    <div class="row">
        <div class="col-lg-5">
            <?php $form = ActiveForm::begin(['id' => 'contact-form']); ?>

                <?= $form->field($model, 'nama')->textInput(['autofocus' => true]) ?>
                <?= $form->field($model, 'mobile_phone') ?>
                <?= $form->field($model, 'email') ?>
                <?= $form->field($model, 'saran')->textarea(['rows' => 6]) ?>
                <?= $form->field($model, 'tanggal')->hiddenInput()->label(false) ?>

                <?= ''//$form->field($model, 'verifyCode')->widget(Captcha::className(), [
                    //'template' => '<div class="row"><div class="col-lg-3">{image}</div><div class="col-lg-6">{input}</div></div>',
                //]) ?>

                <div class="form-group">
                    <?= Html::submitButton('Submit', ['class' => 'btn btn-primary', 'name' => 'contact-button']) ?>
                </div>

            <?php ActiveForm::end(); ?>
        </div>
    </div>

</div>
