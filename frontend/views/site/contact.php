<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\ContactForm */

use yii\helpers\Html;

$this->title = 'Contact';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row">
    <div class="col-md-12">
        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3944.4227270427696!2d115.19816441428455!3d-8.651283790393348!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2dd240a803c539fd%3A0x75cff1802e23ece6!2sPie+Susu+VANIE!5e0!3m2!1sid!2sid!4v1519451879707" width="100%" height="600" frameborder="0" style="border:0" allowfullscreen></iframe>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <h1>PIE SUSU VANIE</h1>
        <table>
            <tr>
                <td>Alamat</td>
                <td>Jl. Gunung Agung Gg Yamuna No. 15a, Denpasar, Bali - Indonesia 80118</td>
            </tr>
            <tr>
                <td>Whats App</td>
                <td>087860903016</td>
            </tr>
            <tr>
                <td>BBM</td>
                <td>DOA5FADD</td>
            </tr>
            <tr>
                <td>Line</td>
                <td>piesusuvanie</td>
            </tr>
            <tr>
                <td>Instagram</td>
                <td>piesusuvanie</td>
            </tr>

        </table>
    </div>
</div>
