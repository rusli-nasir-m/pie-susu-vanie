<?php

/* @var $this yii\web\View */

use common\widgets\ListProduk;
use yii\widgets\ListView;
use common\widgets\Alert;

$this->title = param('app_name');
?>
<div class="row">
    <!--Middle Part Start-->
    <div id="content" class="col-xs-12">
        <!-- Slideshow Start-->
        <div class="slideshow single-slider owl-carousel">
            <?php
                $slider = \common\models\ImageSlider::find()->all();
                if($slider){
                    foreach($slider as $slide){
                        ?>
                        <div class="item"> <a href="#"><img class="img-responsive" src="<?= $slide->getUploadUrl('image')?>" /></a> </div>
                        <?php
                    }
                }
            ?>
        </div>
        <!-- Slideshow End-->
        <div class="product-filter">
            <div class="row">
                <div class="col-md-4 col-sm-5">
                    <div class="btn-group">
                        <button type="button" id="list-view" class="btn btn-default" data-toggle="tooltip" title="List"><i class="fa fa-th-list"></i></button>
                        <button type="button" id="grid-view" class="btn btn-default" data-toggle="tooltip" title="Grid"><i class="fa fa-th"></i></button>
                    </div>

                </div>
            </div>
        </div>
		<?= Alert::widget();?>
        <br />
        <?= ListView::widget([
            'id' => 'list_produk',
//            'pjax' => true,
//            'pjaxSettings' => [
//                'neverTimeout' =>true,
//            ],
            'options' => [
                'tag' => 'div',
            ],
            'dataProvider' => $produkProvider,
            'itemView' => function ($model, $key, $index, $widget) {
                $itemContent = $this->render('_list_produk',['model' => $model]);

                return $itemContent;
            },
            'itemOptions' => [
                'tag' => false,
            ],
            'layout' => "<div class='row products-category'>
                <div class='row'>                    
                    <div class='col-sm-6 text-left'>{pager}</div>
                    <div class='col-sm-6 text-right'>{summary}</div>
                </div>
                \n{items}\n
            </div>",
//            'summary' => '',
            'pager' => [
                'firstPageLabel' => '<<',
                'nextPageLabel' =>'>',
                'prevPageLabel' =>'<',
                'lastPageLabel' =>'>>',
                'maxButtonCount' => 4,
                'options' => [
                    'class' => 'pagination pagination-sm'
                ]
            ],
        ])?>
    </div>
    <!--Middle Part End-->
</div>