<?php
/**
 * Created by PhpStorm.
 * User: AcenetDev
 * Date: 2/3/2018
 * Time: 12:54 PM
 * @var $model \common\models\Produk
 */

use yii\helpers\StringHelper;

$imageThumb = $model->getThumbUploadUrl('images');
$imageReal = $model->getUploadUrl('images');
?>
<div class="product-layout product-list col-xs-12">
    <div class="product-thumb">
        <?= a(img($imageThumb,['style'=>'width:250px']),['/product/view','id' => $model->id_produk])?>
        <div>
            <div class="caption">
                <h4><?= a($model->nama,['/product/view','id' => $model->id_produk])?></h4>
                <p class="description"><?= StringHelper::truncateWords($model->deskripsi,100)?></p>
                <p class="price"><?= formatRupiah($model->harga)?></p>
                <p><?= \yii\helpers\ArrayHelper::getValue(param('size'),$model->size)?></p>
            </div>
            <div class="button-group">
                <?= a("<span>Add to Cart</span>",['/cart/add','id' => $model->id_produk],[
                    'class' => 'btn btn-primary'
                ])?>
            </div>
        </div>
    </div>
</div>
