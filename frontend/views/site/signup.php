<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\SignupForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Registrasi';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-signup">
    <div class="text-center">
        <h1><?= Html::encode($this->title) ?></h1>
    </div>
    <div class="row">
        <div class="col-lg-6 col-lg-offset-3">
            <?php $form = ActiveForm::begin(['id' => 'form-signup']); ?>
            <?= $form->field($model, 'nama')->textInput(['autofocus' => true]) ?>
            <?= $form->field($model, 'alamat')->textarea() ?>
            <?= $form->field($model, 'no_telp') ?>
            <?= $form->field($model, 'email') ?>
            <?= $form->field($model, 'kota') ?>
            <?= $form->field($model, 'provinsi') ?>
            <?= $form->field($model, 'kode_pos') ?>
            <?= $form->field($model, 'username') ?>
            <?= $form->field($model, 'password')->passwordInput() ?>


            <div class="form-group text-center">
                <?= Html::submitButton('Register', ['class' => 'btn btn-success', 'name' => 'signup-button']) ?>
                <?= Html::resetButton('Reset', ['class' => 'btn btn-warning', 'name' => 'signup-button']) ?>
            </div>
            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>
