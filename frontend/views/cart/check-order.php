<?php
/**
 * Created by PhpStorm.
 * User: ASUS
 * Date: 10/04/2018
 * Time: 22:08
 * Project: pie-susu-vanie
 *
 * @var $this \yii\web\View
 * @var $model \frontend\models\CheckOrderForm
 * @var $modelResult \frontend\models\CheckOrderForm
 */

use yii\bootstrap\Html;
use yii\grid\GridView;
use yii\helpers\Url;

$this->title = 'Cek Order';
?>

<div class="row">
    <!--Middle Part Start-->
    <div id="content" class="col-sm-12">
        <h1 class="title">Pesanan Saya</h1>
        <?php $form = \yii\bootstrap\ActiveForm::begin()?>
        <div class="row">
            <div class="col-sm-4 col-sm-offset-8">
                <?= $form->field($model,'no_order')?>
                <div class="form-group">
                    <?= Html::submitButton(Yii::t('app', 'Check'), ['class' => 'btn btn-success']) ?>
                </div>
            </div>
        </div>
        <?php \yii\bootstrap\ActiveForm::end()?>
        <?php
        if($dataProvider){
            ?>
            <div class="table-responsive">
                <?= GridView::widget([
                    'dataProvider' => $dataProvider,
                    'layout' => '{items}',
                    'columns' => [
                        'id_order',
                        'tgl_order:date:Tanggal Order',
                        [
                            'attribute' => 'status_pembayaran',
                            'label' => 'Status Pembayaran',
                            'value' => function($model){
                                return ($model->status_pembayaran)? 'Berhasil':'Belum Konfirmasi';
//                                $validasi = ($model->validasi)? 'Sudah Validasi':'Belum Validasi';
                            }
                        ],
                        [
                            'attribute' => 'metode_pembayaran',
                            'value' => function($model){
                                return \yii\helpers\ArrayHelper::getValue(param('metode_pembayaran'),$model->metode_pembayaran);
                            }
                        ],
                        [
                            'attribute' => 'validasi',
                            'label' => 'Status Validasi',
                            'value' => function($model){
                                return $validasi = ($model->validasi)? 'Sudah Validasi':'Belum Validasi';
                            }
                        ],[
                            'label' => 'Detail',
                            'format' => 'raw',
                            'value' => function($model){
                                return a('Detail',['/cart/detail','id'=> $model->id_order]);
                            }
                        ]

                    ],
                ])?>
            </div>
            <?php
        }else{
            echo Html::tag('h3','Silahkan cek pesanan anda');
        }
        ?>


        <div class="buttons">
            <div class="pull-left"><a href="<?= Url::to(['/product'])?>" class="btn btn-default">Kembali Belanja</a></div>
        </div>
    </div>
    <!--Middle Part End -->
</div>
