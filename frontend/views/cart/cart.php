<?php
/**
 * Created by PhpStorm.
 * User: AcenetDev
 * Date: 1/31/2018
 * Time: 1:35 AM
 *
 * @var $this \yii\web\View
 */

use yii\bootstrap\Html;
use yii\grid\GridView;
use yii\helpers\Url;

$this->title = 'Keranjang Belanja';
//use common\widgets\shoppingCart\ShoppingCart;
?>
<div class="row">
    <!--Middle Part Start-->
    <div id="content" class="col-sm-12">
        <h1 class="title">Keranjang Belanja</h1>
        <?php
        echo \common\widgets\Alert::widget([]);

        if(Yii::$app->cart->getCount()){
            ?>
            <div class="table-responsive">
                <?= GridView::widget([
                    'dataProvider' => $dataProvider,
                    'layout' => '{items}',
                    'columns' => [
                        [
                            'attribute' => 'images',
                            'label' => Yii::t('app', 'Foto'),
                            'format' => 'html',
                            'value' => function ($model) {
                                return img($model->getThumbUploadUrl('images'),['width' => 75, 'class' =>'img-thumbnail']);
                            },
                            'contentOptions' => [
                                'style' => 'width: 15%',
                                'class'=>"text-center"
                            ],
                        ],
                        [
                            'attribute' => 'nama',
                            'label' => Yii::t('app', 'Nama Produk'),
                            'format' => 'html',
                            'value' => function ($model) {
                                return a($model->nama, ['/product/view', 'id' => $model->id_produk]);
                            },
                        ],
                        [
                            'attribute' => 'id_kategori',
                            'label' => Yii::t('app', 'Kategori'),
                            'format' => 'html',
                            'value' => function ($model) {
                                return a($model->kategori->nama_kategori, ['product/kategori', 'id' => $model->id_kategori]);
                            },
                        ],
                        [
                            'format' => 'raw',
                            'attribute' => 'quantity',
                            'label' => 'Quantity',
                            'value' => function ($model) {
                                $htmlInput = "<div class=\"input-group btn-block quantity\">
                            <input type=\"number\" name=\"quantity\" value=\"".$model->getQuantity()."\" class=\"form-control\" />
                            <span class=\"input-group-btn\">
                                <button type=\"button\" data-toggle=\"tooltip\" title=\"Update\" class=\"btn btn-primary btnUpdateCart\"><i class=\"fa fa-refresh\"></i></button>
                                ".a("<i class=\"fa fa-times-circle\"></i>",['cart/remove', 'id' => $model->id_produk],[
                                        'class'=>"btn btn-danger"
                                    ])."                                
                            </span>
                        </div>";
                                return $htmlInput;
                            },
                        ],[
                            'attribute' => 'price',
                            'format' => 'currency',
                            'label' => Yii::t('app', 'Harga'),
                        ],
                        [
                            'label' => 'Sub Total',
                            'format' => 'currency',
                            'value' => function ($model) {
                                return $model->getCost();
                            }
                        ]
                    ],
                ])?>
            </div>
            <div class="row">
                <div class="col-sm-4 col-sm-offset-8">
                    <table class="table table-bordered">
                        <tr>
                            <td class="text-right"><strong>Bruto:</strong></td>
                            <td class="text-right"><?= Yii::$app->formatter->asCurrency(Yii::$app->cart->getCost()) ?></td>
                        </tr>
                        <tr>
                            <td class="text-right"><strong>Biaya Pengiriman</strong></td>
                            <td class="text-right"><?= formatter()->asCurrency(param('biaya_pengiriman'))?></td>
                        </tr>
                        <tr>
                            <td class="text-right"><strong>Total:</strong></td>
                            <td class="text-right"><?= Yii::$app->formatter->asCurrency((param('biaya_pengiriman') + Yii::$app->cart->getCost())) ?></td>
                        </tr>
                    </table>
                </div>
            </div>
            <?php
        }else{
            echo Html::tag('h3','Maaf anda tidak memiliki produk di keranjang belanja');
        }
        ?>


        <div class="buttons">
            <div class="pull-left"><a href="<?= Url::to(['/product'])?>" class="btn btn-default">Kembali Belanja</a></div>
            <div class="pull-right"><a href="<?= Url::to(['/cart/check-out'])?>" class="btn btn-primary">Proses Pesanan</a></div>
        </div>
    </div>
    <!--Middle Part End -->
</div>
    
<?php
$updateLink = \yii\helpers\Url::to(['update']);
$js = <<<JS
$('.btnUpdateCart').on('click',function() {
    var tr = $(this).parents('tr');
    var input = $(this).parents('td').find('input');
    var dataKey = tr.data('key');
    var dataValue = input.val();
    window.location = '$updateLink'+ '?id=' + dataKey + '&quantity=' + dataValue;
});
JS;

$this->registerJs($js);

?>