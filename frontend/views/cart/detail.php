<?php
/**
 * Created by PhpStorm.
 * User: ASUS
 * Date: 11/04/2018
 * Time: 7:35
 * Project: pie-susu-vanie
 *
 * @var $this \yii\web\View
 * @var $model \common\models\Order
 */

use yii\bootstrap\Html;
use yii\helpers\Url;

$this->title = 'Detail Pesanan | ' . $model->id_order;
$dataProvider = new \yii\data\ActiveDataProvider([
    'query' => $model->getDetailOrders(),
    'sort' => false,
    'pagination' => false
])
?>

<div class="row">
    <h1 class="text-center">Detail Order</h1>
    <div class="col-md-6">
        <?= \yii\widgets\DetailView::widget([
            'model' => $model,
            'options' => ['class' => 'table'],
            'template' => "<tr><th width='150px'>{label}</th><td>{value}</td></tr>",
            'attributes' => [
                'id_order:text:No Order',
                'tgl_order:date:Tanggal Order',
                'pelanggan.nama:text:Nama Pelanggan',
                [
                    'attribute' => 'total_harga',
                    'format' => 'currency',
                    'label' => 'Total Harga + Biaya Kirim',
                    'value' => function($model){
                        return $model->total_harga + $model->ongkir;
                    }
                ],
            ]
        ])?>
    </div>
    <div class="col-md-6">
        <?= \yii\widgets\DetailView::widget([
            'model' => $model,
            'options' => ['class' => 'table'],
            'template' => "<tr><th width='150px'>{label}</th><td>{value}</td></tr>",
            'attributes' => [
                [
                    'attribute' => 'status_pembayaran',
                    'value' => function($model){
                        return ($model->status_pembayaran)? 'Berhasil':'Belum Konfirmasi';
                    }
                ],[
                    'attribute' => 'metode_pembayaran',
                    'value' => function($model){
                        return \yii\helpers\ArrayHelper::getValue(param('metode_pembayaran'),$model->metode_pembayaran);
                    }
                ],[
                    'attribute' => 'validasi',
                    'label' => 'Status Validasi',
                    'value' => function($model){
                        return $validasi = ($model->validasi)? 'Sudah Validasi':'Belum Validasi';
                    }
                ],
            ]
        ])?>
    </div>
</div>
<div class="row">
    <div class="cil-md-12">
        <h3>Daftar Pesanan</h3>
        <?= \yii\grid\GridView::widget([
            'dataProvider' => $dataProvider,
            'layout' => '{items}',
            'columns' => [
                'produk.nama:text:Nama Produk',
                'harga:currency',
                'jumlah:decimal:Qty',
                [
                    'label' => 'Total',
                    'format' => 'currency',
                    'value' => function($model){
                        return $model->harga * $model->jumlah;
                    }
                ]
            ]
        ])?>
        <div class="row">
            <div class="col-sm-4 col-sm-offset-8">
                <table class="table table-bordered">
                    <tr>
                        <td class="text-right"><strong>Sub Total:</strong></td>
                        <td class="text-right" id="txtsub_total"><?= Yii::$app->formatter->asCurrency($model->total_harga) ?></td>
                    </tr>
                    <tr>
                        <td class="text-right"><strong>Biaya Pengiriman</strong></td>
                        <td class="text-right" id="txtongkir"><?= formatter()->asCurrency($model->ongkir)?></td>
                    </tr>
                    <tr>
                        <td class="text-right"><strong>Total:</strong></td>
                        <td class="text-right" id="txttotal"><?= Yii::$app->formatter->asCurrency($model->ongkir + $model->total_harga ) ?></td>
                    </tr>
                </table>
            </div>
        </div>
        <div class="buttons">
            <div class="pull-left"><a href="<?= Url::to(['/product'])?>" class="btn btn-default">Kembali Belanja</a></div>
        </div>
    </div>
</div>
