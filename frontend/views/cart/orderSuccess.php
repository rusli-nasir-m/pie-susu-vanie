<?php
/**
 * Created by PhpStorm.
 * User: ASUS
 * Date: 10/04/2018
 * Time: 23:22
 * Project: pie-susu-vanie
 *
 * @var $this \yii\web\View
 * @var $model \common\models\Order
 */

use yii\grid\GridView;

$this->title = 'Order Sukses';
$dataProvider = new \yii\data\ActiveDataProvider([
    'query' => $model->getDetailOrders(),
    'pagination' => false,
    'sort' => false
]);
?>
<div class="row">
    <div class="col-md-12">
        <h2 class="text-center">TERIMAKASIH SUDAH BERBELANJA</h2>
        <h1 class="text-center">PIE SUSU VANIE</h1>
        <p>Pesanan anda telah berhasil di proses. Silahkan konfirmasi pembayaran anda. <?= a('Konfirmasi',['/cart/konfirmasi-pembayaran','id'=> $model->id_order])?></p>
        <h2> Silahkan melakukan pembayaran dengan cara transfer ke rekening BCA: 669035126 Atas dewik ambarwati </h2>
		<h1>No Order: #<?= $model->id_order?></h1>
        <div class="col-md-6">
            <?= \yii\widgets\DetailView::widget([
                'model' => $model,
                'options' => [
                    ['class' => 'table detail-view']
                ],
                'template' => '<tr><th width="130px">{label}</th><td>{value}</td></tr>',
                'attributes' => [
                    'nama',
                    'alamat',
                    'no_telp',

                ]
            ])?>
        </div>
        <div class="col-md-6">
            <?= \yii\widgets\DetailView::widget([
                'model' => $model,
                'options' => [
                    ['class' => 'table detail-view']
                ],
                'template' => '<tr><th width="130px">{label}</th><td>{value}</td></tr>',
                'attributes' => [
                    'kota',
                    'provinsi',
                    'kode_pos',

                ]
            ])?>
        </div>
    </div>
    <div class="col-md-12">
        <h3>Daftar pesanan anda</h3>
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'columns' => [
                'produk.nama:text:Nama Produk',
                'jumlah:decimal',
                'harga:currency: Harga Satuan',
                [
                    'label' => 'Sub Total',
                    'format' => 'currency',
                    'value' => function($model){
                        return $model->harga * $model->jumlah;
                    }
                ]
            ]
        ])?>
    </div>
</div>
<div class="row">
    <div class="col-sm-4 col-sm-offset-8">
        <table class="table table-bordered">
            <tr>
                <td class="text-right"><strong>Bruto:</strong></td>
                <td class="text-right"><?= Yii::$app->formatter->asCurrency($model->total_harga - param('biaya_pengiriman')) ?></td>
            </tr>
            <tr>
                <td class="text-right"><strong>Biaya Pengiriman</strong></td>
                <td class="text-right"><?= formatter()->asCurrency(param('biaya_pengiriman'))?></td>
            </tr>
            <tr>
                <td class="text-right"><strong>Total:</strong></td>
                <td class="text-right"><?= Yii::$app->formatter->asCurrency($model->total_harga) ?></td>
            </tr>
        </table>
    </div>
</div>