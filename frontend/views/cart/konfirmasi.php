<?php
/**
 * Created by PhpStorm.
 * User: AcenetDev
 * Date: 1/31/2018
 * Time: 1:53 AM
 * @var $model \common\models\Order
 * @var $this \yii\web\View
 */


use yii\bootstrap\Html;

$this->title = 'Konfirmasi Pembayaran';
$form = \yii\bootstrap\ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]);
?>
<h1>Konfirmasi Pembayaran</h1>
<h2> Silahkan melakukan pembayaran dengan cara transfer ke rekening BCA: 669035126 Atas dewik ambarwati </h2>
<div class="row">
    <div class="col-md-6">
        <?= $form->field($model,'id_order')->dropDownList(\yii\helpers\ArrayHelper::map(\common\models\Order::find()
            ->where([
                'id_pelanggan' => Yii::$app->user->id,
                'bukti_pembayaran' => null
            ])->all(),'id_order','id_order'))?>
        <?= $form->field($model,'metode_pembayaran')->dropDownList(param('metode_pembayaran'))?>
        <?= $form->field($model,'bukti_pembayaran')->fileInput()?>
        <?= $form->field($model,'nama_akun')->textInput()?>
        <?= $form->field($model,'no_rekening')->textInput()?>
        <?= $form->field($model,'bank')->textInput()?>
        <div class="form-group">
            <?= Html::submitButton(Yii::t('app', 'Kirim'), ['class' => 'btn btn-success']) ?>
        </div>
    </div>
</div>
<?php \yii\bootstrap\ActiveForm::end()?>