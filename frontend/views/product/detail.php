<?php
/**
 * Created by PhpStorm.
 * User: AcenetDev
 * Date: 1/31/2018
 * Time: 1:24 AM
 */
?>

<div class="row">
    <!--Middle Part Start-->
    <div id="content" class="col-md-12">
        <div itemscope itemtype="http://schema.org/Product">
            <h1 class="title" itemprop="name">Laptop Silver black</h1>
            <div class="row product-info">
                <div class="col-sm-6">
                    <div class="image"><img class="img-responsive" itemprop="image" id="zoom_01" src="<?= getstorageUrl('image/')?>product/macbook_air_1-350x525.jpg" title="Laptop Silver black" alt="Laptop Silver black" data-zoom-image="<?= getstorageUrl('image/')?>product/macbook_air_1-600x900.jpg" /> </div>


                </div>
                <div class="col-sm-6">
                    <ul class="list-unstyled description">
                        <li><b>Brand:</b> <a href="#"><span itemprop="brand">Apple</span></a></li>
                        <li><b>Product Code:</b> <span itemprop="mpn">Product 17</span></li>
                        <li><b>Reward Points:</b> 700</li>
                        <li><b>Availability:</b> <span class="instock">In Stock</span></li>
                    </ul>
                    <ul class="price-box">
                        <li class="price" itemprop="offers" itemscope itemtype="http://schema.org/Offer"><span class="price-old">$1,202.00</span> <span itemprop="price">$1,142.00<span itemprop="availability" content="In Stock"></span></span></li>
                        <li></li>
                        <li>Ex Tax: $950.00</li>
                    </ul>
                    <div id="product">
                        <div class="cart">
                            <div>
                                <div class="qty">
                                    <label class="control-label" for="input-quantity">Qty</label>
                                    <input type="text" name="quantity" value="1" size="2" id="input-quantity" class="form-control" />
                                    <a class="qtyBtn plus" href="javascript:void(0);">+</a><br />
                                    <a class="qtyBtn mines" href="javascript:void(0);">-</a>
                                    <div class="clear"></div>
                                </div>
                                <button type="button" id="button-cart" class="btn btn-primary btn-lg">Add to Cart</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--Middle Part End -->
</div>
    
