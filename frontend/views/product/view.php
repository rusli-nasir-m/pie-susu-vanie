<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Produk */

$this->title = $model->id_produk;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Produks'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
$imageLogo = $model->getThumbUploadUrl('images');
?>
<div class="row">
    <!--Middle Part Start-->
    <div id="content" class="col-md-12">
        <div itemscope itemtype="http://schema.org/Product">
            <div class="row product-info">
                <div class="col-sm-6">
                    <div class="image">
                        <img class="img-responsive" itemprop="image" id="zoom_01" src="<?= $imageLogo?>" title="<?$model->nama?>" alt="<?$model->nama?>" data-zoom-image="<?= $model->getThumbUploadUrl('images')?>" />
                    </div>
                </div>
                <div class="col-sm-6">
                    <ul class="list-unstyled description">
                        <li><h1><?= $model->nama?></h1></li>
                        <li><b>Katergori:</b> <span itemprop="mpn"><?= $model->kategori->nama_kategori?></span></li>
                        <li><b>Size:</b> <?= \yii\helpers\ArrayHelper::getValue(param('size'),$model->size)?></li>
                        <li><b>Status:</b> <?= ($model->status)? "<span class=\"instock\">In Stock</span>":"<span class=\"instock\">Out of Stock</span>"?></li>
                        <li><b>Deskripsi:</b> <p><?= $model->deskripsi?></p></li>
                    </ul>
                    <ul class="price-box">
                        <li class="price" itemprop="offers" itemscope itemtype="http://schema.org/Offer">
                            <span itemprop="price"><?= formatRupiah($model->harga)?></span>
                        </li>
                    </ul>
                    <div id="product">
                        <div class="cart">
                            <div>
                                <?php $form = \yii\bootstrap\ActiveForm::begin([
                                    'action' => ['/cart/add'],
                                    'method' => 'get'
                                ])?>
                                    <div class="qty">
                                        <label class="control-label" for="input-quantity">Qty</label>
                                        <input type="text" name="quantity" value="1" size="2" id="input-quantity" class="form-control" />
                                        <?= Html::hiddenInput('id',$model->id_produk)?>
                                        <a class="qtyBtn plus" href="javascript:void(0);">+</a><br />
                                        <a class="qtyBtn mines" href="javascript:void(0);">-</a>
                                        <div class="clear"></div>
                                    </div>
                                    <button type="submit" id="button-cart" class="btn btn-primary btn-lg">Add to Cart</button>
                                <?php \yii\bootstrap\ActiveForm::end()?>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--Middle Part End -->
</div>


