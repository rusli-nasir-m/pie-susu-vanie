<?php
/**
 * Created by PhpStorm.
 * User: AcenetDev
 * Date: 12/13/2017
 * Time: 9:03 AM
 */

/* @var $this \yii\web\View */
/* @var $content string */

use yii\bootstrap\Html;
use yii\data\ArrayDataProvider;
use yii\grid\GridView;
use yii\helpers\Url;

//$this->title = Yii::$app->name;

$mainAsset = \common\assets\marketshop\MarketShopAsset::register($this);
$baseAssetUrl = \yii\helpers\Url::to($mainAsset->baseUrl,true);

$css = <<<CSS
.cart-mini #cart .heading {
    text-decoration: none;
    font-size: 14px;
    height: 32px;
    color: #444;
    display: inline-block;
    position: relative;
    padding: 0;
    background: none;
    border: none;
}

.cart-mini #cart .heading .cart-icon {
    background: #08b0ff;
}

.cart-mini #cart {
    position: relative;
    text-align: right;
}

.cart-mini #cart .heading span {
    display: inline-block;
    line-height: 32px;
}

CSS;

//$this->registerCss($css);

?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<!--[if IE 9]>         <html class="ie9 no-focus" lang="en"> <![endif]-->
<!--[if gt IE 9]><!--> <html class="no-focus" lang="en"> <!--<![endif]-->

<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <title><?= Html::encode($this->title) ?></title>
    <?= Html::csrfMetaTags() ?>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>
<div class="wrapper-wide">
    <div id="header">
        <!-- Top Bar Start-->
        <nav id="top" class="htop">
            <div class="container">
                <div class="row">
                    <span class="drop-icon visible-sm visible-xs"><i class="fa fa-align-justify"></i></span>
                    <div class="pull-left flip left-top">
                        <div class="social flip">
                            <?= a(img(getstorageUrl('image/socialicons/facebook.png')),param('fb_url','https://www.facebook.com/piesusuvanie/'))?>
                            <?= a(img(getstorageUrl('image/socialicons/instagram.png')),param('instagram_url','https://www.instagram.com/piesusuvanie/'))?>
                        </div>
                    </div>
                    <div class="flip left-top">
                        <div class="links">
                            <ul>
                                <li><?= a('Home',['/'])?></li>
                                <li><?= a('Produk',['/product'])?></li>
                                <li><?= a('About Us',['/site/about'])?></li>
                                <li><?= a('Contact Us',['/site/contact'])?></li>
                                <li><?= a('Saran',['/site/saran'])?></li>
                                <?php
                                if (Yii::$app->user->isGuest){
                                    echo '';
                                }else{
                                    ?>
                                    <li><?= a('Konfirmasi Pembayaran',['/cart/konfirmasi-pembayaran'])?></li>
                                    <li><?= a('Cek Order',['/cart/check-order'])?></li>
                                    <?php
                                }
                                ?>
                            </ul>
                        </div>
                    </div>
                    <div id="top-links" class="nav pull-right flip">
                        <ul>
                            <li>
                                <div id="search" class="input-group">
                                    <input id="filter_name" type="text" name="search" value="" placeholder="Search" class="form-control input-lg" />
                                    <button type="button" class="button-search"><i class="fa fa-search"></i></button>
                                </div>
                            </li>
                            <?php
                            if (Yii::$app->user->isGuest) {
                                ?>
                                <li><?= a('Login',['/site/login'])?></li>
                                <li><?= a('Regiter',['/site/signup'])?></li>
                                <?php
                            } else {
                                ?>
                                <li><?= a('Profile',['/site/profile'])?></li>
                                <?php
                                echo '<li>'
                                    . Html::beginForm(['/site/logout'], 'post')
                                    . Html::submitButton(
                                        'Logout (' . Yii::$app->user->identity->username . ')',
                                        ['class' => 'btn btn-link logout']
                                    )
                                    . Html::endForm()
                                    . '</li>';
                            }
                            ?>
                        </ul>
                    </div>
                </div>
            </div>
        </nav>
        <!-- Top Bar End-->
        <!-- Header Start-->
        <header class="header-row">
            <div class="container">
                <div class="table-container">
                    <!-- Logo Start -->
                    <div class="col-table-cell col-lg-6 col-md-6 col-sm-12 col-xs-12 inner">
                        <div id="logo">
                            <?= a(Html::tag('h3',param('app_name')),['/'],['style' => 'color:white'])?>
                        </div>
                    </div>
                    <!-- Logo End -->

                </div>
            </div>
        </header>
        <!-- Header End-->
    </div>
    <div id="container">
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-md-offset-6">
                    <div class="cart-mini pull-right">
                        <div id="cart">

                            <button type="button" data-toggle="dropdown" data-loading-text="Loading..." class="heading dropdown-toggle btn btn-link">
                                <span class="">Pesanan saya: </span>
                                <span class="fa fa-shopping-cart fa-2x flip"></span>
                                <span id="cart-total"><?= getCartTotal()?></span>
                            </button>
                            <ul class="dropdown-menu">
                                <li>
                                    <?php $cartDataProvider = new ArrayDataProvider([
                                        'allModels' => Yii::$app->cart->getPositions(),
                                    ]);
                                    echo GridView::widget([
                                        'dataProvider' => $cartDataProvider,
                                        'tableOptions' => ['class'=>'table table-condensed'],
                                        'layout' => '{items}',
                                        'showHeader' => false,
                                        'columns' => [
                                            [
                                                'attribute' => 'images',
                                                'label' => Yii::t('app', 'Foto'),
                                                'format' => 'html',
                                                'value' => function ($model) {
                                                    return img($model->getThumbUploadUrl('images'),['width' => 75, 'class' =>'img-thumbnail']);
                                                },
                                                'contentOptions' => [
                                                    'style' => 'width: 15%',
                                                    'class'=>"text-center"
                                                ],
                                            ],[
                                                'attribute' => 'nama',
                                                'label' => Yii::t('app', 'Nama Produk'),
                                                'format' => 'html',
                                                'value' => function ($model) {
                                                    return a($model->nama, ['/product/view', 'id' => $model->id_produk]) . '<br>' . $model->kategori->nama_kategori;
                                                },
                                            ],[
                                                'attribute' => 'quantity',
                                                'value' => function($model){
                                                    return ($model->quantity)? 'x ' . $model->quantity:0;
                                                }
                                            ],[
                                                'attribute' => 'price',
                                                'format' => 'currency',
                                                'label' => Yii::t('app', 'Harga'),
                                            ],[
                                                'format' => 'raw',
                                                'value' => function($model){
                                                    return a('<i class="fa fa-times"></i>',['remove','id' => $model->id_produk],['class'=> 'btn btn-danger btn-xs remove']);
                                                }
                                            ]
                                        ],
                                    ])?>
                                </li>
                                <li>
                                    <div>
                                        <table class="table table-bordered">
                                            <tbody>
                                            <tr>
                                                <td class="text-right"><strong>Biaya Pengiriman</strong></td>
                                                <td class="text-right"><?= formatter()->asCurrency((Yii::$app->cart->getCost()? param('biaya_pengiriman'):0))?></td>
                                            </tr>
                                            <tr>
                                                <td class="text-right"><strong>Total</strong></td>
                                                <td class="text-right"><?= Yii::$app->formatter->asCurrency((Yii::$app->cart->getCost()? param('biaya_pengiriman') + Yii::$app->cart->getCost():0)) ?></td>
                                            </tr>
                                            </tbody>
                                        </table>
                                        <p class="checkout"><a href="<?= Url::to(['/cart/index'])?>" class="btn btn-primary"><i class="fa fa-shopping-cart"></i> View Cart</a>&nbsp;&nbsp;&nbsp;<a href="<?= Url::to(['/cart/check-out'])?>" class="btn btn-primary"><i class="fa fa-share"></i> Checkout</a></p>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <?= $content?>
        </div>
    </div>

    <!--Footer Start-->
    <footer id="footer">
        <div class="fpart-first">
            <div class="container">
                <div id="powered" class="clearfix">
                    <div class="powered_text pull-left flip">
                        <p><?= param('app_name')?> © <?= date('Y')?></p>
                    </div>
                </div>
            </div>
        </div>
        <div id="back-top"><a data-toggle="tooltip" title="Back to Top" href="javascript:void(0)" class="backtotop"><i class="fa fa-chevron-up"></i></a></div>
    </footer>
    <!--Footer End-->
</div>

<?php $this->endBody() ?>

<?php
yii\bootstrap\Modal::begin([
    'headerOptions' => ['id' => 'modalHeader'],
    'id' => 'global-modal',
    'size' => 'modal-lg',
    //keeps from closing modal with esc key or by clicking out of the modal.
    // user must click cancel or X to close
//        'clientOptions' => ['backdrop' => 'static', 'keyboard' => FALSE]
]);
echo "<div id='modalContent'>
<div style=\"text-align:center\"><i class=\"fa fa-spinner fa-spin fa-3x fa-fw\"></i>
<span class=\"sr-only\">Loading...</span></div>
</div>";
yii\bootstrap\Modal::end();
?>
</body>
</html>
<?php $this->endPage() ?>
