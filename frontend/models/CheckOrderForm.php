<?php
/**
 * Created by PhpStorm.
 * User: ASUS
 * Date: 10/04/2018
 * Time: 21:55
 * Project: pie-susu-vanie
 */

namespace frontend\models;


use common\models\Order;
use yii\base\Model;
use yii\data\ActiveDataProvider;

class CheckOrderForm extends Model
{
    public $no_order;
    private $result = [];

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['no_order', 'string'],
        ];
    }

    /**
     * Public function checkOrder($no_order) For
     **/
    public function checkOrder()
    {
        $model = Order::find()->where(['id_order' => $this->no_order]);
        $dataProvider = new ActiveDataProvider([
            'query' => $model,
            'pagination' =>false,
            'sort' => false
        ]);
        $this->result = $dataProvider;
        return $this->result;
    }

    /**
     * Public function getOrder() For
     **/
    public function getOrder()
    {
        return $this->result;
    }

}