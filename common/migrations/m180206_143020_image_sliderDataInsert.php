<?php

use yii\db\Schema;
use yii\db\Migration;

class m180206_143020_image_sliderDataInsert extends Migration
{

    public function init()
    {
        $this->db = 'db';
        parent::init();
    }

    public function safeUp()
    {
        Yii::$app->db->createCommand("SET foreign_key_checks = 0")->execute();
        $this->batchInsert('{{%image_slider}}',
                           ["id_slider", "image"],
                            [
    [
        'id_slider' => '1',
        'image' => '5a7556189ec17.png',
    ],
    [
        'id_slider' => '2',
        'image' => '5a7555ff41c5a.jpg',
    ],
    [
        'id_slider' => '3',
        'image' => '5a75583c31648.jpg',
    ],
]
        );
        Yii::$app->db->createCommand("SET foreign_key_checks = 1")->execute();
    }

    public function safeDown()
    {
        //$this->truncateTable('{{%image_slider}} CASCADE');
    }
}
