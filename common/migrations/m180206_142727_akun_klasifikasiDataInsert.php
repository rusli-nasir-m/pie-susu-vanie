<?php

use yii\db\Schema;
use yii\db\Migration;

class m180206_142727_akun_klasifikasiDataInsert extends Migration
{

    public function init()
    {
        $this->db = 'db';
        parent::init();
    }

    public function safeUp()
    {
        Yii::$app->db->createCommand("SET foreign_key_checks = 0")->execute();
        $this->batchInsert('{{%akun_klasifikasi}}',
                           ["klasifikasi", "deskripsi", "normal", "inisial"],
                            [
    [
        'klasifikasi' => 'A',
        'deskripsi' => 'Aktiva',
        'normal' => 'Debit',
        'inisial' => '1',
    ],
    [
        'klasifikasi' => 'B',
        'deskripsi' => 'Kewajiban',
        'normal' => 'Kredit',
        'inisial' => '2',
    ],
    [
        'klasifikasi' => 'C',
        'deskripsi' => 'Modal',
        'normal' => 'Kredit',
        'inisial' => '3',
    ],
    [
        'klasifikasi' => 'D',
        'deskripsi' => 'Pendapatan',
        'normal' => 'Kredit',
        'inisial' => '4',
    ],
    [
        'klasifikasi' => 'F',
        'deskripsi' => 'Biaya',
        'normal' => 'Debit',
        'inisial' => '6',
    ],
]
        );
        Yii::$app->db->createCommand("SET foreign_key_checks = 1")->execute();
    }

    public function safeDown()
    {
        //$this->truncateTable('{{%akun_klasifikasi}} CASCADE');
    }
}
