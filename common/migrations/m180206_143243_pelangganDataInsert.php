<?php

use yii\db\Schema;
use yii\db\Migration;

class m180206_143243_pelangganDataInsert extends Migration
{

    public function init()
    {
        $this->db = 'db';
        parent::init();
    }

    public function safeUp()
    {
        Yii::$app->db->createCommand("SET foreign_key_checks = 0")->execute();
        $this->batchInsert('{{%pelanggan}}',
                           ["id_pelanggan", "nama", "alamat", "no_telp", "email", "images", "username", "password"],
                            [
    [
        'id_pelanggan' => 'PLGN000001',
        'nama' => null,
        'alamat' => 'askhadbkadb akdjbad ',
        'no_telp' => '2345678765',
        'email' => 'asada@sdsd.sd',
        'images' => null,
        'username' => 'test',
        'password' => '$2y$13$OwHFaEWN06JUzquE1s2yo.rQgT2VXPZglsCPxnUTLoHigCI4G/9yO',
    ],
]
        );
        Yii::$app->db->createCommand("SET foreign_key_checks = 1")->execute();
    }

    public function safeDown()
    {
        //$this->truncateTable('{{%pelanggan}} CASCADE');
    }
}
