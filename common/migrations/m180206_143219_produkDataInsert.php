<?php

use yii\db\Schema;
use yii\db\Migration;

class m180206_143219_produkDataInsert extends Migration
{

    public function init()
    {
        $this->db = 'db';
        parent::init();
    }

    public function safeUp()
    {
        Yii::$app->db->createCommand("SET foreign_key_checks = 0")->execute();
        $this->batchInsert('{{%produk}}',
                           ["id_produk", "id_kategori", "nama", "harga", "size", "status", "images", "deskripsi"],
                            [
    [
        'id_produk' => 'Aliquam.',
        'id_kategori' => 'D',
        'nama' => 'Tianna Schoen',
        'harga' => '2871560.00',
        'size' => '1',
        'status' => '1',
        'images' => null,
        'deskripsi' => 'ddfdfdsf sdf sdfs df dsf dsf ds',
    ],
    [
        'id_produk' => 'Corrupti.',
        'id_kategori' => 'D',
        'nama' => 'Aditya Runolfsdottir',
        'harga' => '994.00',
        'size' => '2',
        'status' => '1',
        'images' => null,
        'deskripsi' => 'ddfdfdsf sdf sdfs df dsf dsf ds',
    ],
    [
        'id_produk' => 'Delectus.',
        'id_kategori' => 'D',
        'nama' => 'Cloyd Klocko',
        'harga' => '10392.00',
        'size' => '1',
        'status' => '1',
        'images' => null,
        'deskripsi' => 'ddfdfdsf sdf sdfs df dsf dsf ds',
    ],
    [
        'id_produk' => 'Deleniti.',
        'id_kategori' => 'D',
        'nama' => 'Candida Bahringer',
        'harga' => '6651148.00',
        'size' => '2',
        'status' => '1',
        'images' => null,
        'deskripsi' => 'ddfdfdsf sdf sdfs df dsf dsf ds',
    ],
    [
        'id_produk' => 'Deserunt.',
        'id_kategori' => 'D',
        'nama' => 'Mrs. Patsy Keeling',
        'harga' => '729311.00',
        'size' => '2',
        'status' => '1',
        'images' => null,
        'deskripsi' => 'ddfdfdsf sdf sdfs df dsf dsf ds',
    ],
    [
        'id_produk' => 'Et ut.',
        'id_kategori' => 'D',
        'nama' => 'Savanah Gleason I',
        'harga' => '6.00',
        'size' => '2',
        'status' => '1',
        'images' => null,
        'deskripsi' => 'ddfdfdsf sdf sdfs df dsf dsf ds',
    ],
    [
        'id_produk' => 'Ex.',
        'id_kategori' => 'D',
        'nama' => 'Brittany Corwin',
        'harga' => '48620.00',
        'size' => '2',
        'status' => '1',
        'images' => null,
        'deskripsi' => 'ddfdfdsf sdf sdfs df dsf dsf ds',
    ],
    [
        'id_produk' => 'Minima.',
        'id_kategori' => 'D',
        'nama' => 'Shirley Jacobson',
        'harga' => '0.00',
        'size' => '2',
        'status' => '1',
        'images' => null,
        'deskripsi' => 'ddfdfdsf sdf sdfs df dsf dsf ds',
    ],
    [
        'id_produk' => 'Officiis.',
        'id_kategori' => 'D',
        'nama' => 'Destini Koch',
        'harga' => '353304605.00',
        'size' => '2',
        'status' => '1',
        'images' => null,
        'deskripsi' => 'ddfdfdsf sdf sdfs df dsf dsf ds',
    ],
    [
        'id_produk' => 'Quaerat.',
        'id_kategori' => 'D',
        'nama' => 'Helena Hoeger Jr.',
        'harga' => '839353.00',
        'size' => '2',
        'status' => '1',
        'images' => null,
        'deskripsi' => 'ddfdfdsf sdf sdfs df dsf dsf ds',
    ],
    [
        'id_produk' => 'Qui eaque.',
        'id_kategori' => 'D',
        'nama' => 'Mr. Darrell Terry III',
        'harga' => '6422.00',
        'size' => '2',
        'status' => '1',
        'images' => null,
        'deskripsi' => 'ddfdfdsf sdf sdfs df dsf dsf ds',
    ],
    [
        'id_produk' => 'Qui illum.',
        'id_kategori' => 'D',
        'nama' => 'Mrs. Liza Okuneva DVM',
        'harga' => '0.00',
        'size' => '2',
        'status' => '1',
        'images' => null,
        'deskripsi' => 'ddfdfdsf sdf sdfs df dsf dsf ds',
    ],
    [
        'id_produk' => 'Quibusdam.',
        'id_kategori' => 'D',
        'nama' => 'Amy Wuckert Sr.',
        'harga' => '776772.00',
        'size' => '2',
        'status' => '1',
        'images' => null,
        'deskripsi' => 'ddfdfdsf sdf sdfs df dsf dsf ds',
    ],
    [
        'id_produk' => 'Quo odio.',
        'id_kategori' => 'D',
        'nama' => 'Jalen Balistreri',
        'harga' => '272894591.00',
        'size' => '2',
        'status' => '1',
        'images' => null,
        'deskripsi' => 'ddfdfdsf sdf sdfs df dsf dsf ds',
    ],
    [
        'id_produk' => 'Rerum aut.',
        'id_kategori' => 'D',
        'nama' => 'Birdie Lowe',
        'harga' => '48030.00',
        'size' => '2',
        'status' => '1',
        'images' => null,
        'deskripsi' => 'ddfdfdsf sdf sdfs df dsf dsf ds',
    ],
    [
        'id_produk' => 'Saepe.',
        'id_kategori' => 'D',
        'nama' => 'Mrs. Elyse Senger',
        'harga' => '73268.00',
        'size' => '2',
        'status' => '1',
        'images' => null,
        'deskripsi' => 'ddfdfdsf sdf sdfs df dsf dsf ds',
    ],
    [
        'id_produk' => 'STRAW001',
        'id_kategori' => 'PS',
        'nama' => 'Pie Susu Strawbery',
        'harga' => '18000.00',
        'size' => '1',
        'status' => '1',
        'images' => '5a75611923b26.jpg',
        'deskripsi' => 'Pie Susu dengan rasa strawbery',
    ],
    [
        'id_produk' => 'Suscipit.',
        'id_kategori' => 'D',
        'nama' => 'Prof. Niko Kunde',
        'harga' => '2.00',
        'size' => '2',
        'status' => '1',
        'images' => null,
        'deskripsi' => 'ddfdfdsf sdf sdfs df dsf dsf ds',
    ],
    [
        'id_produk' => 'Velit.',
        'id_kategori' => 'D',
        'nama' => 'Gudrun Kuhn',
        'harga' => '27178164.00',
        'size' => '2',
        'status' => '1',
        'images' => null,
        'deskripsi' => 'ddfdfdsf sdf sdfs df dsf dsf ds',
    ],
]
        );
        Yii::$app->db->createCommand("SET foreign_key_checks = 1")->execute();
    }

    public function safeDown()
    {
        //$this->truncateTable('{{%produk}} CASCADE');
    }
}
