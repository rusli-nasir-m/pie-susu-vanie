<?php

use yii\db\Schema;
use yii\db\Migration;

class m180206_142847_akunDataInsert extends Migration
{

    public function init()
    {
        $this->db = 'db';
        parent::init();
    }

    public function safeUp()
    {
        Yii::$app->db->createCommand("SET foreign_key_checks = 0")->execute();
        $this->batchInsert('{{%akun}}',
                           ["kode_rekening", "nama_rekening", "klasifikasi"],
                            [
    [
        'kode_rekening' => '1.1',
        'nama_rekening' => 'Kas',
        'klasifikasi' => 'A',
    ],
    [
        'kode_rekening' => '3.1',
        'nama_rekening' => 'Modal',
        'klasifikasi' => 'C',
    ],
    [
        'kode_rekening' => '4.1',
        'nama_rekening' => 'Pendapatan Usaha',
        'klasifikasi' => 'D',
    ],
    [
        'kode_rekening' => '5.1',
        'nama_rekening' => 'Beban Gaji',
        'klasifikasi' => 'F',
    ],
    [
        'kode_rekening' => '5.2',
        'nama_rekening' => 'Beban Bahan Baku',
        'klasifikasi' => 'F',
    ],
    [
        'kode_rekening' => '5.3',
        'nama_rekening' => 'Beban Listrik',
        'klasifikasi' => 'F',
    ],
    [
        'kode_rekening' => '5.4',
        'nama_rekening' => 'Beban Air',
        'klasifikasi' => 'F',
    ],
]
        );
        Yii::$app->db->createCommand("SET foreign_key_checks = 1")->execute();
    }

    public function safeDown()
    {
        //$this->truncateTable('{{%akun}} CASCADE');
    }
}
