<?php

use yii\db\Schema;
use yii\db\Migration;

class m180206_142223_Mass extends Migration
{

    public function init()
    {
        $this->db = 'db';
        parent::init();
    }

    public function safeUp()
    {

        Yii::$app->db->createCommand("SET foreign_key_checks = 0")->execute();
        $tables = Yii::$app->db->schema->getTableNames();
        foreach ($tables as $table) {
            Yii::$app->db->createCommand()->dropTable($table)->execute();
        }


        $tableOptions = 'ENGINE=InnoDB';

        $this->createTable('{{%akun}}',[
            'kode_rekening'=> $this->string(10)->notNull(),
            'nama_rekening'=> $this->string(100)->notNull(),
            'klasifikasi'=> $this->char(1)->null()->defaultValue(null),
        ], $tableOptions);

        $this->createIndex('fk_akun_akun_klasifikasi_1','{{%akun}}',['klasifikasi'],false);
        $this->addPrimaryKey('pk_on_akun','{{%akun}}',['kode_rekening']);

        $this->createTable('{{%akun_klasifikasi}}',[
            'klasifikasi'=> $this->char(1)->notNull(),
            'deskripsi'=> $this->string(25)->null()->defaultValue(null),
            'normal'=> $this->string(20)->notNull(),
            'inisial'=> $this->smallInteger(6)->notNull(),
        ], $tableOptions);

        $this->createIndex('_WA_Sys_DebitCredit_07C12930','{{%akun_klasifikasi}}',['normal'],false);
        $this->createIndex('_WA_Sys_CodeInitial_07C12930','{{%akun_klasifikasi}}',['inisial'],false);
        $this->createIndex('_WA_Sys_Description_07C12930','{{%akun_klasifikasi}}',['deskripsi'],false);
        $this->addPrimaryKey('pk_on_akun_klasifikasi','{{%akun_klasifikasi}}',['klasifikasi']);

        $this->createTable('{{%auto_number}}',[
            'group'=> $this->string(32)->notNull(),
            'number'=> $this->integer(11)->null()->defaultValue(null),
            'optimistic_lock'=> $this->integer(11)->null()->defaultValue(null),
            'update_time'=> $this->integer(11)->null()->defaultValue(null),
        ], $tableOptions);

        $this->addPrimaryKey('pk_on_auto_number','{{%auto_number}}',['group']);

        $this->createTable('{{%cart}}',[
            'sessionId'=> $this->string(255)->notNull(),
            'cartData'=> $this->text()->null()->defaultValue(null),
        ], $tableOptions);

        $this->addPrimaryKey('pk_on_cart','{{%cart}}',['sessionId']);

        $this->createTable('{{%detail_order}}',[
            'id_detail_cart'=> $this->primaryKey(11),
            'id_order'=> $this->string(10)->null()->defaultValue(null),
            'id_produk'=> $this->string(10)->null()->defaultValue(null),
            'harga'=> $this->decimal(15, 2)->null()->defaultValue(null),
            'jumlah'=> $this->decimal(15, 2)->null()->defaultValue(null),
        ], $tableOptions);

        $this->createIndex('fk_detail_order_order_1','{{%detail_order}}',['id_order'],false);
        $this->createIndex('fk_detail_order_produk_1','{{%detail_order}}',['id_produk'],false);

        $this->createTable('{{%image_slider}}',[
            'id_slider'=> $this->primaryKey(11),
            'image'=> $this->string(255)->null()->defaultValue(null),
        ], $tableOptions);


        $this->createTable('{{%jurnal}}',[
            'id'=> $this->primaryKey(11),
            'id_transaksi'=> $this->string(20)->notNull()->defaultValue('0'),
            'keterangan'=> $this->string(255)->null()->defaultValue(null),
            'tanggal'=> $this->date()->null()->defaultValue(null),
            'images'=> $this->string(255)->null()->defaultValue(null),
        ], $tableOptions);

        $this->createIndex('FK_JOURNAL_BUKTI','{{%jurnal}}',['id_transaksi'],false);

        $this->createTable('{{%jurnal_detail}}',[
            'id_jurnal'=> $this->integer(11)->null()->defaultValue(null),
            'kode_akun'=> $this->string(10)->null()->defaultValue(null),
            'debit'=> $this->decimal(15, 2)->null()->defaultValue(null),
            'credit'=> $this->decimal(15, 2)->null()->defaultValue(null),
            'id'=> $this->primaryKey(11),
        ], $tableOptions);

        $this->createIndex('fk_jurnal_detail_jurnal_1','{{%jurnal_detail}}',['id_jurnal'],false);
        $this->createIndex('fk_jurnal_detail_akun_1','{{%jurnal_detail}}',['kode_akun'],false);

        $this->createTable('{{%kategori_produk}}',[
            'id_kategori'=> $this->string(10)->notNull(),
            'nama_kategori'=> $this->string(20)->null()->defaultValue(null),
        ], $tableOptions);

        $this->addPrimaryKey('pk_on_kategori_produk','{{%kategori_produk}}',['id_kategori']);

        $this->createTable('{{%migration}}',[
            'version'=> $this->string(180)->notNull(),
            'apply_time'=> $this->integer(11)->null()->defaultValue(null),
        ], $tableOptions);

        $this->addPrimaryKey('pk_on_migration','{{%migration}}',['version']);

        $this->createTable('{{%order}}',[
            'id_order'=> $this->string(10)->notNull(),
            'id_pelanggan'=> $this->string(10)->null()->defaultValue(null),
            'tgl_order'=> $this->date()->null()->defaultValue(null),
            'total_harga'=> $this->decimal(15, 2)->null()->defaultValue(null),
            'bukti_pembayaran'=> $this->string(255)->null()->defaultValue(null),
            'no_rekening'=> $this->string(20)->null()->defaultValue(null),
            'bank'=> $this->string(50)->null()->defaultValue(null),
            'metode_pembayaran'=> $this->string(20)->null()->defaultValue(null),
            'nama_akun'=> $this->string(255)->null()->defaultValue(null),
            'status_pembayaran'=> $this->integer(1)->null()->defaultValue(null),
            'validasi'=> $this->integer(1)->null()->defaultValue(null),
            'tanggal_validasi'=> $this->date()->null()->defaultValue(null),
            'user_validasi'=> $this->integer(11)->null()->defaultValue(null),
        ], $tableOptions);

        $this->createIndex('fk_order_pelanggan_1','{{%order}}',['id_pelanggan'],false);
        $this->createIndex('fk_order_user_1','{{%order}}',['user_validasi'],false);
        $this->addPrimaryKey('pk_on_order','{{%order}}',['id_order']);

        $this->createTable('{{%pelanggan}}',[
            'id_pelanggan'=> $this->string(10)->notNull(),
            'nama'=> $this->string(25)->null()->defaultValue(null),
            'alamat'=> $this->string(50)->null()->defaultValue(null),
            'no_telp'=> $this->string(12)->null()->defaultValue(null),
            'email'=> $this->string(25)->null()->defaultValue(null),
            'images'=> $this->string(255)->null()->defaultValue(null),
            'username'=> $this->string(255)->null()->defaultValue(null),
            'password'=> $this->string(255)->null()->defaultValue(null),
        ], $tableOptions);

        $this->addPrimaryKey('pk_on_pelanggan','{{%pelanggan}}',['id_pelanggan']);

        $this->createTable('{{%produk}}',[
            'id_produk'=> $this->string(10)->notNull(),
            'id_kategori'=> $this->string(10)->null()->defaultValue(null),
            'nama'=> $this->string(25)->null()->defaultValue(null),
            'harga'=> $this->decimal(15, 2)->null()->defaultValue(null),
            'size'=> $this->smallInteger(1)->null()->defaultValue(null),
            'status'=> $this->smallInteger(1)->notNull()->defaultValue(1),
            'images'=> $this->string(255)->null()->defaultValue(null),
            'deskripsi'=> $this->text()->null()->defaultValue(null),
        ], $tableOptions);

        $this->createIndex('fk_produk_kategori_produk_1','{{%produk}}',['id_kategori'],false);
        $this->addPrimaryKey('pk_on_produk','{{%produk}}',['id_produk']);

        $this->createTable('{{%saran}}',[
            'id_saran'=> $this->primaryKey(11),
            'nama'=> $this->string(100)->null()->defaultValue(null),
            'mobile_phone'=> $this->string(20)->null()->defaultValue(null),
            'tanggal'=> $this->date()->null()->defaultValue(null),
            'email'=> $this->string(50)->null()->defaultValue(null),
            'saran'=> $this->text()->null()->defaultValue(null),
        ], $tableOptions);


        $this->createTable('{{%user}}',[
            'id_user'=> $this->primaryKey(11),
            'nama_user'=> $this->string(255)->null()->defaultValue(null),
            'username'=> $this->string(255)->null()->defaultValue(null),
            'password'=> $this->string(255)->null()->defaultValue(null),
            'akses'=> $this->string(10)->null()->defaultValue(null),
        ], $tableOptions);

        $this->addForeignKey(
            'fk_detail_order_id_order',
            '{{%detail_order}}', 'id_order',
            '{{%order}}', 'id_order',
            'CASCADE', 'CASCADE'
        );
        $this->addForeignKey(
            'fk_detail_order_id_produk',
            '{{%detail_order}}', 'id_produk',
            '{{%produk}}', 'id_produk',
            'CASCADE', 'CASCADE'
        );
        $this->addForeignKey(
            'fk_jurnal_id_transaksi',
            '{{%jurnal}}', 'id_transaksi',
            '{{%transaksi}}', 'id_transaksi',
            'CASCADE', 'CASCADE'
        );
        $this->addForeignKey(
            'fk_jurnal_detail_kode_akun',
            '{{%jurnal_detail}}', 'kode_akun',
            '{{%akun}}', 'kode_rekening',
            'CASCADE', 'CASCADE'
        );
        $this->addForeignKey(
            'fk_jurnal_detail_id_jurnal',
            '{{%jurnal_detail}}', 'id_jurnal',
            '{{%jurnal}}', 'id',
            'CASCADE', 'CASCADE'
        );
        $this->addForeignKey(
            'fk_order_id_pelanggan',
            '{{%order}}', 'id_pelanggan',
            '{{%pelanggan}}', 'id_pelanggan',
            'CASCADE', 'CASCADE'
        );
        $this->addForeignKey(
            'fk_order_user_validasi',
            '{{%order}}', 'user_validasi',
            '{{%user}}', 'id_user',
            'CASCADE', 'CASCADE'
        );
        $this->addForeignKey(
            'fk_produk_id_kategori',
            '{{%produk}}', 'id_kategori',
            '{{%kategori_produk}}', 'id_kategori',
            'CASCADE', 'CASCADE'
        );
        Yii::$app->db->createCommand("SET foreign_key_checks = 1")->execute();
    }

    public function safeDown()
    {
        Yii::$app->db->createCommand("SET foreign_key_checks = 0")->execute();
            $this->dropForeignKey('fk_detail_order_id_order', '{{%detail_order}}');
            $this->dropForeignKey('fk_detail_order_id_produk', '{{%detail_order}}');
            $this->dropForeignKey('fk_jurnal_id_transaksi', '{{%jurnal}}');
            $this->dropForeignKey('fk_jurnal_detail_kode_akun', '{{%jurnal_detail}}');
            $this->dropForeignKey('fk_jurnal_detail_id_jurnal', '{{%jurnal_detail}}');
            $this->dropForeignKey('fk_order_id_pelanggan', '{{%order}}');
            $this->dropForeignKey('fk_order_user_validasi', '{{%order}}');
            $this->dropForeignKey('fk_produk_id_kategori', '{{%produk}}');
            $this->dropPrimaryKey('pk_on_akun','{{%akun}}');
            $this->dropTable('{{%akun}}');
            $this->dropPrimaryKey('pk_on_akun_klasifikasi','{{%akun_klasifikasi}}');
            $this->dropTable('{{%akun_klasifikasi}}');
            $this->dropPrimaryKey('pk_on_auto_number','{{%auto_number}}');
            $this->dropTable('{{%auto_number}}');
            $this->dropPrimaryKey('pk_on_cart','{{%cart}}');
            $this->dropTable('{{%cart}}');
            $this->dropTable('{{%detail_order}}');
            $this->dropTable('{{%image_slider}}');
            $this->dropTable('{{%jurnal}}');
            $this->dropTable('{{%jurnal_detail}}');
            $this->dropPrimaryKey('pk_on_kategori_produk','{{%kategori_produk}}');
            $this->dropTable('{{%kategori_produk}}');
            $this->dropPrimaryKey('pk_on_migration','{{%migration}}');
            $this->dropTable('{{%migration}}');
            $this->dropPrimaryKey('pk_on_order','{{%order}}');
            $this->dropTable('{{%order}}');
            $this->dropPrimaryKey('pk_on_pelanggan','{{%pelanggan}}');
            $this->dropTable('{{%pelanggan}}');
            $this->dropPrimaryKey('pk_on_produk','{{%produk}}');
            $this->dropTable('{{%produk}}');
            $this->dropTable('{{%saran}}');
            $this->dropTable('{{%user}}');
        Yii::$app->db->createCommand("SET foreign_key_checks = 1")->execute();
    }
}
