<?php
return [
    'aliases' => [
        '@bower' => '@vendor/bower-asset',
        '@npm'   => '@vendor/npm-asset',
    ],
    'vendorPath' => dirname(dirname(__DIR__)) . '/vendor',
    'bootstrap' => [
//        'common\cclass\UpdateTableBootstrap' // non aktifkan bagian ini saat live
    ],
    'components' => [
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'urlManager' => [
            'class'=>'yii\web\UrlManager',
            'enablePrettyUrl' => true,
            'showScriptName' => false,
        ],
        'cart' => [
            'class' => 'common\widgets\dxCart\ShoppingCart',
            'cartId' => 'yii2shop',
        ],
        'formatter' => [
//            'dateFormat' => 'dd.MM.yyyy',
            'decimalSeparator' => ',',
            'thousandSeparator' => '.',
            'currencyCode' => 'IDR',
            'nullDisplay' => '-'
        ],
//        'assetManager' => [
//            'linkAssets' => true,
////            'basePath'=>'@assetCache',
////            'baseUrl'=>'@assetUrl',
//            'appendTimestamp' => true,
//        ],

    ],
];
