<?php
require dirname(__DIR__).'/functions/globalFunctions.php';

Yii::setAlias('@common', dirname(__DIR__));
Yii::setAlias('@frontend', dirname(dirname(__DIR__)) . '/frontend');
Yii::setAlias('@admin', dirname(dirname(__DIR__)) . '/admin');
Yii::setAlias('@console', dirname(dirname(__DIR__)) . '/console');
Yii::setAlias('@storage', dirname(dirname(__DIR__)) . '/storage');

//Yii::setAlias('@assetCache', dirname(dirname(__DIR__)) . '/console');
//Yii::setAlias('@assetUrl', dirname(dirname(__DIR__)) . '/console');

//Yii::setAlias('@frontendUrl','http://localhost/stiki/pie-susu-vanie/');
//Yii::setAlias('@adminUrl', 'http://localhost/stiki/pie-susu-vanie/admin');
//Yii::setAlias('@storageUrl', 'http://localhost/stiki/pie-susu-vanie/storage');

//Yii::setAlias('@frontendUrl','http://localhost/pie-susu-vanie/');
//Yii::setAlias('@adminUrl', 'http://localhost/pie-susu-vanie/admin');
//Yii::setAlias('@storageUrl', 'http://localhost/pie-susu-vanie/storage');

require __DIR__.'/bootstrap-local.php';