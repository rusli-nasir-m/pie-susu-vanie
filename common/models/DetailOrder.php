<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%detail_order}}".
 *
 * @property int $id_detail_cart
 * @property string $id_order
 * @property string $id_produk
 * @property string $harga
 * @property string $jumlah
 *
 * @property Order $order
 * @property Produk $produk
 */
class DetailOrder extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%detail_order}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['harga', 'jumlah'], 'number'],
            [['id_order', 'id_produk'], 'string', 'max' => 10],
            [['id_order'], 'exist', 'skipOnError' => true, 'targetClass' => Order::className(), 'targetAttribute' => ['id_order' => 'id_order']],
            [['id_produk'], 'exist', 'skipOnError' => true, 'targetClass' => Produk::className(), 'targetAttribute' => ['id_produk' => 'id_produk']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_detail_cart' => Yii::t('app', 'Id Detail Cart'),
            'id_order' => Yii::t('app', 'Id Order'),
            'id_produk' => Yii::t('app', 'Id Produk'),
            'harga' => Yii::t('app', 'Harga'),
            'jumlah' => Yii::t('app', 'Jumlah'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrder()
    {
        return $this->hasOne(Order::className(), ['id_order' => 'id_order']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProduk()
    {
        return $this->hasOne(Produk::className(), ['id_produk' => 'id_produk']);
    }

    /**
     * @inheritdoc
     * @return \common\models\query\DetailOrderQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \common\models\query\DetailOrderQuery(get_called_class());
    }
}
