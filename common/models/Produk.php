<?php

namespace common\models;

use common\behaviors\UploadImageBehavior;
use common\widgets\hcCart\ItemInterface;
use Yii;

/**
 * This is the model class for table "{{%produk}}".
 *
 * @property string $id_produk
 * @property string $id_kategori
 * @property string $nama
 * @property string $harga
 * @property int $size
 * @property int $status
 * @property string $images
 * @property string $deskripsi
 *
 * @property DetailOrder[] $detailOrders
 * @property KategoriProduk $kategori
 */
class Produk extends \yii\db\ActiveRecord implements ItemInterface
{

    public $quantity =0;

    const INSERT_SCENARIOS = 'insert';
    const UPDATE_SCENARIOS = 'update';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%produk}}';
    }

    public function behaviors()
    {
        return [
            [
                'class' => UploadImageBehavior::className(),
                'attribute' => 'images',
				'key_value' => 'id_produk',
                'scenarios' => ['insert', 'update'],
                'placeholder' => '@storage/image/no_image.jpg',
                'path' => '@storage/web/produk/foto/{id_produk}',
                'url' => '@storageUrl/web/produk/foto/{id_produk}',
                'thumbs' => [
                    'thumb' => ['width' => 400, 'quality' => 90],
                    'preview' => ['width' => 200, 'height' => 200],
                    'news_thumb' => ['width' => 200, 'height' => 200, 'bg_color' => '000'],
                ],
            ]
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_produk'], 'required'],
            [['harga'], 'number'],
            [['size', 'status'], 'integer'],
            [['deskripsi'], 'string'],
            [['id_produk', 'id_kategori'], 'string', 'max' => 10],
            [['nama'], 'string', 'max' => 25],
            ['images', 'image', 'extensions' => 'jpg, jpeg, gif, png',
                'minWidth' => 100, 'maxWidth' => 3000,
                'minHeight' => 100, 'maxHeight' => 3000,
                'maxSize' => (2048*2048), 'tooBig' => 'Limit is 2 Mb',
                'on' => ['insert', 'update']
            ],
            [['id_produk'], 'unique'],
            [['id_kategori'], 'exist', 'skipOnError' => true, 'targetClass' => KategoriProduk::className(), 'targetAttribute' => ['id_kategori' => 'id_kategori']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_produk' => Yii::t('app', 'Id Produk'),
            'id_kategori' => Yii::t('app', 'Id Kategori'),
            'nama' => Yii::t('app', 'Nama'),
            'harga' => Yii::t('app', 'Harga'),
            'size' => Yii::t('app', 'Size'),
            'status' => Yii::t('app', 'Status'),
            'images' => Yii::t('app', 'Images'),
            'deskripsi' => Yii::t('app', 'Deskripsi'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDetailOrders()
    {
        return $this->hasMany(DetailOrder::className(), ['id_produk' => 'id_produk']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getKategori()
    {
        return $this->hasOne(KategoriProduk::className(), ['id_kategori' => 'id_kategori']);
    }

    /**
     * @inheritdoc
     * @return \common\models\query\ProdukQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \common\models\query\ProdukQuery(get_called_class());
    }

    /**
     * @return integer
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @return string
     */
    public function getId()
    {
        return $this->id_produk;
    }

    /**
     * @param bool $withDiscount
     *
     * @return integer
     */
    public function getCost($withDiscount = false)
    {
        // TODO: Implement getCost() method.
    }

    /**
     * @param int $quantity
     */
    public function setQuantity($quantity)
    {
        $this->quantity += $quantity;
    }

    /**
     * @return int
     */
    public function getQuantity()
    {
        return $this->quantity;
    }
}
