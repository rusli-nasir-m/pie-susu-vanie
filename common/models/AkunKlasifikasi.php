<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%akun_klasifikasi}}".
 *
 * @property string $klasifikasi
 * @property string $deskripsi
 * @property string $normal
 * @property int $inisial
 */
class AkunKlasifikasi extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%akun_klasifikasi}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['klasifikasi', 'normal', 'inisial'], 'required'],
            [['inisial'], 'integer'],
            [['klasifikasi'], 'string', 'max' => 1],
            [['deskripsi'], 'string', 'max' => 25],
            [['normal'], 'string', 'max' => 20],
            [['klasifikasi'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'klasifikasi' => Yii::t('app', 'Klasifikasi'),
            'deskripsi' => Yii::t('app', 'Deskripsi'),
            'normal' => Yii::t('app', 'Normal'),
            'inisial' => Yii::t('app', 'Inisial'),
        ];
    }

    /**
     * @inheritdoc
     * @return \common\models\query\AkunKlasifikasiQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \common\models\query\AkunKlasifikasiQuery(get_called_class());
    }
}
