<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%saran}}".
 *
 * @property int $id_saran
 * @property string $nama
 * @property string $mobile_phone
 * @property string $tanggal
 * @property string $email
 * @property string $saran
 */
class Saran extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%saran}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['tanggal'], 'safe'],
            [['saran'], 'string'],
            [['nama'], 'string', 'max' => 100],
            [['mobile_phone'], 'string', 'max' => 20],
            [['email'], 'string', 'max' => 50],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_saran' => Yii::t('app', 'Id Saran'),
            'nama' => Yii::t('app', 'Nama'),
            'mobile_phone' => Yii::t('app', 'Mobile Phone'),
            'tanggal' => Yii::t('app', 'Tanggal'),
            'email' => Yii::t('app', 'Email'),
            'saran' => Yii::t('app', 'Saran'),
        ];
    }

    /**
     * @inheritdoc
     * @return \common\models\query\SaranQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \common\models\query\SaranQuery(get_called_class());
    }
}
