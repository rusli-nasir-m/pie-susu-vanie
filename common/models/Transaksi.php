<?php

namespace common\models;

use common\behaviors\Autonumber\Behavior;
use Yii;

/**
 * This is the model class for table "{{%transaksi}}".
 *
 * @property string $id_transaksi
 * @property string $tanggal
 * @property string $keterangan_transaksi
 * @property string $total_transaksi
 * @property string $status
 * @property string $tipe_transaksi
 * @property string $no_bukti
 *
 * @property Jurnal[] $jurnals
 */
class Transaksi extends \yii\db\ActiveRecord
{

    const TIPE_MASUK = 'Masuk';
    const TIPE_KELUAR = 'Keluar';
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%transaksi}}';
    }

    public function behaviors()
    {
        return [
            [
                'class' => Behavior::className(),
                'attribute' => 'id_transaksi',
                'value' => 'TR?',
                'digit' => 6,
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
//            [['id_transaksi'], 'required'],
            [['tanggal'], 'safe'],
            [['total_transaksi'], 'number'],
            [['status', 'tipe_transaksi'], 'string'],
            [['id_transaksi'], 'string', 'max' => 20],
            [['keterangan_transaksi', 'no_bukti'], 'string', 'max' => 100],
            [['id_transaksi'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_transaksi' => Yii::t('app', 'Id Transaksi'),
            'tanggal' => Yii::t('app', 'Tanggal'),
            'keterangan_transaksi' => Yii::t('app', 'Keterangan Transaksi'),
            'total_transaksi' => Yii::t('app', 'Total Transaksi'),
            'status' => Yii::t('app', 'Status'),
            'tipe_transaksi' => Yii::t('app', 'Tipe Transaksi'),
            'no_bukti' => Yii::t('app', 'No Bukti'),
        ];
    }

    /**
     * Public static function getTotal($provider,$field) For
     **/
    public static function getTotal($provider,$field,$formated = false)
    {
        $total = 0;

        foreach ($provider as $item){
            $total += $item[$field];
        }
        return $formated? formatter()->asCurrency($total):$total;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getJurnals()
    {
        return $this->hasMany(Jurnal::className(), ['id_transaksi' => 'id_transaksi']);
    }

    /**
     * @inheritdoc
     * @return \common\models\query\TransaksiQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \common\models\query\TransaksiQuery(get_called_class());
    }
}
