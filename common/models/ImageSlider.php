<?php

namespace common\models;

use Yii;
use common\behaviors\UploadImageBehavior;

/**
 * This is the model class for table "{{%image_slider}}".
 *
 * @property int $id_slider
 * @property string $image
 */
class ImageSlider extends \yii\db\ActiveRecord
{

    const INSERT_SCENARIOS = 'insert';
    const UPDATE_SCENARIOS = 'update';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%image_slider}}';
    }

    public function behaviors()
    {
        return [
            [
                'class' => UploadImageBehavior::className(),
                'attribute' => 'image',
                'scenarios' => ['insert', 'update'],
                'placeholder' => '@storage/image/slider/banner-1.jpg',
                'path' => '@storage/web/slider',
                'url' => '@storageUrl/web/slider',
                'thumbs' => [
                    'thumb' => ['width' => 400, 'quality' => 90],
                    'preview' => ['width' => 200, 'height' => 200],
                    'news_thumb' => ['width' => 200, 'height' => 200, 'bg_color' => '000'],
                ],
            ]
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['image', 'image', 'extensions' => 'jpg, jpeg, gif, png',
                'minWidth' => 100, 'maxWidth' => 1140,
                'minHeight' => 100, 'maxHeight' => 1000,
                'maxSize' => (1024*1024), 'tooBig' => 'Limit is 1 Mb',
                'on' => ['insert', 'update']
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_slider' => Yii::t('app', 'Id Slider'),
            'image' => Yii::t('app', 'Image'),
        ];
    }
}
