<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%bahan_baku}}".
 *
 * @property string $id_bahanbaku
 * @property string $id_admin
 * @property string $gaji_pegawai
 * @property string $biaya_listrik
 * @property string $biaya_air
 * @property string $biaya_lainnya
 * @property string $tanggal
 * @property string $images
 */
class BahanBaku extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%bahan_baku}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_bahanbaku', 'id_admin', 'gaji_pegawai', 'biaya_listrik', 'biaya_air', 'biaya_lainnya', 'tanggal', 'images'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_bahanbaku' => Yii::t('app', 'Id Bahanbaku'),
            'id_admin' => Yii::t('app', 'Id Admin'),
            'gaji_pegawai' => Yii::t('app', 'Gaji Pegawai'),
            'biaya_listrik' => Yii::t('app', 'Biaya Listrik'),
            'biaya_air' => Yii::t('app', 'Biaya Air'),
            'biaya_lainnya' => Yii::t('app', 'Biaya Lainnya'),
            'tanggal' => Yii::t('app', 'Tanggal'),
            'images' => Yii::t('app', 'Images'),
        ];
    }

    /**
     * @inheritdoc
     * @return \common\models\query\BahanBakuQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \common\models\query\BahanBakuQuery(get_called_class());
    }
}
