<?php

namespace common\models;

use Yii;
use common\behaviors\Autonumber\Behavior;
use common\behaviors\UploadImageBehavior;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "{{%pelanggan}}".
 *
 * @property string $id_pelanggan
 * @property string $nama
 * @property string $alamat
 * @property string $no_telp
 * @property string $email
 * @property string $images
 * @property string $username
 * @property string $password
 * @property string $tgl_registrasi
 * @property int $status
 * @property string $kota
 * @property string $provinsi
 * @property string $kode_pos
 *
 * @property Order[] $orders
 */
class Pelanggan extends \yii\db\ActiveRecord
{

    const INSERT_SCENARIOS = 'insert';
    const UPDATE_SCENARIOS = 'update';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%pelanggan}}';
    }

    public function behaviors()
    {
        return [
            [
                'class' => Behavior::className(),
                'attribute' => 'id_pelanggan',
                'value' => 'PLGN?',
                'digit' => 6,
            ],
            [
                'class' => UploadImageBehavior::className(),
                'attribute' => 'images',
                'scenarios' => ['insert', 'update'],
                'placeholder' => '@storage/image/no_image.jpg',
                'path' => '@storage/web/pelanggan/{id_pelanggan}/foto/{id_pelanggan}',
                'url' => '@storageUrl/web/pelanggan/{id_pelanggan}/foto/{id_pelanggan}',
                'thumbs' => [
                    'thumb' => ['width' => 400, 'quality' => 90],
                    'preview' => ['width' => 200, 'height' => 200],
                    'news_thumb' => ['width' => 200, 'height' => 200, 'bg_color' => '000'],
                ],
            ]
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_pelanggan'], 'required'],
            [['id_pelanggan'], 'string', 'max' => 10],
            [['nama', 'email'], 'string', 'max' => 25],
            [['alamat'], 'string', 'max' => 50],
            [['no_telp'], 'string', 'max' => 12],
            ['images', 'image', 'extensions' => 'jpg, jpeg, gif, png',
                'minWidth' => 100, 'maxWidth' => 1000,
                'minHeight' => 100, 'maxHeight' => 1000,
                'maxSize' => (1024*1024), 'tooBig' => 'Limit is 1 Mb',
                'on' => ['insert', 'update']
            ],
            ['tgl_registrasi','default','value' => date('Y-m-d h:i:s')],
            [['id_pelanggan'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_pelanggan' => Yii::t('app', 'Id Pelanggan'),
            'nama' => Yii::t('app', 'Nama'),
            'alamat' => Yii::t('app', 'Alamat'),
            'no_telp' => Yii::t('app', 'No Telp'),
            'email' => Yii::t('app', 'Email'),
            'images' => Yii::t('app', 'Images'),
            'username' => Yii::t('app', 'Username'),
            'password' => Yii::t('app', 'Password'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrders()
    {
        return $this->hasMany(Order::className(), ['id_pelanggan' => 'id_pelanggan']);
    }

    /**
     * @inheritdoc
     * @return \common\models\query\PelangganQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \common\models\query\PelangganQuery(get_called_class());
    }
}
