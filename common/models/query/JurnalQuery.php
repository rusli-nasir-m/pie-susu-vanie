<?php

namespace common\models\query;

/**
 * This is the ActiveQuery class for [[\common\models\Jurnal]].
 *
 * @see \common\models\Jurnal
 */
class JurnalQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return \common\models\Jurnal[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return \common\models\Jurnal|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
