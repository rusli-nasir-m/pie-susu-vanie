<?php

namespace common\models\query;

/**
 * This is the ActiveQuery class for [[\common\models\BahanBaku]].
 *
 * @see \common\models\BahanBaku
 */
class BahanBakuQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return \common\models\BahanBaku[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return \common\models\BahanBaku|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
