<?php

namespace common\models\query;

/**
 * This is the ActiveQuery class for [[\common\models\Order]].
 *
 * @see \common\models\Order
 */
class OrderQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return \common\models\Order[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return \common\models\Order|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }

    /**
     * Public function isValidasi() For
     **/
    public function isValidasi()
    {
        return $this->andWhere(['validasi'=> 1]);
    }

    /**
     * Public function byID($id) For
     **/
    public function byID($id)
    {
        return $this->andWhere(['id_order' => $id]);
    }
}
