<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%kategori_produk}}".
 *
 * @property string $id_kategori
 * @property string $nama_kategori
 * @property string $status
 *
 * @property Produk[] $produks
 */
class KategoriProduk extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%kategori_produk}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_kategori'], 'required'],
            [['status'], 'number'],
			['status', 'default', 'value' => 1],
            [['id_kategori'], 'string', 'max' => 10],
            [['nama_kategori'], 'string', 'max' => 20],
            [['id_kategori'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_kategori' => Yii::t('app', 'Id Kategori'),
            'nama_kategori' => Yii::t('app', 'Nama Kategori'),
            'status' => Yii::t('app', 'Status'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProduks()
    {
        return $this->hasMany(Produk::className(), ['id_kategori' => 'id_kategori']);
    }

    /**
     * @inheritdoc
     * @return \common\models\query\KategoriProdukQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \common\models\query\KategoriProdukQuery(get_called_class());
    }
}
