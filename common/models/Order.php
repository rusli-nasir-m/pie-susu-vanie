<?php

namespace common\models;

use common\behaviors\Autonumber\Behavior;
use common\behaviors\UploadImageBehavior;
use Yii;

/**
 * This is the model class for table "{{%order}}".
 *
 * @property string $id_order
 * @property string $id_pelanggan
 * @property string $tgl_order
 * @property string $tanggal_validasi
 * @property string $total_harga
 * @property string $bukti_pembayaran
 * @property string $no_rekening
 * @property string $bank
 * @property string $metode_pembayaran
 * @property string $nama_akun
 * @property int $status_pembayaran
 * @property string $tgl_pembayaran
 * @property int $validasi
 * @property int $user_validasi
 * @property string $nama
 * @property string $alamat
 * @property string $no_telp
 * @property string $ongkir
 * @property string $kota
 * @property string $provinsi
 * @property string $kode_pos

 *
 * @property DetailOrder[] $detailOrders
 * @property Pelanggan $pelanggan
 */
class Order extends \yii\db\ActiveRecord
{

    const SCENARIO_VALIDASI = 'validasi';
    const SCENARIO_NEW_UPDATE = 'new';
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            [
                'class' => Behavior::className(),
                'attribute' => 'id_order',
                'value' => 'PO?',
                'digit' => 6,
            ],
            [
                'class' => UploadImageBehavior::className(),
                'attribute' => 'bukti_pembayaran',
                'key_value' => 'id_order',
                'scenarios' => ['insert', 'update'],
                'placeholder' => '@storage/image/no_image.jpg',
                'path' => '@storage/web/order/{id_order}/bukti',
                'url' => '@storageUrl/web/order/{id_order}/bukti',
                'thumbs' => [
                    'thumb' => ['width' => 400, 'quality' => 90],
                    'preview' => ['width' => 200, 'height' => 200],
                    'news_thumb' => ['width' => 200, 'height' => 200, 'bg_color' => '000'],
                ],
            ]
//            TimestampBehavior::className(),
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%order}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
//            [['id_order'], 'required'],
            [['tgl_order','total_harga','ongkir','nama','alamat','no_telp','provinsi','kode_pos','kota'], 'required','on' => self::SCENARIO_NEW_UPDATE],
            [['tgl_order','total_harga','validasi','tanggal_validasi'], 'required','on' => self::SCENARIO_VALIDASI],
            [['tgl_order','tgl_pembayaran','tanggal_validasi'], 'safe'],
            [['total_harga','ongkir'], 'number'],
            [['status_pembayaran','validasi','user_validasi'], 'integer'],
            [['id_order', 'id_pelanggan','kode_pos'], 'string', 'max' => 10],
            [['nama_akun', 'nama', 'alamat', 'kota', 'provinsi'], 'string', 'max' => 255],
            [['no_rekening', 'metode_pembayaran','no_telp'], 'string', 'max' => 20],
            [['bank'], 'string', 'max' => 50],
            [['id_order'], 'unique'],
            [['id_pelanggan'], 'exist', 'skipOnError' => true, 'targetClass' => Pelanggan::className(), 'targetAttribute' => ['id_pelanggan' => 'id_pelanggan']],
            ['bukti_pembayaran', 'image', 'extensions' => 'jpg, jpeg, gif, png',
                'minWidth' => 100, 'maxWidth' => 1000,
                'minHeight' => 100, 'maxHeight' => 1000,
                'maxSize' => (1024*1024), 'tooBig' => 'Limit is 1 Mb',
                'on' => ['insert', 'update']
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_order' => Yii::t('app', 'Id Order'),
            'id_pelanggan' => Yii::t('app', 'Id Pelanggan'),
            'tgl_order' => Yii::t('app', 'Tgl Order'),
            'total_harga' => Yii::t('app', 'Total Harga'),
            'bukti_pembayaran' => Yii::t('app', 'Bukti Pembayaran'),
            'no_rekening' => Yii::t('app', 'No Rekening'),
            'bank' => Yii::t('app', 'Bank'),
            'metode_pembayaran' => Yii::t('app', 'Metode Pembayaran'),
            'nama_akun' => Yii::t('app', 'Nama Akun'),
            'status_pembayaran' => Yii::t('app', 'Status Pembayaran'),
            'validasi' => Yii::t('app', 'Validasi'),
            'tanggal_validasi' => Yii::t('app', 'Tanggal Validasi'),
            'user_validasi' => Yii::t('app', 'User Validasi'),
            'nama' => Yii::t('app', 'Nama'),
            'alamat' => Yii::t('app', 'Alamat'),
            'no_telp' => Yii::t('app', 'No Telp'),
            'ongkir' => Yii::t('app', 'Ongkir'),
            'kota' => Yii::t('app', 'Kota'),
            'provinsi' => Yii::t('app', 'Provinsi'),
            'kode_pos' => Yii::t('app', 'Kode Pos'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDetailOrders()
    {
        return $this->hasMany(DetailOrder::className(), ['id_order' => 'id_order']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPelanggan()
    {
        return $this->hasOne(Pelanggan::className(), ['id_pelanggan' => 'id_pelanggan']);
    }

    /**
     * @inheritdoc
     * @return \common\models\query\OrderQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \common\models\query\OrderQuery(get_called_class());
    }
}
