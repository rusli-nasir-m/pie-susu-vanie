<?php

namespace common\widgets\shoppingCart\storage;

use common\widgets\shoppingCart\Cart;

/**
 * Interface StorageInterface
 * @package common\widgets\shoppingCart\cart
 */
interface StorageInterface
{

    /**
     * @param \common\widgets\shoppingCart\Cart $cart
     *
     * @return void
     */
    public function load(Cart $cart);

    /**
     * @param \common\widgets\shoppingCart\Cart $cart
     *
     * @return void
     */
    public function save(Cart $cart);
}