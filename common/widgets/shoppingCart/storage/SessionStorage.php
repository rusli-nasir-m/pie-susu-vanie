<?php

namespace common\widgets\shoppingCart\storage;

use Yii;
use common\widgets\shoppingCart\BaseObject as Object;
use common\widgets\shoppingCart\Cart;

/**
 * Class SessionStorage
 * @property \yii\web\Session session
 * @package common\widgets\shoppingCart\cart
 */
class SessionStorage extends Object implements StorageInterface
{
    /**
     * @var string
     */
    public $key = 'cart';

    /**
     * @inheritdoc
     */
    public function load(Cart $cart)
    {
        $cartData = [];
        if (false !== ($session = ($this->session->get($this->key, false)))) {
            $cartData = unserialize($session);
        }
        return $cartData;
    }

    /**
     * @inheritdoc
     */
    public function save(Cart $cart)
    {
        $sessionData = serialize($cart->getItems());
        $this->session->set($this->key, $sessionData);
    }

    /**
     * @return \yii\web\Session
     */
    public function getSession()
    {
        return Yii::$app->get('session');
    }
}