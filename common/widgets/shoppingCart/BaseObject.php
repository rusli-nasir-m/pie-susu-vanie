<?php
/**
 * Created by PhpStorm.
 * User: AcenetDev
 * Date: 2/5/2018
 * Time: 9:42 PM
 */

namespace common\widgets\shoppingCart;

if(!class_exists('yii\base\BaseObject')){
    class_alias('yii\base\Object', 'yii\base\BaseObject');
}


class BaseObject extends \yii\base\BaseObject
{

}