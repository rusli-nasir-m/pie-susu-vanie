<?php

namespace common\assets\marketshop;

use yii\helpers\ArrayHelper;
use yii\web\AssetBundle;

/**
 * Main frontend application asset bundle.
 */
class MarketShopAsset extends AssetBundle
{

//    public $basePath = '@webroot';
//    public $baseUrl = '@web';

    public $sourcePath = '@common/assets/marketshop/src';

    public $themes = '';

    public $css = [
        'css/font-awesome/css/font-awesome.min.css',
        'css/stylesheet.css',
        'css/owl.carousel.css',
        'css/owl.transitions.css',
        'css/responsive.css',
        'css/stylesheet-skin2.css',
    ];

    public $js = [
        'js/jquery.easing-1.3.min.js',
        'js/jquery.dcjqaccordion.min.js',
        'js/owl.carousel.min.js',
        'js/custom.js'
    ];

    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapPluginAsset',
    ];

}
