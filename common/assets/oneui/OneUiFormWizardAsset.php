<?php

namespace common\assets\oneui;

use yii\helpers\ArrayHelper;
use yii\web\AssetBundle;

/**
 * Main frontend application asset bundle.
 */
class OneUiFormWizardAsset extends AssetBundle
{

    public $sourcePath = '@common/assets/oneui/src';
    public $themes = '';

    public $js = [
        'js/plugins/bootstrap-wizard/jquery.bootstrap.wizard.min.js',
    ];
    public $depends = [
        'yii\bootstrap\BootstrapPluginAsset',
    ];
}
