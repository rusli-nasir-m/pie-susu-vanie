<?php
/**
 * Created by PhpStorm.
 * User: AcenetDev
 * Date: 1/22/2018
 * Time: 1:45 PM
 */

namespace common\assets\oneui\widget\Marque;

use yii\web\AssetBundle;

class MarqueAsset extends AssetBundle
{

    public $sourcePath = '@common/assets/oneui/widget/Marque/src';

    public $css = [

    ];

    public $js = [
        'js/jquery.marquee.min.js'
    ];

    public $depends = [
        'yii\bootstrap\BootstrapPluginAsset',
    ];
}