<?php
/**
 * Created by PhpStorm.
 * User: AcenetDev
 * Date: 11/21/2017
 * Time: 3:55 PM
 */

namespace common\assets\oneui\widget;


use yii\bootstrap\Html;
use yii\bootstrap\Widget;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;

class Stat extends Widget
{

    public $items = [];

    public $template = '<div class="content bg-white border-b">
            <div class="row items-push text-uppercase">
            {items}            
            </div>
    </div>';

    public $itemTemplate = '<div class="col-xs-6 col-sm-3">
                    <div class="font-w700 text-gray-darker animated fadeIn">{title}</div>
                    <div class="text-muted animated fadeIn"><small>{icon} {subtitle}</small></div>
                    {link}
                </div>';
    public $linkTemplate = '<a class="h2 font-w300 text-primary animated flipInX" href="{url}">{value}</a>';

    public function renderItems($items)
    {
        $n = count($items);
        $lines = [];

        foreach ($items as $i => $item){
            $lines[] = $this->renderItem($item);
        }

        return implode("\n",$lines);
    }

    public function renderItem($item)
    {
        $template = ArrayHelper::getValue($item, 'template', $this->itemTemplate);
        $link = '';
        if(isset($item['link'])){
            $link = strtr($this->linkTemplate,[
                '{url}' => Html::encode(Url::to($item['link']['url'])),
                '{value}' => $item['link']['value'],
            ]);
        }else{
            $link = strtr($this->linkTemplate,[
                '{url}' => '#',
                '{value}' => $item['value'],
            ]);
        }

        return strtr($template,[
            '{title}' => $item['title'],
            '{subtitle}' => $item['subtitle'],
            '{icon}' => isset($item['icon'])? $item['icon']:null,
            '{link}' => $link,
        ]);
    }

    public function run()
    {
        return strtr($this->template,[
            '{items}' => $this->renderItems($this->items)
        ]);
    }

}