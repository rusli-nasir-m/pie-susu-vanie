<?php
/**
 * Created by PhpStorm.
 * User: AcenetDev
 * Date: 1/12/2018
 * Time: 2:06 PM
 */
namespace common\assets\oneui\widget\Slider;

use Yii;
use yii\web\AssetBundle;

class SliderAsset extends AssetBundle
{

    /**
     * [$sourcePath description]
     * @var string
     */
    public $sourcePath = '@common/assets/oneui/src';
    /**
     * the language the calender will be displayed in
     * @var string ISO2 code for the wished display language
     */
    public $language = NULL;
    /**
     * [$autoGenerate description]
     * @var boolean
     */
    public $autoGenerate = true;
    /**
     * tell the calendar, if you like to render google calendar events within the view
     * @var boolean
     */
    public $googleCalendar = false;

    /**
     * [$css description]
     * @var array
     */
    public $css = [
        'js/plugins/slick/slick.min.css',
        'js/plugins/slick/slick-theme.min.css',
    ];
    /**
     * [$js description]
     * @var array
     */
    public $js = [
        'js/plugins/slick/slick.min.js',
    ];

    /**
     * [$depends description]
     * @var array
     */
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapPluginAsset',
    ];


}