<?php
/**
 * Created by PhpStorm.
 * User: AcenetDev
 * Date: 1/12/2018
 * Time: 2:10 PM
 */

/**
 * How to use
 *<?=Slick::widget([
 *		// HTML tag for container. Div is default.
 *        'itemContainer' => 'div',
 *
 *        // HTML attributes for widget container
 *        'containerOptions' => ['class' => 'container'],
 *
 *        // Position for inclusion js-code
 *        // see more here: http://www.yiiframework.com/doc-2.0/yii-web-view.html#registerJs()-detail
 *        'jsPosition' => yii\web\View::POS_READY,
 *
 *        // It possible to use Slick.js events
 *        // see more: http://kenwheeler.github.io/slick/#events
 *        'events' => [
 *            'edge' => 'function(event, slick, direction) {
 *                           console.log(direction);
 *                           // left
 *                      });'
 *        ],
 *
 *        // Items for carousel. Empty array not allowed, exception will be throw, if empty
 *        'items' => [
 *            "
 *              <img class="img-avatar" src="assets/img/avatars/avatar5.jpg" alt="">
                <div class="push-10-t font-w600">Judy Alvarez</div>
                <div class="font-s13 text-muted">Font Designer</div>
 *            ",
 *            Html::img('/cat_gallery/cat_002.png'),
 *            Html::img('/cat_gallery/cat_003.png'),
 *            Html::img('/cat_gallery/cat_004.png'),
 *            Html::img('/cat_gallery/cat_005.png'),
 *        ],
 *
 * Or alternative item using data provider
 *        'dataProvider' => $listDataProvider,
 *        'items' => function ($model, $key, $index, $widget) {
 *              $itemContent = $this->render('_list_job',['model' => $model]);
 *              return $itemContent;
 *        }
 *
 *        // HTML attribute for every carousel item
 *        'itemOptions' => ['class' => 'cat-image'],
 *        // Widget configuration. See example above.
 *        // settings for js plugin
 *        // @see http://kenwheeler.github.io/slick/#settings
 *        'clientOptions' => [
 *                'dots'     => true,
 *                'speed'    => 300,
 *                'autoplay' => true,
 *                'infinite' => false,
 *                'slidesToShow' => 4,
 *                'slidesToScroll' => 4,
 *                'responsive' => [
 *                    [
 *                        'breakpoint' => 1200,
 *                        'settings' => [
 *                            'slidesToShow' => 4,
 *                            'slidesToScroll' => 4,
 *                            'infinite' => true,
 *                            'autoplay' => true,
 *                            'dots' => true,
 *                        ],
 *                    ],
 *                    [
 *                        'breakpoint' => 992,
 *                        'settings' => [
 *                            'slidesToShow' => 4,
 *                            'slidesToScroll' => 4,
 *                            'infinite' => true,
 *                            'autoplay' => true,
 *                            'dots' => true,
 *                        ],
 *                    ],
 *                    [
 *                        'breakpoint' => 768,
 *                        'settings' => [
 *                            'slidesToShow' => 2,
 *                            'slidesToScroll' => 2,
 *                            'infinite' => true,
 *                            'autoplay' => true,
 *                            'dots' => true,
 *                        ],
 *                    ],
 *                    [
 *                        'breakpoint' => 480,
 *                        'settings' => 'unslick', // Destroy carousel, if screen width less than 480px
 *                    ],
 *
 *                ],
 *        ],
 *
 *    ]); ?>
 */

namespace common\assets\oneui\widget\Slider;

use yii\base\Exception;
use yii\base\Widget;
use yii\bootstrap\Html;
use yii\data\DataProviderInterface;
use yii\helpers\Json;
use yii\web\JsExpression;
use yii\web\View;

class Slider extends Widget
{

    /**
     * @var array options to call an event such as "init", "destroy", etc..
     */
    public $events = [];
    /**
     * @var array options to populate Slick jQuery object
     */
    public $clientOptions = [];
    /**
     * @var integer position for inclusion javascript widget code to web page
     * @link http://www.yiiframework.com/doc-2.0/yii-web-view.html#registerJs()-detail
     */
    public $jsPosition = View::POS_READY;
    /**
     * @var array HTML attributes to render on the container
     */
    public $containerOptions = [];
    /**
     * @var string HTML tag to render the container
     */
    public $containerTag = 'div';
    /**
     * @var string HTML tag to render items for the carousel
     */
    public $itemContainer = 'div';
    /**
     * @var array HTML attributes for the one item
     */
    public $itemOptions = [];

    /**
     * @var $dataProfider DataProviderInterface
     */
    public $dataProfider = null;
    /**
     * @var array elements for the carousel
     */
    public $items = [];

    public $viewParams = '';
    /**
     * @inheritdoc
     */
    public function init()
    {
        $this->normalizeOptions();
        // not allowed empty Items
        if(empty($this->items)) {
            throw new Exception('Not allowed without items');
        }
    }
    /**
     * Preparing some options for this widgets
     */
    protected function normalizeOptions()
    {
        // not allowed empty container
        !$this->containerTag && $this->containerTag = 'div';
        if(!isset($this->containerOptions['id']) || empty($this->containerOptions['id'])) {
            $this->containerOptions['id'] = $this->getId();
        }
    }
    /**
     * Register required scripts for the Slick plugin
     */
    protected function registerClientScript()
    {
        $view = $this->getView();
        SliderAsset::register($view);
        $options = Json::encode($this->clientOptions);
        $id = $this->containerOptions['id'];
        $js[] = ";";
        $js[] = "jQuery('#$id').slick($options);";
        $view->registerJs(implode(PHP_EOL, $js), $this->jsPosition);
        foreach ($this->events as $key => $value) {
            $view->registerJs(new JsExpression("$('#".$this->id."').on('".$key."', ".$value.""), $this->jsPosition);
        }
    }
    /**
     * @inheritdoc
     */
    public function run()
    {
        $slider = Html::beginTag($this->containerTag, $this->containerOptions);
//        foreach($this->items as $item) {
//            $slider .= Html::tag($this->itemContainer, $item, $this->itemOptions);
//        }
        $slider .= $this->renderItems();
        $slider .= Html::endTag($this->containerTag);
        echo $slider;
        $this->registerClientScript();
    }

    /**
     * @return null|string
     */
    public function renderItems()
    {
        $slider = null;
        if($this->dataProfider){
            $model = $this->dataProfider->getModels();
            $keys = $this->dataProfider->getKeys();
            foreach (array_values($model) as $index => $model) {
                $key = $keys[$index];
                $item = $this->renderItem($model, $key, $index);
                $slider .= Html::tag($this->itemContainer, $item, $this->itemOptions);
            }

        }else{
            foreach($this->items as $item) {
                $slider .= Html::tag($this->itemContainer, $item, $this->itemOptions);
            }
        }

        return $slider;
    }

    /**
     * @param $model
     * @param $key
     * @param $index
     * @return mixed|null|string
     */
    public function renderItem($model, $key, $index)
    {
        $content = null;
        if(is_string($this->items)){
            $content = $this->getView()->render($this->items, array_merge([
                'model' => $model,
                'key' => $key,
                'index' => $index,
                'widget' => $this,
            ], $this->viewParams));
        }else{
            $content = call_user_func($this->items, $model, $key, $index, $this);
        }

        return $content;
    }


}