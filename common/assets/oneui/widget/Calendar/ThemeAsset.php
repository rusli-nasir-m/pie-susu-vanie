<?php
/**
 * Created by PhpStorm.
 * User: AcenetDev
 * Date: 1/12/2018
 * Time: 3:14 PM
 */

namespace common\assets\oneui\widget\Calendar;


use yii\web\AssetBundle;

class ThemeAsset extends AssetBundle
{

    /**
     * [$depends description]
     * @var array
     */
    public $depends = [
        'yii\jui\JuiAsset'
    ];

}