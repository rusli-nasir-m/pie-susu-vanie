<?php
/**
 * Created by PhpStorm.
 * User: AcenetDev
 * Date: 1/12/2018
 * Time: 2:06 PM
 */
namespace common\assets\oneui\widget\Calendar;

use Yii;
use yii\web\AssetBundle;

class CalendarAsset extends AssetBundle
{

    /**
     * [$sourcePath description]
     * @var string
     */
    public $sourcePath = '@common/assets/oneui/src';
    /**
     * the language the calender will be displayed in
     * @var string ISO2 code for the wished display language
     */
    public $language = NULL;
    /**
     * [$autoGenerate description]
     * @var boolean
     */
    public $autoGenerate = true;
    /**
     * tell the calendar, if you like to render google calendar events within the view
     * @var boolean
     */
    public $googleCalendar = false;

    /**
     * [$css description]
     * @var array
     */
    public $css = [
        'js/plugins/fullcalendar/fullcalendar.min.css',
    ];
    /**
     * [$js description]
     * @var array
     */
    public $js = [
        'js/plugins/fullcalendar/fullcalendar.js',
        'js/plugins/fullcalendar/locale-all.js',
    ];

    /**
     * [$depends description]
     * @var array
     */
    public $depends = [
        'yii\web\YiiAsset',
        'common\assets\oneui\widget\Calendar\MomentAsset',
        'common\assets\oneui\widget\Calendar\PrintAsset'
    ];
    /**
     * @inheritdoc
     */
    public function registerAssetFiles($view)
    {
        $language = $this->language ? $this->language : Yii::$app->language;
//        if (strtoupper($language) != 'EN-US')
//        {
//            $this->js[] = "locale/{$language}.js";
//        }
        if($this->googleCalendar)
        {
            $this->js[] = 'js/plugins/fullcalendar/gcal.js';
        }
        parent::registerAssetFiles($view);
    }

}