<?php
/**
 * Created by PhpStorm.
 * User: AcenetDev
 * Date: 1/12/2018
 * Time: 2:09 PM
 */

namespace common\assets\oneui\widget\Calendar;


use yii\web\AssetBundle;

class PrintAsset extends AssetBundle
{

    /**
     * [$sourcePath description]
     * @var string
     */
    public $sourcePath = '@common/assets/oneui/src';

    /**
     * [$css description]
     * @var array
     */
    public $css = [
        'js/plugins/fullcalendar/fullcalendar.print.css'
    ];
    /**
     * options for the css file beeing published
     * @var [type]
     */
    public $cssOptions = [
        'media' => 'print'
    ];

}