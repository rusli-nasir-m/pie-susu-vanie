App = () ->
  uiInit = ()->

    $lHtml              = jQuery 'html'
    $lBody              = jQuery 'body'
    $lPage              = jQuery '#page-container'
    $lSidebar           = jQuery '#sidebar'
    $lSidebarScroll     = jQuery '#sidebar-scroll'
    $lSideOverlay       = jQuery '#side-overlay'
    $lSideOverlayScroll = jQuery '#side-overlay-scroll'
    $lHeader            = jQuery '#header-navbar'
    $lMain              = jQuery '#main-container'
    $lFooter            = jQuery '#page-footer'

#    Initialize Tooltips
    jQuery('[data-toggle="tooltip"], .js-tooltip').tooltip({
      container: 'body',
      animation: false
    })

#    Initialize Popovers
    jQuery('[data-toggle="popover"], .js-popover').popover({
      container: 'body',
      animation: true,
      trigger: 'hover'
    })

#    Initialize Tabs
    jQuery('[data-toggle="tabs"] a, .js-tabs a').click((e)->
      e.preventDefault()
      jQuery(this).tab('show')
      return
    )
#    Init form placeholder (for IE9)
    jQuery('.form-control').placeholder();

    uiLayout = () ->

      if $lMain.length
        uiHandleMain()

        jQuery(window).on('resize orientationchange', ()->
          clearTimeout($resizeTimeout);
          $resizeTimeout = setTimeout(()->
            uiHandleMain()
            return
          , 150)
        )

      return

    return
  return