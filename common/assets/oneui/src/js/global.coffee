if typeof jQuery == "undefined"
  throw new Error "This App Required Jquery"
else
  $.extend Date.prototype,
    addHours:(h) ->
      @.setHours @.getHours()+h
    addDay: (d) ->
      @.setDay @.getDay()+d
    addMonth: (m) ->
      @.setMonth @.getMonth()+m

if typeof Storage == "undefined"
  throw new Error "Sorry! No Web Storage support.."
else


Date.prototype.addDays = (days) ->
  @.setDate @.getDate() + parseInt(days)

(($)->
  $.fn.enterAsTab = (options) ->

    settings = $.extend('allowSubmit': false,options)
    $(@).find('input, select, textarea, button').on("keydown", {localSettings: settings},(event)->
      if settings.allowSubmit
        type = $(@).attr("type")
        if type == "submit" then return true
      if (event.keyCode == 13)
        inputs = $(@).parents("form").eq(0).find(":input:visible:not(:disabled):not([readonly])")
        idx = inputs.index(@)
        if (idx == inputs.length - 1)
          idx = -1
        else
          inputs[idx + 1].focus() ##* handles submit buttons
        try
          inputs[idx + 1].select()
        catch err
        return false
    )
    return @
)(jQuery)



$(()->
  "use strict"
#  o = $.AdminLTE.options
#  if (o.enableBSToppltip)
#    $('body').tooltip(
#      html:true,
#      selector: "[data-toggle='tooltip-ajax']",
#      container: 'body',
#      content: '... waiting on ajax ...',
#      trigger: "hover",
#      placement: "bottom",
#      template:'<div class="popover" role="tooltip">' +
#        '<div class="arrow"></div>' +
#        '<h3 class="popover-title"></h3>' +
#        '<div class="popover-content"></div>' +
#        '</div>',
#      open: (evt, ui) ->
#        elem = $(@)
#        $.ajax('/echo/html').always(()->
#          elem.tooltip 'option', 'content', 'Ajax call complete'
#          return)
#        return
#    )

#get the click of modal button to create / update item
#we get the button by class not by ID because you can only have one id on a page and you can
#have multiple classes therefore you can have multiple open modal buttons on a page all with or without
#the same link.
#we use on so the dom element can be called again if they are nested, otherwise when we load the content once it kills the dom element and wont let you load anther modal on click without a page refresh
  modal = $("#global-modal");
  $(document).on('click', '.showModalButton',(e)->
    e.preventDefault()
    showModal modal,$(@).attr('title'), $(@).attr('value')
#    modal.find('#modalContent').html "<div style=\"text-align:center\">
#      <i class=\"fa fa-spinner fa-spin fa-3x fa-fw\"></i>
#      <span class=\"sr-only\">Loading...</span></div>"
#
#    header = modal.find('#modalHeader').find('h4')
#    if (header.html())
#      header.html($(@).attr('title'))
#    else
#      modal.find('#modalHeader').append('<h4>' + $(@).attr('title') + '</h4>')
#
#    if (modal.data().isShown)
#      modal.find('#modalContent').load($(@).attr('value'))
##dynamiclly set the header for the modal
#    else
#      modal.modal 'show'
#        .find '#modalContent'
#        .load($(@).attr 'value');

#    modal.draggable(
#      handle: ".modal-header"
#    );
    #dynamiclly set the header for the modal
#    $(modal).on 'submit', 'form', ()->
#      alert "asas"
#      modal.modal 'hide'
#      return
    return
  )
#  $(document).on('click', '.pjax-grid-reload',(e) ->
#    e.preventDefault()
#    $this = $(@)
#    grid = $this.attr('data-grid')
#    $.pjax.reload(container:'#' + grid)
#    $('#'+grid).on('pjax:beforeSend',(e, jqXHR, settings)->
#      $($this).children('i').addClass('fa-spin')
#      return
#    ).on('pjax:end',(e, jqXHR, settings) ->
#      $($this).children('i').removeClass('fa-spin')
#      return
#    )
#    return false
#  )

  return
)

bsWidget =
  alert: (element, message_id, alertType, message)->
    $("#" + element).prepend "<div class=\"alert " + alertType + "\" role=\"alert\" id='" + message_id + "'>" +
        "<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"Close\">" +
        "<span aria-hidden=\"true\">&times;</span>" +
        "</button>" + message + "</div>"

    $("#" + message_id).fadeTo(2000, 500).slideUp(500, ()->
      $("#" + message_id).slideUp 500, () ->
        $(@).remove()
        return
      return
    );
    return

showModal = (modalElement,title,contentURL)->
  modalElement.find('#modalContent').html "<div style=\"text-align:center\">
      <i class=\"fa fa-spinner fa-spin fa-3x fa-fw\"></i>
      <span class=\"sr-only\">Loading...</span></div>"

  header = modalElement.find('#modalHeader').find('h4')
  if (header.html())
    header.html($(@).attr('title'))
  else
    modalElement.find('#modalHeader').append('<h4>' + title + '</h4>')
  if(modalElement.data().isShown )
    modalElement.find('#modalContent').load(contentURL)
#dynamiclly set the header for the modal
  else
    modalElement.modal 'show'
      .find '#modalContent'
      .load(contentURL);
  return

globalSearchModal = (formElement, containerElement, overlayElement) ->

#  console.log(formElement)
#  console.log(containerElement)
#  console.log(overlayElement)
  $(formElement).on('submit',(e)->
    e.preventDefault()
    reloadGrid formElement, containerElement, overlayElement
    return
  )
  return

reloadGrid = (formElement, containerElement, overlayElement) ->
  $(containerElement).on('pjax:beforeSend',(e, jqXHR, settings) ->
    $(overlayElement).toggleClass('hidden')
    $.pjax.defaults.data = $(formElement).serialize()
    return
  ).on('pjax:end',(e, jqXHR, settings) ->
    $(overlayElement).toggleClass('hidden')
    return
  )
  params = $(formElement).serialize()
  console.log containerElement
  $.pjax.reload(
    container: $(containerElement),
    type: 'POST',
    data: params,
  )
  return

detail = (obj,person_id) ->
  a = obj
  td = $(a).parent()
  tr = $(td).parent()
  tdCount = $(tr).children().length
  table = $(tr).parent()
  $(table).children(".trDetail").remove()

  trDetail = document.createElement "tr"
  $(trDetail).attr "class","trDetail"
  tdDetail = document.createElement "td"
  $(tdDetail).attr "colspan",tdCount
  $(tdDetail).html "<span class=\'fa fa-spinner fa-spin\'></span>"

#  get content via ajax
  $.get("'.\yii\helpers\Url::to(['person/detail-person']).'?id="+person_id,(data)->
    $(tdDetail).html( data )
    return
  ).fail(()->
    alert "error"
    return
  )
  $(trDetail).append tdDetail #add td to tr
  $(tr).after trDetail #add tr to table
  return

juiMessage = (output_msg, title_msg, button) ->
  if typeof jQuery.ui == "undefined" then throw new Error "This App Required Jquery UI"
  if not title_msg then title_msg = 'Alert'
  if not output_msg then output_msg = 'No Message to Display.'
  if not button then button =
    "Close":() ->
      $(@).dialog 'close'
      return

  $('#jui_message').html(output_msg).dialog({
    title: title_msg
    dialogClass: 'no-close'
    resizable: false
    modal: true
    buttons:
      button
  })
  return

###*
# Get selected row in grid view
###
getSelectedGrid:(gridId) ->
  if gridId instanceof jQuery then grid = gridId else grid = $("#"+ gridId)
  grid.yiiGridView 'getSelectedRows'

###*
# Module Global Function
###
###*
# reload grid using pjax
###
reloadPjaxGrid = (options) ->
  pjaxGridID = $('#' + options.pjaxGridID)
  pjaxGridID_overlay = $('#' + options.pjaxGridID + '_overlay')
  searchFormID = $('#' + options.searchFormID)
  $.pjax.reload
    container: pjaxGridID
    type: 'POST'
    data: searchFormID.serialize()
  pjaxGridID.on('pjax:beforeSend',(e, jqXHR, settings) ->
    pjaxGridID_overlay.toggleClass('hidden')
    $.pjax.defaults.data = searchFormID.serialize()
    return
  ).on('pjax:end',(e, jqXHR, settings) ->
    pjaxGridID_overlay.toggleClass('hidden')
    return
  ).on('pjax:complete',(event)->
    if options.msg isnt 'undefined' and options.msg != null
      msg = options.msg
      if msg.status isnt 'undefined' and msg.status == "success"
        bsWidget.alert options.pjaxGridID,
          options.controller + "_msgalert", msg.elClass,
          msg.message
        options.msg = null
    return
  )

appLockScreen =(options)->
  autoLockTimer = 0
  goLockScreen = false
  stop = false
  init =
    lockScreen: ->
      stop = true
      window.location.href = options.lockScreenUrl + '?previous=' + encodeURIComponent(window.location.href)
      return
    lockIdentity: ->
      goLockScreen = true
      return
    resetTimer: ->
      if stop == true then
      else if goLockScreen == true
        init.lockScreen()
      else
        clearTimeout(autoLockTimer)
        autoLockTimer = setTimeout(init.lockIdentity, options.limitTimeout)
      return
    run: ->
      window.onload = init.resetTimer
      window.onmousemove = init.resetTimer
      window.onmousedown = init.resetTimer # catches touchscreen presses
      window.onclick = init.resetTimer     # catches touchpad clicks
      window.onscroll = init.resetTimer    # catches scrolling with arrow keys
      window.onkeypress = init.resetTimer
      return
  init.run()
  return
#appLockScreen(ace_global_options)
###*
# Button Checkbox
###
(($)->

#  $(()->
#    $('.search-job').affix(
#      offset: '10px'
#    )
#    return
#  )

  $(document).ready ->
    $('.tongle-button').click ->
      elm = $(this).data('tongle');
#      $('.' + elm).toggleClass('ace-toggled')
      $('.' + elm).slideToggle();
      return
    return

  $('.button-checkbox').each ->
    $widget = $(this)
    $button = $widget.find 'button'
    $checkbox = $widget.find 'input:checkbox'
    color = $button.data 'color'
    settings = {
      on: {icon: 'glyphicon glyphicon-check'},
      off: {icon: 'fa fa-square-o'}
    }
    $button.on 'click', ->
      $checkbox.prop 'checked', !$checkbox.is(':checked')
      $checkbox.triggerHandler 'change'
      updateDisplay()
      return


    $checkbox.on 'change', ->
      updateDisplay()
      return

    updateDisplay = ->
      isChecked = $checkbox.is(':checked')
      ##Set the button's state
      $button.data('state', if isChecked then "on" else "off" );
      ##Set the button's icon
#      console.log(settings);
      $button.find '.state-icon'
      .removeClass()
      .addClass 'state-icon ' + settings[$button.data('state')].icon
      if isChecked
        $button
        .removeClass 'btn-default'
        .addClass 'btn-' + color + ' active'
      else
        $button
        .removeClass 'btn-' + color + ' active'
        .addClass 'btn-default'
      return
    init = ->
      updateDisplay()
      if $button.find('.state-icon').length == 0
        $button.prepend('<i class="state-icon ' + settings[$button.data('state')].icon + '"></i> ')
      return
    init()
    return

    $body = $(document.body)
    navHeight = $('.navbar').outerHeight(true) + 10
    $('.search-job').affix({
      offset:{
        top:() ->
          navOuterHeight = $('#job_list').height()
          @.top = navOuterHeight
        bottom:()->
          @.bottom = $('footer').outerHeight(true)

      }
    })

    $body.scrollspy({
      target: '#leftCol',
      offset: navHeight
    })
  return
)(jQuery)