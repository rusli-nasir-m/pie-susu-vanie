<?php

namespace common\assets\oneui;

use yii\helpers\ArrayHelper;
use yii\web\AssetBundle;

/**
 * Main frontend application asset bundle.
 */
class OneUiAsset extends AssetBundle
{
//    public $basePath = '@webroot';
//    public $baseUrl = '@web';

    public $sourcePath = '@common/assets/oneui/src';
//    public $themes = 'disnaker-front';
    public $themes = '';

    public $css = [
        'http://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400italic,600,700%7COpen+Sans:300,400,400italic,600,700',
        'js/plugins/slick/slick.min.css',
        'js/plugins/slick/slick-theme.min.css',
        'js/plugins/jquery-tags-input/jquery.tagsinput.min.css',

    ];
    public $js = [
        'js/core/jquery.slimscroll.min.js',
        'js/core/jquery.scrollLock.min.js',
        'js/core/jquery.appear.min.js',
        'js/core/jquery.countTo.min.js',
        'js/core/jquery.placeholder.min.js',
        'js/core/js.cookie.min.js',
        'js/plugins/jquery-validation/jquery.validate.min.js',
        'js/plugins/jquery-tags-input/jquery.tagsinput.min.js',
        'js/plugins/bootstrap-maxlength/bootstrap-maxlength.min.js',
        'js/plugins/jquery-vide/jquery.vide.min.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapPluginAsset',
    ];

    public function init(){
        $this->themes = (isset(\Yii::$app->params['oneui-theme']))? \Yii::$app->params['oneui-theme']: $this->themes;
        $themes = ($this->themes)? "css/themes/{$this->themes}.min.css":null;
        if(YII_ENV_PROD) {
            $this->css = ArrayHelper::merge($this->css,[
                'css/app.min.css',
                $themes
            ]);
            $this->js = ArrayHelper::merge($this->js,[
                'js/global.js',
                'js/app.js',
            ]);
        }else{
            $this->css = ArrayHelper::merge($this->css,[
                'css/app.css',
                $themes
            ]);
            $this->js = ArrayHelper::merge($this->js,[
                'js/global.js',
                'js/app.js',
            ]);
        }

        parent::init();
    }
}
