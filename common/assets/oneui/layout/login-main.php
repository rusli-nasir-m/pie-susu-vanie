<?php
/**
 * Created by PhpStorm.
 * User: AcenetDev
 * Date: 12/1/2017
 * Time: 10:55 PM
 */


use common\assets\oneui\widget\Stat;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use common\assets\oneui\OneUiAsset;
use common\widgets\Alert;

$mainAsset = OneUiAsset::register($this);
$baseAssetUrl = $mainAsset->baseUrl;
$this->title = Yii::$app->name;
$storageAsset = \common\assets\StorageAsset::register($this);
$storageAssetBase = $storageAsset->basePath;
$storageAssetUrl = $storageAsset->baseUrl;

$this->params['body-class'] = array_key_exists('body-class', $this->params) ?
    $this->params['body-class']
    : 'bg-image';

$this->params['body-img'] = array_key_exists('body-img',$this->params)?
    $this->params['body-img']:
    Yii::getAlias('@storageUrl/img/bg.jpg');

?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<!--[if IE 9]>         <html class="ie9 no-focus" lang="en"> <![endif]-->
<!--[if gt IE 9]><!--> <html class="no-focus" lang="en"> <!--<![endif]-->

<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <title><?= Html::encode($this->title) ?></title>
    <?= \common\widgets\favicon\Pavicon::widget([
        'web' => '@assetUrl/favicon',
        'webroot' => '@assetCache/favicon',
        'favicon' => "$storageAssetBase/img/logo-disnaker.png",
        'color' => '#2b5797',
        'viewComponent' => 'view',
    ])?>
    <?= Html::csrfMetaTags() ?>
    <?php $this->head() ?>

</head>
<body class="<?= $this->params['body-class']?>" style="background-image: url('<?= $this->params['body-img']?>');" >
<?php $this->beginBody() ?>
<!-- Login Content -->
<div class="content pulldown bg-white overflow-hidden animated fadeIn">
    <?= $content ?>
</div>
<!-- END Login Content -->
<?php $this->endBody() ?>
<?php
yii\bootstrap\Modal::begin([
    'headerOptions' => ['id' => 'modalHeader'],
    'id' => 'global-modal',
    'size' => 'modal-lg',
    //keeps from closing modal with esc key or by clicking out of the modal.
    // user must click cancel or X to close
//        'clientOptions' => ['backdrop' => 'static', 'keyboard' => FALSE]
]);
echo "<div id='modalContent'>
<div style=\"text-align:center\"><i class=\"fa fa-spinner fa-spin fa-3x fa-fw\"></i>
<span class=\"sr-only\">Loading...</span></div>
</div>";
yii\bootstrap\Modal::end();
?>
</body>
</html>
<?php $this->endPage() ?>