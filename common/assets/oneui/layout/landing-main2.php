<?php
/**
 * Created by PhpStorm.
 * User: AcenetDev
 * Date: 12/13/2017
 * Time: 9:03 AM
 */

/* @var $this \yii\web\View */
/* @var $content string */

use rmrevin\yii\fontawesome\FA;
use yii\bootstrap\Html;
use common\assets\oneui\OneUiAsset;
use yii\helpers\Url;

$this->title = Yii::$app->name;
$params = Yii::$app->params;
$mainAsset = OneUiAsset::register($this);
$baseAssetUrl = \yii\helpers\Url::to($mainAsset->baseUrl,true);

$storageAsset = \common\assets\StorageAsset::register($this);
$storageAssetUrl = $storageAsset->basePath;
$storageUrl = $storageAsset->baseUrl;

$this->params['body-class'] = array_key_exists('body-class', $this->params) ?
    $this->params['body-class']
    : null;

$js = <<<JS
$(function(){App.initHelpers(['appear']);});

JS;



$this->registerJs($js);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<!--[if IE 9]>         <html class="ie9 no-focus" lang="en"> <![endif]-->
<!--[if gt IE 9]><!--> <html class="no-focus" lang="en"> <!--<![endif]-->

<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <title><?= Html::encode($this->title) ?></title>
    <?= \common\widgets\favicon\Pavicon::widget([
        'web' => '@assetUrl/favicon',
        'webroot' => '@assetCache/favicon',
        'favicon' => "$storageAssetUrl/img/logo-disnaker.png",
        'color' => '#2b5797',
        'viewComponent' => 'view',
    ])?>
    <?= Html::csrfMetaTags() ?>
    <?php $this->head() ?>

</head>
<body class="<?= $this->params['body-class']?>">
<?php $this->beginBody() ?>

<!-- Page Container -->
<div id="page-container">
    <header id="header-navbar" class="content-mini content-mini-full">
        <!-- Header Navigation Right -->
        <ul class="nav-header pull-right">
            <li class="js-header-search header-search">
                <form class="form-horizontal" action="base_pages_search.html" method="post">
                    <div class="form-material form-material-primary input-group remove-margin-t remove-margin-b">
                        <input class="form-control" type="text" id="base-material-text" name="base-material-text" placeholder="Search..">
                        <span class="input-group-addon"><i class="si si-magnifier"></i></span>
                    </div>
                </form>
            </li>
            <li>
                <div class="btn-group">
                    <button class="btn btn-default btn-image dropdown-toggle" data-toggle="dropdown" type="button">
                        <img src="<?= $baseAssetUrl?>/img/avatars/avatar10.jpg" alt="Avatar">
                        <span class="caret"></span>
                    </button>
                    <ul class="dropdown-menu dropdown-menu-right">
                        <li class="dropdown-header">Profile</li>
                        <li>
                            <a tabindex="-1" href="base_pages_inbox.html">
                                <i class="si si-envelope-open pull-right"></i>
                                <span class="badge badge-primary pull-right">3</span>Inbox
                            </a>
                        </li>
                        <li>
                            <a tabindex="-1" href="base_pages_profile.html">
                                <i class="si si-user pull-right"></i>
                                <span class="badge badge-success pull-right">1</span>Profile
                            </a>
                        </li>
                        <li>
                            <a tabindex="-1" href="javascript:void(0)">
                                <i class="si si-settings pull-right"></i>Settings
                            </a>
                        </li>
                        <li class="divider"></li>
                        <li class="dropdown-header">Actions</li>
                        <li>
                            <a tabindex="-1" href="base_pages_lock.html">
                                <i class="si si-lock pull-right"></i>Lock Account
                            </a>
                        </li>
                        <li>
                            <a tabindex="-1" href="base_pages_login.html">
                                <i class="si si-logout pull-right"></i>Log out
                            </a>
                        </li>
                    </ul>
                </div>
            </li>
        </ul>
        <!-- END Header Navigation Right -->

        <!-- Header Navigation Left -->
        <ul class="nav-header pull-left">
            <li class="visible-xs">
                <!-- Toggle class helper (for .js-header-search below), functionality initialized in App() -> uiToggleClass() -->
                <button class="btn btn-link" data-toggle="class-toggle" data-target=".js-header-search" data-class="header-search-xs-visible" type="button">
                    <i class="fa fa-search"></i>
                </button>
            </li>
        </ul>
        <!-- END Header Navigation Left -->
    </header>
    <!-- Main Container -->
    <main id="main-container">
        <!-- Hero Content -->
        <div class="bg-video" data-vide-bg="<?= $baseAssetUrl?>/img/videos/hero_tech" data-vide-options="posterType: jpg, position: 50% 75%">
            <div class="bg-primary-dark-op">
                <section class="content content-full content-boxed">
                    <!-- Section Content -->
                    <div class="text-center push-30-t visibility-hidden" data-toggle="appear" data-class="animated fadeIn">
                        <a class="fa-2x text-white" href="">
                            <?= Html::img($storageUrl.'/img/logo-disnaker.png',[
                                'height'=> '100px'
                            ])?>
                        </a>
                        <h1 class="h5 font-w100 text-white push-20">Dinas Tenaga Kerja dan Energi Sumber Daya Mineral Provinsi Bali</h1>
                    </div>
                    <div class="push-30-t push-50 text-center">
                        <h1 class="h2 font-w700 text-white push-20 visibility-hidden" data-toggle="appear" data-class="animated fadeInDown">Selamat Datang Dipusat Layanan Bursa Kerja Online Bali.</h1>
                        <h2 class="h4 text-white-op visibility-hidden" data-toggle="appear" data-timeout="750" data-class="animated fadeIn"><em>Hati-Hati Dengan Segala Bentuk Penipuan. Seluruh Bentuk Perekrutan Tenaga Kerja Gratis Tanpa Biaya Apapun.</em></h2>

                        <div class="push-50-t push-20 text-center">
                            <div class="row animated fadeInRight">
                                <div class="col-sm-4">
                                    <a class="block block-link-hover2" href="<?= Url::to(['/job/'])?>">
                                        <div class="block-content block-content-full text-center">
                                            <div>
                                                <?= img("{$storageUrl}/img/icon/icon-jobs-64.png")?>
                                            </div>
                                            <div class="h5 push-15-t push-5">Daftar Lowongan Kerja</div>
                                        </div>
                                    </a>
                                </div>
                                <div class="col-sm-4">
                                    <a class="block block-link-hover2" href="<?= Url::to(['login'])?>">
                                        <div class="block-content block-content-full text-center">
                                            <div>
                                                <i class="si si-login" style="font-size: 64px"></i>
                                            </div>
                                            <div class="h5 push-15-t push-5">Login</div>
                                        </div>
                                    </a>
                                </div>
                                <div class="col-sm-4">
                                    <a class="block block-link-hover2" href="<?= Url::to(['signup'])?>">
                                        <div class="block-content block-content-full text-center">
                                            <div>
                                                <?= img("{$storageUrl}/img/icon/register64.png")?>
                                            </div>
                                            <div class="h5 push-15-t push-5">Registrasi</div>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- END Section Content -->
                </section>
            </div>
        </div>
        <!-- END Hero Content -->
        online: <?php echo Yii::$app->userCounter->getOnline(); ?><br />
        today: <?php echo Yii::$app->userCounter->getToday(); ?><br />
        yesterday: <?php echo Yii::$app->userCounter->getYesterday(); ?><br />
        total: <?php echo Yii::$app->userCounter->getTotal(); ?><br />
        maximum: <?php echo Yii::$app->userCounter->getMaximal(); ?><br />
        date for maximum: <?php echo date('d.m.Y', Yii::$app->userCounter->getMaximalTime()); ?>
    </main>
    <!-- END Main Container -->
    <footer id="page-footer" class="bg-white">
        <div class="content content-boxed">
            <!-- Footer Navigation -->
            <div class="row push-30-t items-push-2x">
                <div class="col-sm-4">
                    <h3 class="h5 font-w600 text-uppercase push-20">Company</h3>
                    <ul class="list list-simple-mini font-s13">
                        <li>
                            <a class="font-w600" href="frontend_home.html">Home</a>
                        </li>
                        <li>
                            <a class="font-w600" href="frontend_features.html">Features</a>
                        </li>
                        <li>
                            <a class="font-w600" href="frontend_pricing.html">Pricing</a>
                        </li>
                        <li>
                            <a class="font-w600" href="frontend_about.html">About Us</a>
                        </li>
                        <li>
                            <a class="font-w600" href="frontend_contact.html">Contact Us</a>
                        </li>
                    </ul>
                </div>
                <div class="col-sm-4">
                    <h3 class="h5 font-w600 text-uppercase push-20">Support</h3>
                    <ul class="list list-simple-mini font-s13">
                        <li>
                            <a class="font-w600" href="frontend_login.html">Log In</a>
                        </li>
                        <li>
                            <a class="font-w600" href="frontend_signup.html">Sign Up</a>
                        </li>
                        <li>
                            <a class="font-w600" href="frontend_support.html">Support Center</a>
                        </li>
                        <li>
                            <a class="font-w600" href="javascript:void(0)">Privacy Policy</a>
                        </li>
                        <li>
                            <a class="font-w600" href="javascript:void(0)">Terms &amp; Conditions</a>
                        </li>
                    </ul>
                </div>
                <div class="col-sm-4">
                    <h3 class="h5 font-w600 text-uppercase push-20">Get In Touch</h3>
                    <div class="font-s13 push">
                        <strong>Company, Inc.</strong><br>
                        980 Folsom Ave, Suite 1230<br>
                        San Francisco, CA 94107<br>
                        <abbr title="Phone">P:</abbr> (123) 456-7890
                    </div>
                    <div class="font-s13">
                        <i class="si si-envelope-open"></i> company@example.com
                    </div>
                </div>
            </div>
            <!-- END Footer Navigation -->

            <!-- Copyright Info -->
            <div class="font-s12 push-20 clearfix">
                <hr class="remove-margin-t">
                <div class="pull-right">
                    Crafted with <i class="fa fa-heart text-city"></i> by <a class="font-w600" href="http://goo.gl/vNS3I" target="_blank">pixelcave</a>
                </div>
                <div class="pull-left">
                    <a class="font-w600" href="http://goo.gl/6LF10W" target="_blank">OneUI 3.1</a> © <span class="js-year-copy">2015-18</span>
                </div>
            </div>
            <!-- END Copyright Info -->
        </div>
    </footer>
</div>
<!-- END Page Container -->

<?php $this->endBody() ?>
<?php
yii\bootstrap\Modal::begin([
    'headerOptions' => ['id' => 'modalHeader'],
    'id' => 'global-modal',
    'size' => 'modal-lg',
    //keeps from closing modal with esc key or by clicking out of the modal.
    // user must click cancel or X to close
//        'clientOptions' => ['backdrop' => 'static', 'keyboard' => FALSE]
]);
echo "<div id='modalContent'>
<div style=\"text-align:center\"><i class=\"fa fa-spinner fa-spin fa-3x fa-fw\"></i>
<span class=\"sr-only\">Loading...</span></div>
</div>";
yii\bootstrap\Modal::end();
?>
</body>
</html>
<?php $this->endPage() ?>
