<?php
/**
 * Created by PhpStorm.
 * User: AcenetDev
 * Date: 12/1/2017
 * Time: 10:52 PM
 */
?>

<?php
/* @var $this \yii\web\View */
/* @var $content string */

use common\assets\oneui\widget\Stat;
use rmrevin\yii\fontawesome\FA;
use yii\bootstrap\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use common\assets\oneui\OneUiAsset;
use common\widgets\Alert;
$params = Yii::$app->params;
$this->title = Yii::$app->name;
$userIdentity = Yii::$app->user->identity->userProfile;
$mainAsset = OneUiAsset::register($this);
$baseAssetUrl = $mainAsset->baseUrl;
$storageAsset = \common\assets\StorageAsset::register($this);
$storageAssetBase = getStoragePath();
$storageAssetUrl = getStorageUrl();
$menuItems = [
    ['label' => 'Home', 'url' => ['/site/index'], 'icon' => 'si si-speedometer'],
    ['label' => 'About', 'url' => ['/site/about']],
    ['label' => 'Contact', 'url' => ['/site/contact']],
    [
        'label' => 'Submenu',  'items' => [
        ['label' => 'Action', 'url' => '#'],
        ['label' => 'Another action', 'url' => '#'],
        ['label' => 'Something else here', 'url' => '#'],
        [
            'label' => 'Submenu',  'items' => [
            ['label' => 'Action', 'url' => '#'],
            ['label' => 'Another action', 'url' => '#'],
            ['label' => 'Something else here', 'url' => '#'],
            [
                'label' => 'Submenu',  'items' => [
                ['label' => 'Action', 'url' => '#'],
                ['label' => 'Another action', 'url' => '#'],
                ['label' => 'Something else here', 'url' => '#'],
                [
                    'label' => 'Submenu',  'items' => [
                    ['label' => 'Action', 'url' => '#'],
                    ['label' => 'Another action', 'url' => '#'],
                    ['label' => 'Something else here', 'url' => '#'],
                ],
                ],
            ],
            ],
        ],
        ],
    ],
    ],
];
$this->params['body-class'] = array_key_exists('body-class', $this->params) ?
    $this->params['body-class']
    : null;
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<!--[if IE 9]>         <html class="ie9 no-focus" lang="en"> <![endif]-->
<!--[if gt IE 9]><!--> <html class="no-focus" lang="en"> <!--<![endif]-->

<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <title><?= Html::encode($this->title) ?></title>
    <?= \common\widgets\favicon\Pavicon::widget([
        'web' => '@assetUrl/favicon',
        'webroot' => '@assetCache/favicon',
        'favicon' => "$storageAssetBase/img/logo-disnaker.png",
        'color' => '#2b5797',
        'viewComponent' => 'view',
    ])?>
    <?= Html::csrfMetaTags() ?>
    <?php $this->head() ?>

</head>
<body class="<?= $this->params['body-class']?>">
<?php $this->beginBody() ?>
<!-- Page Container -->
<!--
    Available Classes:

    'enable-cookies'             Remembers active color theme between pages (when set through color theme list)

    'sidebar-l'                  Left Sidebar and right Side Overlay
    'sidebar-r'                  Right Sidebar and left Side Overlay
    'sidebar-mini'               Mini hoverable Sidebar (> 991px)
    'sidebar-o'                  Visible Sidebar by default (> 991px)
    'sidebar-o-xs'               Visible Sidebar by default (< 992px)

    'side-overlay-hover'         Hoverable Side Overlay (> 991px)
    'side-overlay-o'             Visible Side Overlay by default (> 991px)

    'side-scroll'                Enables custom scrolling on Sidebar and Side Overlay instead of native scrolling (> 991px)

    'header-navbar-fixed'        Enables fixed header
-->
<div id="page-container" class="side-scroll header-navbar-fixed header-navbar-transparent">


    <!-- Header -->
    <header id="header-navbar" class="content-mini content-mini-full">
        <div class="">
            <!-- Header Navigation Right -->
            <ul class="nav-header pull-right">
                <li class="hidden-md hidden-lg">
                    <!-- Toggle class helper (for main header navigation below in small screens), functionality initialized in App() -> uiToggleClass() -->
                    <button class="btn btn-link text-white pull-right" data-toggle="class-toggle" data-target=".js-nav-main-header" data-class="nav-main-header-o" type="button">
                        <i class="fa fa-navicon"></i>
                    </button>
                </li>
                <li class="hidden-md hidden-lg">
                    <!-- Toggle class helper (for .js-header-search below), functionality initialized in App() -> uiToggleClass() -->
                    <button class="btn btn-link text-white pull-right" data-toggle="class-toggle" data-target=".js-header-search" data-class="header-search-xs-visible" type="button">
                        <i class="fa fa-search"></i>
                    </button>
                </li>
            </ul>
            <!-- END Header Navigation Right -->

            <!-- Main Header Navigation -->
            <ul class="js-nav-main-header nav-main-header pull-right">
                <li class="text-right hidden-md hidden-lg">
                    <!-- Toggle class helper (for main header navigation in small screens), functionality initialized in App() -> uiToggleClass() -->
                    <button class="btn btn-link text-white" data-toggle="class-toggle" data-target=".js-nav-main-header" data-class="nav-main-header-o" type="button">
                        <i class="fa fa-times"></i>
                    </button>
                </li>
                <li>
                    <?= Html::a(FA::icon(FA::_HOME) . ' Home',['/'])?>
                </li>
                <li>
                    <?= Html::a(FA::icon(FA::_TASKS) . ' Lowongan Kerja',['/job'])?>
                </li>
                <li>
                    <?= Html::a(FA::icon(FA::_TASKS) . ' Job Fair',['/jobfair'])?>
                </li>
                <li>
                    <?= Html::a(FA::icon(FA::_BUILDING_O) . ' Profil Perusahaan',['/job/employer/'])?>
                </li>
                <li>
                    <?= Html::a(FA::icon(FA::_INFO) . ' Tentang',['/about/'])?>
                </li>
                <?php
                if(Yii::$app->user->isGuest){
                    ?>
                    <li>
                        <?= Html::a('Login',['/site/login'])?>
                    </li>
                    <?php
                }else{
                    ?>
                    <li>
                        <div class="nav-submenu">
                            <button class="btn btn-link btn-image dropdown-toggle" data-toggle="dropdown" type="button" aria-expanded="false">
                                <?= Html::img($storageAssetUrl . '/img/logo-disnaker.png',[
                                    'height'=> '32px'
                                ])?>
                                <?= (!Yii::$app->user->isGuest)? getProfileUserName(): 'Guest'?>
                                <span class="caret"></span>
                            </button>
                        </div>
                        <ul class="bg-white">
                            <li class="dropdown-header">Profile</li>
                            <li>
                                <a tabindex="-1" href="base_pages_inbox.html">
                                    <i class="si si-envelope-open pull-right"></i>
                                    <span class="badge badge-primary pull-right">3</span>Inbox
                                </a>
                            </li>
                            <li>
                                <?= Html::a('<i class="si si-user pull-right"></i> Profile',['/job-seeker/profile'])?>
                            </li>
                            <li>
                                <a tabindex="-1" href="javascript:void(0)">
                                    <i class="si si-settings pull-right"></i>Settings
                                </a>
                            </li>
                            <li class="divider"></li>
                            <li class="dropdown-header">Actions</li>
                            <li>
                                <a tabindex="-1" href="base_pages_lock.html">
                                    <i class="si si-lock pull-right"></i>Lock Account
                                </a>
                            </li>
                            <li>
                                <?= Html::a('<i class="si si-logout pull-right"></i> Logout', ['site/logout'], ['data' => ['method' => 'post']]) ?>
                            </li>
                        </ul>
                    </li>
                    <?php
                }
                ?>
            </ul>

            <!-- END Main Header Navigation -->

            <!-- Header Navigation Left -->
            <ul class="nav-header pull-left">
                <li class="header-content">
                    <?= a(img($storageAssetUrl.'/img/logo-disnaker.png',['height'=> '32px']),['/'],['class' => 'h5'])?>
                </li>
                <li class="header-content">
                    <div id="header-title">
                        <h1 class="h2 text-white animated zoomIn"><?= $this->title?></h1>
                    </div>
                </li>

            </ul>
            <!-- END Header Navigation Left -->
        </div>
    </header>
    <!-- END Header -->

    <!-- Main Container -->
    <main id="main-container">
        <!-- Page Header -->
        <div class="content bg-primary-dark overflow-hidden">
            <div class="col-sm-4 text-left pull-left">
                <div class="push-50-t push-15">
                    <h1 class="h2 text-white animated zoomIn">Bursa Kerja Online</h1>
                    <h2 class="h5 text-white-op animated zoomIn">Dinas Tenaga Kerja dan Energi Sumber Daya Mineral Provinsi Bali</h2>
                </div>
            </div>
            <div class="push-50-t push-15">
                <ul class="nav-header pull-right">
                    <li class="js-header-search header-search">
                        <?= Html::beginForm(['/job/index'],'get',['class'=> 'form-horizontal'])?>
                        <div class="form-material form-material-primary input-group remove-margin-t remove-margin-b">
                            <input class="form-control" type="text" id="key" name="key" placeholder="Search..">
                            <span class="input-group-addon"><i class="si si-magnifier"></i></span>
                        </div>
                        <?= Html::endForm() ?>
                    </li>
                </ul>
            </div>
        </div>
        <!-- END Page Header -->
        <!-- Page Content -->
        <section class="content">
            <?= $content ?>
        </section>
        <!-- END Page Content -->
    </main>
    <!-- END Main Container -->

    <!-- Footer -->
    <footer id="page-footer" class="bg-white">
        <div class="content content-boxed">
            <!-- Footer Navigation -->
            <div class="row push-30-t items-push-2x">
                <div class="col-sm-8">
                    <h3 class="h5 font-w600 text-uppercase push-20">Hubungi Kami</h3>
                    <table>
                        <tr>
                            <td width="100px">Alamat</td>
                            <td><?= getConfig('alamat')?></td>
                        </tr>
                        <tr>
                            <td>No Telp</td>
                            <td><?= getConfig('info_phone')?></td>
                        </tr>
                        <tr>
                            <td>Email</td>
                            <td><?= getConfig('support_email')?></td>
                        </tr>
                        <tr>
                            <td>Pengaduan</td>
                            <td><?= a('Link Pengaduan',['/site/contact'])?></td>
                        </tr>

                    </table>
                </div>
                <div class="col-sm-4">
                    <h3 class="h5 font-w600 text-uppercase push-20">Statistik Pengunjung</h3>
                    <div class="font-s12 push">
                        <i class="si si-users"></i> Online <?php echo Yii::$app->userCounter->getOnline(); ?><br>
                        <i class="si si-users"></i> Saat Ini <?php echo Yii::$app->userCounter->getToday(); ?><br>
                        <i class="si si-users"></i> Kemarin <?php echo Yii::$app->userCounter->getYesterday(); ?><br>
                        <i class="fa fa-calendar"></i> Bulan Ini <?php echo Yii::$app->userCounter->getThisMonth(); ?><br>
                        <i class="si si-calendar"></i> Bulan Lalu <?php echo Yii::$app->userCounter->getLastMonth(); ?><br>
                        <i class="si si-users"></i> <b>Total Pengunjung <?php echo Yii::$app->userCounter->getTotal(); ?></b><br>
                        <hr>
                        <i class="si si-users"></i> Tgl Terbanyak <?php echo formatter()->asDate(Yii::$app->userCounter->getMaximalTime()); ?><br>
                        <i class="si si-users"></i> Terbanyak <?php echo Yii::$app->userCounter->getMaximal(); ?><br>
                    </div>
                </div>
            </div>
            <!-- END Footer Navigation -->
        </div>
        <div class="content-mini content-mini-full font-s12 bg-gray-lighter clearfix">
            <div class="pull-right">
                version <?= getConfig('app_version')?>
            </div>
            <div class="pull-left">
                <a class="font-w600" href="http://www.acenet.net.id" target="_blank"><?= getConfig('app_name')?></a> &copy; <span class="js-year-copy"></span>
            </div>
        </div>
    </footer>
    <!-- END Footer -->
</div>
<!-- END Page Container -->

<?php $this->endBody() ?>
<?php
yii\bootstrap\Modal::begin([
    'headerOptions' => ['id' => 'modalHeader'],
    'id' => 'global-modal',
    'size' => 'modal-lg',
    //keeps from closing modal with esc key or by clicking out of the modal.
    // user must click cancel or X to close
//        'clientOptions' => ['backdrop' => 'static', 'keyboard' => FALSE]
]);
echo "<div id='modalContent'>
<div style=\"text-align:center\"><i class=\"fa fa-spinner fa-spin fa-3x fa-fw\"></i>
<span class=\"sr-only\">Loading...</span></div>
</div>";
yii\bootstrap\Modal::end();
?>
<a id="back-to-top" href="#" class="btn btn-primary btn-lg back-to-top" role="button" title="Click to return on the top page" data-toggle="tooltip" data-placement="left"><span class="glyphicon glyphicon-chevron-up"></span></a>
</body>
</html>
<?php $this->endPage() ?>
