<?php

namespace common\assets;

use yii\helpers\ArrayHelper;
use yii\web\AssetBundle;

/**
 * Main frontend application asset bundle.
 */
class StorageAsset extends AssetBundle
{
    public $sourcePath = '@storage/web';

}
