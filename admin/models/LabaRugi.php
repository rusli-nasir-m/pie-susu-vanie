<?php
/**
 * Created by PhpStorm.
 * User: ASUS
 * Date: 20/04/2018
 * Time: 21:15
 * Project: pie-susu-vanie
 */

namespace admin\models;


use common\models\Jurnal;
use common\models\JurnalDetail;
use yii\base\Model;

class LabaRugi extends Model
{

    public $dari_tanggal;
    public $sampai_tanggal;
    
    
    public function rules()
    {
        return [
            [['dari_tanggal','sampai_tanggal'],'safe'],
            [['dari_tanggal','sampai_tanggal'],'required'],
        ];
    }

    public function getLabarugi()
    {
        $query = JurnalDetail::find();
        $query->innerJoin('{{%jurnal}} as j ','j.id = {{%jurnal_detail}}.id_jurnal');
        if($this->dari_tanggal && $this->sampai_tanggal){
            $query->where(['between','j.tanggal',$this->dari_tanggal,$this->sampai_tanggal]);
        }elseif(!$this->dari_tanggal && $this->sampai_tanggal){
            $query->where(['>=','j.tanggal',$this->dari_tanggal]);
        }elseif($this->dari_tanggal && !$this->sampai_tanggal) {
            $query->where(['<=','j.tanggal',$this->sampai_tanggal]);
        }
        return $query->all();
    }

}