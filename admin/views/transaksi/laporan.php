<?php
/**
 * Created by PhpStorm.
 * User: ASUS
 * Date: 19/04/2018
 * Time: 16:01
 * Project: pie-susu-vanie
 *
 * @var $this \yii\web\View
 * @var $searchModel \common\models\Transaksi
 */

use yii\bootstrap\ActiveForm;
use yii\bootstrap\Html;

$js = <<<JS
$('#btn-print').on('click',function() {
  window.print();
});

$('#transaksisearch-tanggal').datepicker({
  autoclose: true,
  format: 'yyyy-mm-dd'
});
JS;
$this->registerJs($js);
?>

<h1 class="text-center">Laporan Transaksi</h1>
<div class="hidden-print">
    <?php $form = ActiveForm::begin([
        'options' => ['enctype' => 'multipart/form-data'],
    ]); ?>
    <div class="row">
        <div class="col-md-6">
            <div class="row">
                <div class="col-md-6">
                    <?= $form->field($searchModel, 'no_bukti')->textInput(['maxlength' => true]) ?>
                </div>
                <div class="col-md-6">
                    <?= $form->field($searchModel, 'tanggal')->textInput() ?>
                </div>
            </div>
            <?= $form->field($searchModel, 'keterangan_transaksi')->textarea(['rows' => 3]) ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($searchModel, 'total_transaksi')->textInput(['maxlength' => true]) ?>
            <?= $form->field($searchModel, 'tipe_transaksi')->dropDownList([ 'Keluar' => 'Keluar', 'Masuk' => 'Masuk', ], ['prompt' => '--Tipe Transaksi']) ?>
            <?= $form->field($searchModel, 'status')->dropDownList([ 'Lunas' => 'Lunas', 'Belum Lunas' => 'Belum Lunas', ], ['prompt' => '--Status Transaksi']) ?>
            <div class="form-group">
                <?= Html::submitButton(Yii::t('app', 'Cari'), ['class' => 'btn btn-success']) ?>
            </div>
        </div>
    </div>

    <?php ActiveForm::end(); ?>
    <p>
        <?= \yii\bootstrap\Html::button('Cetak',['class' => 'btn btn-info','id' => 'btn-print'])?>
        <?= a('Kembali',['index'],['class' => 'btn btn-warning'])?>
    </p>
</div>
<?= \yii\grid\GridView::widget([
    'dataProvider' => $dataProvider,
    'emptyText' => '-',
    'sorter' => false,
    'showFooter' => true,
    'columns' => [
        [
            'class' => 'yii\grid\SerialColumn',
            'footer' => '<b>Total</b>',
            'footerOptions' => [
                'colspan' => 5,
                'class' => 'text-center'
            ]
        ],[
            'attribute' => 'id_transaksi',
            'footerOptions' => ['style' => 'display:none']
        ],[
            'attribute' => 'no_bukti',
            'footerOptions' => ['style' => 'display:none']
        ],[
            'attribute' => 'tanggal',
            'format' => 'date',
            'footerOptions' => ['style' => 'display:none']
        ],[
            'attribute' => 'keterangan_transaksi',
            'footerOptions' => ['style' => 'display:none']
        ],[
            'attribute' => 'total_transaksi',
            'format' => 'currency',
            'footer' => '<b>'.\common\models\Transaksi::getTotal($dataProvider->models,'total_transaksi',true). '</b>',
            'footerOptions' => [
                'colspan' => 3,
                'class' => 'text-center'
            ]
        ],[
            'attribute' => 'tipe_transaksi',
            'footerOptions' => ['style' => 'display:none']
        ],[
            'attribute' => 'status',
            'footerOptions' => ['style' => 'display:none']
        ],
    ],
])?>