<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel admin\models\TransaksiSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Transaksis');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="transaksi-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app', 'Create Transaksi'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'id_transaksi',
            'no_bukti',
            'tanggal:date',
            'keterangan_transaksi',
            'total_transaksi:currency',
            [
				'attribute' => 'tipe_transaksi',
				'filter' => [
					'Masuk' => 'Masuk',
					'Keluar' => 'Keluar',
				]
			],
            [
				'attribute' => 'status',
				'filter' => [
					'Lunas' => 'Lunas',
					'Belum Lunas' => 'Belum Lunas',
				]
			],           
            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
