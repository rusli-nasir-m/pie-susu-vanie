<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Transaksi */
/* @var $form yii\widgets\ActiveForm */

$js = <<<JS
$('#transaksi-tanggal').datepicker({
  autoclose: true,
  format: 'yyyy-mm-dd'
});
JS;
$this->registerJs($js);
?>

<div class="transaksi-form">

    <?php $form = ActiveForm::begin(); ?>
    <?= $form->field($model, 'no_bukti')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'tanggal')->textInput() ?>
    <?= $form->field($model, 'keterangan_transaksi')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'total_transaksi')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'status')->dropDownList([ 'Lunas' => 'Lunas', 'Belum Lunas' => 'Belum Lunas', ], ['prompt' => '--Status Transaksi']) ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
