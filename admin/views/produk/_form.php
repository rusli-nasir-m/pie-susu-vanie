<?php

use common\models\KategoriProduk;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Produk */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="produk-form">

    <?php $form = ActiveForm::begin([
        'options' => ['enctype' => 'multipart/form-data'],
    ]); ?>

    <?= $form->field($model, 'id_produk')->textInput(['maxlength' => true]) ?>
	
    <?php 
	$kategori = ($model->isNewRecord)? KategoriProduk::find()->where(['status' => 1])->all(): KategoriProduk::find()->all();
	echo $form->field($model, 'id_kategori')
		->dropDownList(ArrayHelper::map($kategori,'id_kategori','nama_kategori'))
        ->label('Kategori Produk'); 
	?>

    <?= $form->field($model, 'nama')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'harga')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'size')->dropDownList(param('size')) ?>

    <?= $form->field($model, 'status')->dropDownList(param('status')) ?>
    <?= $form->field($model, 'images')->fileInput() ?>

    <?= $form->field($model, 'deskripsi')->textarea(['rows' => 6]) ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
