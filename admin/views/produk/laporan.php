<?php
/**
 * Created by PhpStorm.
 * User: ASUS
 * Date: 19/04/2018
 * Time: 16:01
 * Project: pie-susu-vanie
 *
 * @var $this \yii\web\View
 * @var $searchModel \common\models\Produk
 */


use common\models\KategoriProduk;
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Html;
use yii\helpers\ArrayHelper;

$js = <<<JS
$('#btn-print').on('click',function() {
  window.print();
})
JS;
$this->registerJs($js);
?>

<h1 class="text-center">Laporan Daftar Produk</h1>
<div class="hidden-print">
    <?php $form = ActiveForm::begin([
        'options' => ['enctype' => 'multipart/form-data'],
    ]); ?>
    <div class="row">
        <div class="col-md-6">
            <?= $form->field($searchModel, 'id_kategori')
                ->dropDownList(ArrayHelper::map(KategoriProduk::find()->all(),'id_kategori','nama_kategori'),[
                    'prompt' => '--Pilih Kategori--'
                ])
                ->label('Kategori Produk') ?>
            <?= $form->field($searchModel, 'nama')->textInput(['maxlength' => true]) ?>
            <?= $form->field($searchModel, 'harga')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($searchModel, 'size')->dropDownList(param('size'),[
                'prompt' => '--Pilih Size--'
            ]) ?>
            <?= $form->field($searchModel, 'status')->dropDownList(param('status'),[
                'prompt' => '--Pilih Status--'
            ]) ?>
        </div>
        <?= ''//$form->field($searchModel, 'id_produk')->textInput(['maxlength' => true]) ?>
        <div class="form-group">
            <?= Html::submitButton(Yii::t('app', 'Cari'), ['class' => 'btn btn-success']) ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>
    <p>
        <?= \yii\bootstrap\Html::button('Cetak',['class' => 'btn btn-info','id' => 'btn-print'])?>
        <?= a('Kembali',['index'],['class' => 'btn btn-warning'])?>
    </p>
</div>
<?= \yii\grid\GridView::widget([
    'dataProvider' => $dataProvider,
    'emptyText' => '-',
    'sorter' => false,
    'columns' => [
        'id_produk:text:ID Produk',
        'kategori.nama_kategori:text:Kategori',
        'nama',
        [
            'attribute' => 'harga',
            'format' => 'currency',
            'contentOptions' => ['class' => 'text-right']
        ],
        [
            'attribute' => 'size',
            'value' => function($model){
                return $model->size? \yii\helpers\ArrayHelper::getValue(param('size'),$model->size):null;
            }
        ],
        [
            'attribute' => 'status',
            'value' => function($model){
                return $model->status? \yii\helpers\ArrayHelper::getValue(param('status'),$model->status):null;
            }
        ],
        'deskripsi',

    ]
])?>