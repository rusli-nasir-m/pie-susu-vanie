<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel admin\models\ProdukSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Produks');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="produk-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app', 'Create Produk'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id_produk',
            'kategori.nama_kategori',
            'nama',
            [
                'attribute' => 'harga',
                'format' =>'raw',
                'value' => function($model){
                    return ($model->harga)?number_format($model->harga,2):null;
                }
            ],
            [
                'attribute' => 'size',
                'format' =>'raw',
                'value' => function($model){
                    return \yii\helpers\ArrayHelper::getValue(param('size'),$model->size);
                }
            ],
            [
                'attribute' => 'status',
                'format' =>'raw',
                'value' => function($model){
                    return \yii\helpers\ArrayHelper::getValue(param('status'),$model->status);
                }
            ],
            [
                'attribute' => 'images',
                'format' => 'raw',
                'value' => function($model){
                    return Html::img($model->getThumbUploadUrl('images'), ['class' => 'img-thumbnail']);
                }
            ],
            //'deskripsi:ntext',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
