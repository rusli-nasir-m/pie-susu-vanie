<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Pelanggan */

$this->title = Yii::t('app', 'Update Pelanggan: {nameAttribute}', [
    'nameAttribute' => $model->id_pelanggan,
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Pelanggans'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id_pelanggan, 'url' => ['view', 'id' => $model->id_pelanggan]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="pelanggan-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
