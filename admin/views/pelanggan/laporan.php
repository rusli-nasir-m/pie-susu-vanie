<?php
/**
 * Created by PhpStorm.
 * User: ASUS
 * Date: 19/04/2018
 * Time: 16:01
 * Project: pie-susu-vanie
 *
 * @var $this \yii\web\View
 * @var $searchModel \common\models\Pelanggan
 */


use yii\bootstrap\Html;

$js = <<<JS
$('#btn-print').on('click',function() {
  window.print();
})
JS;
$this->registerJs($js);
?>

<h1 class="text-center">Laporan Daftar Pelanggan</h1>
<div class="hidden-print">
    <?php
    $form = \yii\bootstrap\ActiveForm::begin();
    ?>
    <div class="row">
        <div class="col-md-6">
            <?= $form->field($searchModel,'nama');?>
            <?= $form->field($searchModel,'alamat')->textarea();?>
            <?= $form->field($searchModel,'email');?>
        </div>
        <div class="col-md-6">
            <?= $form->field($searchModel,'kota');?>
            <?= $form->field($searchModel,'provinsi');?>
            <?= $form->field($searchModel,'kode_pos');?>
        </div>
        <div class="form-group">
            <?= Html::submitButton(Yii::t('app', 'Cari'), ['class' => 'btn btn-success']) ?>
        </div>
    </div>
    <?php \yii\bootstrap\ActiveForm::end();?>
    <p>
        <?= \yii\bootstrap\Html::button('Cetak',['class' => 'btn btn-info','id' => 'btn-print'])?>
        <?= a('Kembali',['index'],['class' => 'btn btn-warning'])?>
    </p>
</div>
<?= \yii\grid\GridView::widget([
    'dataProvider' => $dataProvider,
    'sorter' => false,
    'columns' => [
        'id_pelanggan',
        'tgl_registrasi:date:Tanggal Registrasi',
        'nama',
        'alamat',
        'no_telp',
        'email',
        'kota',
        'provinsi',
        'kode_pos',
    ]
])?>