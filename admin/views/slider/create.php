<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\ImageSlider */

$this->title = Yii::t('app', 'Create Image Slider');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Image Sliders'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="image-slider-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
