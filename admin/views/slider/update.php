<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\ImageSlider */

$this->title = Yii::t('app', 'Update Image Slider: {nameAttribute}', [
    'nameAttribute' => $model->id_slider,
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Image Sliders'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id_slider, 'url' => ['view', 'id' => $model->id_slider]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="image-slider-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
