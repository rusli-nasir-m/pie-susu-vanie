<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Saran */

$this->title = Yii::t('app', 'Update Saran: {nameAttribute}', [
    'nameAttribute' => $model->id_saran,
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Sarans'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id_saran, 'url' => ['view', 'id' => $model->id_saran]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="saran-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
