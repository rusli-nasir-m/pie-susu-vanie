<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model admin\models\SaranSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="saran-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id_saran') ?>

    <?= $form->field($model, 'nama') ?>

    <?= $form->field($model, 'mobile_phone') ?>

    <?= $form->field($model, 'tanggal') ?>

    <?= $form->field($model, 'email') ?>

    <?php // echo $form->field($model, 'saran') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
