<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Saran */

$this->title = $model->id_saran;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Sarans'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="saran-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id_saran], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id_saran], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id_saran',
            'nama',
            'mobile_phone',
            'tanggal',
            'email:email',
            'saran:ntext',
        ],
    ]) ?>

</div>
