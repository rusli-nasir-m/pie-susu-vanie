<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $searchModel admin\models\KategoriProdukSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Kategori Produks');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="kategori-produk-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app', 'Create Kategori Produk'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id_kategori',
            'nama_kategori',
			[
				'attribute'=> 'status',
				'value' => function($model){
					return ArrayHelper::getValue(param('status_aktif'), $model->status);
				}
			],
            [
				'class' => 'yii\grid\ActionColumn',
				'buttons' => [
					'delete' => function ($url, $model, $key) {
						$label = $model->status ? 'Non Aktifkan': 'Aktifkan';
						$icon = Html::tag('span', '', ['class' => "glyphicon glyphicon-" . ($model->status? 'remove':'ok')]);
						return a($icon, $url,[
							'title' => $label,
							'data' => [
								'pjax' => 0,
								'confirm' => Yii::t('app','Yakin akan merubah status?'),
								'method' => 'POST'
							]
						]);
					},
				],
				
			],
        ],
    ]); ?>
</div>
