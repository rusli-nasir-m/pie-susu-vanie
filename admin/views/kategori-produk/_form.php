<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\KategoriProduk */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="kategori-produk-form">

    <?php $form = ActiveForm::begin(); ?>
    <?= $form->field($model, 'id_kategori')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'nama_kategori')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'status')->dropDownList(param('status_aktif')) ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
