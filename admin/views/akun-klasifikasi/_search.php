<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model admin\models\AkunKlasifikasiSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="akun-klasifikasi-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'klasifikasi') ?>

    <?= $form->field($model, 'deskripsi') ?>

    <?= $form->field($model, 'normal') ?>

    <?= $form->field($model, 'inisial') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
