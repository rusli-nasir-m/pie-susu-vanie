<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\AkunKlasifikasi */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="akun-klasifikasi-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'klasifikasi')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'deskripsi')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'normal')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'inisial')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
