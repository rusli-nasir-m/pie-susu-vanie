<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\AkunKlasifikasi */

$this->title = Yii::t('app', 'Create Akun Klasifikasi');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Akun Klasifikasis'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="akun-klasifikasi-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
