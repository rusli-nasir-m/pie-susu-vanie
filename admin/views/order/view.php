<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Order */

$this->title = $model->id_order;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Orders'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

$dataProvider = new \yii\data\ActiveDataProvider([
    'query' => $model->getDetailOrders(),
    'sort' => false,
    'pagination' => false
])
?>
<div class="order-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id_order], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Validasi Order'), ['validasi', 'id' => $model->id_order], ['class' => 'btn btn-warning']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id_order], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id_order',
            'pelanggan.nama:text:Pelanggan',
            'tgl_order:date',
            'total_harga:currency:Total Transaksi',
            [
                'attribute' => 'bukti_pembayaran',
                'format' => 'raw',
                'value' => function($model){
                    return ($model->bukti_pembayaran)? img($model->getThumbUploadUrl('bukti_pembayaran'),['class'=>'img-thumbnail', 'width' => '150px']):null;
                }
            ],
            'no_rekening',
            'bank',
            [
                'attribute' => 'metode_pembayaran',
                'value' => function($model){
                    return \yii\helpers\ArrayHelper::getValue(param('metode_pembayaran'),$model->metode_pembayaran);
                }
            ],
            'nama_akun',
            [
                'attribute' => 'status_pembayaran',
                'label' => 'Status Pembayaran',
                'value' => function($model){
                    return ($model->status_pembayaran)? 'Berhasil':'Belum Konfirmasi';
//                                $validasi = ($model->validasi)? 'Sudah Validasi':'Belum Validasi';
                }
            ],
            [
                'attribute' => 'validasi',
                'value' => function($model){
                    return ($model->validasi)? 'Sudah Validasi':'Belum Validasi';
                }
            ],
        ],
    ]) ?>

</div>
<div class="row">
    <div class="col-md-12">
        <?= \yii\grid\GridView::widget([
            'dataProvider' => $dataProvider,
            'layout' => '{items}',
            'columns' => [
                'produk.nama:text:Nama Produk',
                'harga:currency',
                'jumlah:decimal:Qty',
                [
                    'label' => 'Sub Total',
                    'format' => 'currency',
                    'value' => function($model){
                        return $model->harga * $model->jumlah;
                    }
                ]
            ]
        ])?>
    </div>
</div>