<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model admin\models\OrderSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="order-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id_order') ?>

    <?= $form->field($model, 'id_pelanggan') ?>

    <?= $form->field($model, 'tgl_order') ?>

    <?= $form->field($model, 'total_harga') ?>

    <?= $form->field($model, 'bukti_pembayaran') ?>

    <?php // echo $form->field($model, 'no_rekening') ?>

    <?php // echo $form->field($model, 'bank') ?>

    <?php // echo $form->field($model, 'metode_pembayaran') ?>

    <?php // echo $form->field($model, 'nama_akun') ?>

    <?php // echo $form->field($model, 'status_pembayaran') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
