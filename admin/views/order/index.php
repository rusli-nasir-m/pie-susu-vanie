<?php

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\grid\GridView;
/* @var $this yii\web\View */
/* @var $searchModel admin\models\OrderSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
$this->title = Yii::t('app', 'Orders');
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="order-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
       <? = Html::a(Yii::t('app', 'Create Order'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'id_order',[
                'attribute' => 'id_pelanggan',
                'format' => 'raw',
                'value' => function($model){
                    $namaPelanggan = $model->id_pelanggan? $model->pelanggan->nama:null;
                    return nl2br("{$model->id_pelanggan}\n{$namaPelanggan}");
                }
            ],
            'tgl_order:date',
            [
                'attribute' => 'total_harga',
                'contentOptions' => ['class' =>'text-right'],
                'format' => 'currency'
            ],[
                'attribute' => 'bukti_pembayaran',
                'format' => 'raw',
                'value' => function($model){
                    $image = ($model->bukti_pembayaran)? img($model->getThumbUploadUrl('bukti_pembayaran'),['class'=>'img-thumbnail', 'width' => '100px']) . "\n":null;
                    $status = $model->metode_pembayaran? ArrayHelper::getValue(param('metode_pembayaran'),$model->metode_pembayaran)."\n":null;
                    $tglBayar = $model->tgl_pembayaran? formatter()->asDate($model->tgl_pembayaran):null;
                    return nl2br("{$image}{$status}{$tglBayar}");
                }
            ],[
                'attribute' => 'validasi',
                'format' => 'raw',
                'value' => function($model){
                    $isValid = $model->validasi? "<span class='label label-success'>Sudah validasi</span>":"<span class='label label-warning'>Belum validasi</span>";
                    $tglValidasi = formatter()->asDate($model->tanggal_validasi);
                    return nl2br("{$isValid}\n{$tglValidasi}");
                }
            ],
            //'no_rekening',
            //'bank',
            //'metode_pembayaran',
            //'nama_akun',
            //'status_pembayaran',
			
            [
                'class' => 'yii\grid\ActionColumn',
                'headerOptions' => ['width' => '70px'],
                'header' => 'Aksi',
                'buttons' => [
                    'delete' => function($url, $model, $key){
                        $button = a("<span class=\"glyphicon glyphicon-trash\"></span>",[$url],[
                            'title' => 'Hapus',
                            'data' => [
                                'pjax' => 0,
                                'method' => 'post',
                                'confirm'=> "Apakah Anda yakin ingin menghapus item ini?"
                            ]
                        ]);

                        return !$model->validasi? $button:null;
                    }
                ]
            ],
        ],
    ]); ?>
</div>
