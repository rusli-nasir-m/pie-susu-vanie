<?php
/**
 * Created by PhpStorm.
 * User: ASUS
 * Date: 19/04/2018
 * Time: 16:01
 * Project: pie-susu-vanie
 *
 * @var $this \yii\web\View
 * @var $searchModel \common\models\Order
 */


use common\models\KategoriProduk;
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Html;
use yii\helpers\ArrayHelper;

$js = <<<JS
$('#btn-print').on('click',function() {
  window.print();
})

$('#ordersearch-tgl_order').datepicker({
  autoclose: true,
  format: 'yyyy-mm-dd'
});
JS;
$this->registerJs($js);
?>

<h1 class="text-center">Laporan Daftar Order</h1>
<div class="hidden-print">
    <?php $form = ActiveForm::begin([
        'options' => ['enctype' => 'multipart/form-data'],
    ]); ?>
    <div class="row">
        <div class="col-md-6">
            <?= $form->field($searchModel, 'id_order')->textInput(['maxlength' => true]) ?>
            <?= $form->field($searchModel, 'id_pelanggan')->dropDownList(ArrayHelper::map(\common\models\Pelanggan::find()->all(),'id_pelanggan','nama'),[
                'prompt' => '--Pilih Pelanggan--'
            ])->label('Pealanggan') ?>
            <?= $form->field($searchModel, 'tgl_order')->textInput(['maxlength' => true]) ?>
            <?= $form->field($searchModel, 'no_rekening')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($searchModel, 'bank')->textInput(['maxlength' => true]) ?>
            <?= $form->field($searchModel, 'metode_pembayaran')->dropDownList(param('metode_pembayaran'),[
                'prompt' => '--Pilih Metode Pembayaran--'
            ]) ?>
            <?= $form->field($searchModel, 'nama_akun')->textInput(['maxlength' => true]) ?>
            <?= $form->field($searchModel, 'status_pembayaran')->dropDownList([
                0 => 'Belum Bayar',
                1 => 'Sudah Bayar',
            ],[
                'prompt' => '--Pilih Status Pembayaran--'
            ]) ?>
            <div class="form-group">
                <?= Html::submitButton(Yii::t('app', 'Cari'), ['class' => 'btn btn-success']) ?>
            </div>
        </div>
    </div>

    <?php ActiveForm::end(); ?>
    <p>
        <?= \yii\bootstrap\Html::button('Cetak',['class' => 'btn btn-info','id' => 'btn-print'])?>
        <?= a('Kembali',['index'],['class' => 'btn btn-warning'])?>
    </p>
</div>
<?= \yii\grid\GridView::widget([
    'dataProvider' => $dataProvider,
    'emptyText' => '-',
    'sorter' => false,
    'columns' => [
        'id_order',
        'tgl_order:date',
        [
            'attribute' => 'id_pelanggan',
            'label' => 'Pelanggan',
            'format' => 'raw',
            'value' => function($model){
                $pelanggan =  $model->id_pelanggan? $model->pelanggan->nama:null;
                $alamat =  $model->id_pelanggan? $model->pelanggan->alamat:null;
                return "<p>{$model->id_pelanggan}<br><small>$pelanggan</small></p>";
                return "<dl class=\"dl-horizontal\">
                    <dt>ID Pelanggan</dt>
                    <dd>{$model->id_pelanggan}</dd>
                    <dt>Pelanggan</dt>
                    <dd>{$pelanggan}</dd>
                    <dt>Alamat</dt>
                    <dd>{$alamat}</dd>                                        
                </dl>";
            }
        ],
        'total_harga:currency',
        [
            'attribute' => 'bukti_pembayaran',
            'label' => 'Pembayaran',
            'format' => 'raw',
            'value' => function($model){
                $metode_bayar = ArrayHelper::getValue(param('metode_pembayaran'),$model->metode_pembayaran);
                return "<dl class=\"dl-horizontal\">
                    <dt>Akun</dt>
                    <dd>{$model->nama_akun}</dd>
                    <dt>No Rek</dt>
                    <dd>{$model->no_rekening}</dd>
                    <dt>Bank</dt>
                    <dd>{$model->bank}</dd>
                    <dt>Metode Pembayaran</dt>
                    <dd>{$metode_bayar}</dd>                    
                </dl>";
            }
        ],
        [
            'attribute' => 'validasi',
            'format' => 'raw',
            'value' => function($model){
                $tglValidasi = formatter()->asDate($model->tanggal_validasi);
                $validasi = $model->validasi? 'Sudah Validasi': 'Belum Validasi';
                return "<dl class=\"dl-horizontal\">
                    <dt>Status Validasi</dt>
                    <dd>{$validasi}</dd>
                    <dt>Tgl Validasi</dt>
                    <dd>{$tglValidasi}</dd>                                        
                </dl>";
            }
        ]
    ]
])?>