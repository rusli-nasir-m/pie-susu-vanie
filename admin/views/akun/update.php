<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Akun */

$this->title = Yii::t('app', 'Update Akun: {nameAttribute}', [
    'nameAttribute' => $model->kode_rekening,
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Akuns'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->kode_rekening, 'url' => ['view', 'id' => $model->kode_rekening]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="akun-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
