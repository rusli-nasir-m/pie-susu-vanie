<?php
/**
 * Created by PhpStorm.
 * User: ASUS
 * Date: 16/04/2018
 * Time: 16:46
 * Project: pie-susu-vanie
 *
 * @var $this \yii\web\View
 */

use yii\helpers\Url;

$this->title = "Daftar Laporan"
?>

<div class="row">
    <div class="col-lg-3 col-md-6">
        <a href="<?= Url::to(['/produk/laporan'])?>">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <div class="row">
                    <div class="col-xs-3">
                        <i class="fa fa-list-alt fa-5x"></i>
                    </div>
                    <div class="col-xs-9 text-right">
                        <div>Daftar Produk</div>
                    </div>
                </div>
            </div>
        </div>
        </a>
    </div>
    <div class="col-lg-3 col-md-6">
        <a href="<?= Url::to(['/pelanggan/laporan'])?>">
            <div class="panel panel-green">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-xs-3">
                            <i class="fa fa-users fa-5x"></i>
                        </div>
                        <div class="col-xs-9 text-right">
                            <div>Daftar Pelanggan</div>
                        </div>
                    </div>
                </div>
            </div>
        </a>
    </div>
    <div class="col-lg-3 col-md-6">
        <a href="<?= Url::to(['/order/laporan'])?>">
            <div class="panel panel-yellow">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-xs-3">
                            <i class="fa fa-shopping-cart fa-5x"></i>
                        </div>
                        <div class="col-xs-9 text-right">
                            <div>Daftar Order</div>
                        </div>
                    </div>
                </div>
            </div>
        </a>
    </div>
    <div class="col-lg-3 col-md-6">
        <a href="<?= Url::to(['/transaksi/laporan','tipe_transaksi' => 'Keluar'])?>">
            <div class="panel panel-red">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-xs-3">
                            <i class="fa fa-support fa-5x"></i>
                        </div>
                        <div class="col-xs-9 text-right">
                            <div>Pengeluaran</div>
                        </div>
                    </div>
                </div>
            </div>
        </a>
    </div>
    <div class="col-lg-3 col-md-6">
        <a href="<?= Url::to(['/laporan/labarugi'])?>">
            <div class="panel panel-info">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-xs-3">
                            <i class="fa fa-archive fa-5x"></i>
                        </div>
                        <div class="col-xs-9 text-right">
                            <div>LabaRugi</div>
                        </div>
                    </div>
                </div>
            </div>
        </a>
    </div>
</div>
