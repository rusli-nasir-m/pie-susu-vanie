<?php
/**
 * Created by PhpStorm.
 * User: ASUS
 * Date: 20/04/2018
 * Time: 23:51
 * Project: pie-susu-vanie
 *
 * @var $this \yii\web\View
 * @var $model \admin\models\LabaRugi
 * @var $labarugi \common\models\JurnalDetail
 */

use yii\bootstrap\ActiveForm;
use yii\bootstrap\Html;

$js = <<<JS
    $('#btn-print').on('click',function() {
    window.print();
    });
    $('#labarugi-dari_tanggal').datepicker({
  autoclose: true,
  format: 'yyyy-mm-dd'
});
    $('#labarugi-sampai_tanggal').datepicker({
  autoclose: true,
  format: 'yyyy-mm-dd'
});
JS;
$this->registerJs($js);
?>

    <h1 class="text-center">Laporan Laba Rugi</h1>
<p class="text-center visible-print" style="display: none;">Dari <?= formatter()->asDate($model->dari_tanggal)?> S/d <?= formatter()->asDate($model->sampai_tanggal)?></p>
    <div class="hidden-print">
        <?php $form = ActiveForm::begin([
            'options' => ['enctype' => 'multipart/form-data'],
        ]); ?>
        <div class="row">
            <div class="col-md-6">
                <?= $form->field($model, 'dari_tanggal') ?>
                <?= $form->field($model, 'sampai_tanggal') ?>
                <div class="form-group">
                    <?= Html::submitButton(Yii::t('app', 'Cari'), ['class' => 'btn btn-success']) ?>
                </div>
            </div>
        </div>

        <?php ActiveForm::end(); ?>
        <p>
            <?= \yii\bootstrap\Html::button('Cetak',['class' => 'btn btn-info','id' => 'btn-print'])?>
            <?= a('Kembali',['index'],['class' => 'btn btn-warning'])?>
        </p>
    </div>
<table class="table">
    <?php
    $totalDebit = 0;
    $totalKredit = 0;
    $klasifikasi = \common\models\AkunKlasifikasi::find()->where(['in','klasifikasi',['D','F']])->all();
    if($klasifikasi){
        foreach ($klasifikasi as $klas){
            $subTotalDebit = 0;
            $subTotalKredit = 0;
            ?>
            <tr>
                <td colspan="4"><b><?= $klas->deskripsi?></b></td>
            </tr>
            <?php
            if($labarugi){
                foreach ($labarugi as $key => $val){					
				
                    $akunKlas = $val->kode_akun ? $val->akun->klasifikasi:null;
                    if($klas->klasifikasi == $akunKlas){
                        ?>
                        <tr>
                            <td class="text-center"></td>
                            <td colspan="2"><?= $val->akun->nama_rekening?></td>
                            <?php

                            switch ($klas->normal){
                                case 'Debit':
                                    $subTotalDebit += $val->debit;
                                    ?><td class="text-right"><?= formatter()->asCurrency($val->debit)?></td><?php
                                    break;
                                case 'Kredit':
                                    $subTotalKredit += $val->credit;
                                    ?><td class="text-right"><?= formatter()->asCurrency($val->credit)?></td><?php
                                    break;

                            }
                            ?>
                        </tr>
                        <?php
                    }
                }
                ?>
                <tr>
                    <td colspan="3" class="text-right text-uppercase">Total <?= $klas->deskripsi?></td>
                    <?php
                    switch ($klas->normal){
                        case 'Debit':

                            ?><td class="text-right"><?= formatter()->asCurrency($subTotalDebit)?></td><?php
                            break;
                        case 'Kredit':

                            ?><td class="text-right"><?= formatter()->asCurrency($subTotalKredit)?></td><?php
                            break;
                    }

                    $totalDebit += $subTotalDebit;
                    $totalKredit += $subTotalKredit;
                    ?>
                </tr>
                <?php
            }else{
                echo "<tr>
                                    <td colspan='10' class=\"text-center\">Tentukan periode terlebih dahulu</td>
                             </tr>";
            }

        }
        $total = $totalKredit - $totalDebit;
    }

    ?>
    <tr>
        <td colspan="3" class="text-right"><b>Laba Rugi Bersih</b></td>
        <td class="text-right"><b><?= formatter()->asCurrency($total) ?></b></td>
    </tr>
</table>
