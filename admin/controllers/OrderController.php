<?php

namespace admin\controllers;

use common\models\DetailOrder;
use common\models\Jurnal;
use common\models\JurnalDetail;
use common\models\Transaksi;
use Yii;
use common\models\Order;
use admin\models\OrderSearch;
use yii\base\ErrorException;
use yii\bootstrap\Html;
use yii\db\Exception;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * OrderController implements the CRUD actions for Order model.
 */
class OrderController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Order models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new OrderSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Order model.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Order model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Order();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id_order]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Order model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id_order]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    public function actionValidasi($id)
    {
        $model = $this->findModel($id);
        $model->scenario = $model::SCENARIO_VALIDASI;
        $model->validasi = 1;
        $model->tanggal_validasi = date('Y-m-d');

        $transaction = Yii::$app->db->beginTransaction();
        if ($model->status_pembayaran && $model->save()) {
            try{
                $trans = new Transaksi();
                $trans->tanggal = $model->tanggal_validasi;
                $trans->keterangan_transaksi = 'Validasi Order ' . $model->id_order;
                $trans->total_transaksi = $model->total_harga;
                $trans->status = 'Lunas';
                $trans->tipe_transaksi = 'Masuk';
                $trans->no_bukti = $model->id_order;
                if ($trans->save()){
                    $journal = new Jurnal();
                    $journal->id_transaksi = $trans->id_transaksi;
                    $journal->keterangan = $trans->keterangan_transaksi;
                    $journal->tanggal = $model->tanggal_validasi;
                    if($journal->save()){
                        Yii::$app->db->createCommand()->batchInsert(JurnalDetail::tableName(),['id_jurnal','kode_akun','debit','credit'],[
                            [$journal->id,'4.1',0,$model->total_harga],
                            [$journal->id,'1.1',$model->total_harga,0],
                        ])->execute();
                        $transaction->commit();
                    }else{
                        $transaction->rollBack();
                        echo Html::errorSummary($journal);
                        die();
                    }
                }else{
                    $transaction->rollBack();
                    echo Html::errorSummary($trans);
                    die();
                }
            }catch (\Exception $x ){
                $transaction->rollBack();
                throw new NotFoundHttpException($x->getMessage());
            }

            return $this->redirect(['view', 'id' => $model->id_order]);
        }else{
            echo Html::errorSummary($model);
            $transaction->rollBack();
            die();
        }
    }

    /**
     * Deletes an existing Order model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $orderIsValidasi = Order::find()->isValidasi()->byID($id)->count();
        if($orderIsValidasi >=1){
            Yii::$app->session->setFlash('error','Maaf Order tidak dapat dibatalkan karena sudah di validasi');
        }else{
            DetailOrder::findOne(['id_order'=>$id])->delete();
            $this->findModel($id)->delete();
        }

        return $this->redirect(['index']);
    }

    /**
     * Public function actionLaporan() For
     **/
    public function actionLaporan()
    {
        $searchModel = new OrderSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->post());
        $dataProvider->pagination = false;
        $dataProvider->sort = false;
        return $this->render('laporan', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Finds the Order model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return Order the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Order::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }
}
