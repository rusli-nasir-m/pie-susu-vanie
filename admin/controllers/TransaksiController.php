<?php

namespace admin\controllers;

use admin\models\TransaksiSearch;
use common\models\Jurnal;
use common\models\JurnalDetail;
use common\models\Transaksi;
use Yii;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * UserController implements the CRUD actions for User model.
 */
class TransaksiController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function actionIndex()
    {
        $searchModel = new TransaksiSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionPengeluaran()
    {
        $searchModel = new TransaksiSearch();
        $searchModel->tipe_transaksi = $searchModel::TIPE_KELUAR;
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }



    /**
     * Displays a single Transaksi model.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }
    /**
     * Creates a new Transaksi model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($type = 'Keluar')
    {
        $model = new Transaksi();
        $model->tipe_transaksi = $type;
        $model->status = 'Lunas';
        $transaction = Yii::$app->db->beginTransaction();
        try {
            if ($model->load(Yii::$app->request->post()) && $model->save()) {

                $journal = new Jurnal();
                $journal->id_transaksi = $model->id_transaksi;
                $journal->keterangan = $model->keterangan_transaksi;
                $journal->tanggal = $model->tanggal;
                if ($journal->save()) {
                    Yii::$app->db->createCommand()->batchInsert(JurnalDetail::tableName(), ['id_jurnal', 'kode_akun', 'debit', 'credit'], [
                        [$journal->id, '5.5', $model->total_transaksi,0],
                        [$journal->id, '1.1',0 , $model->total_transaksi],
                    ])->execute();

                    $transaction->commit();
                }

                return $this->redirect(['view', 'id' => $model->id_transaksi]);
            }
        }catch (\Exception $x ){
            $transaction->rollBack();
        }
        return $this->render('create', [
            'model' => $model,
        ]);
    }
    /**
     * Updates an existing Transaksi model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id_transaksi]);
        }
        return $this->render('update', [
            'model' => $model,
        ]);
    }
    /**
     * Deletes an existing Transaksi model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();
        return $this->redirect(['index']);
    }

    /**
     * Public function actionLaporan() For
     **/
    public function actionLaporan($tipe_transaksi = null)
    {
        $searchModel = new TransaksiSearch();
        $searchModel->tipe_transaksi = $tipe_transaksi;
        $dataProvider = $searchModel->search(Yii::$app->request->post());
        $dataProvider->sort = false;
        $dataProvider->pagination = false;
        return $this->render('laporan', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Finds the Transaksi model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return Transaksi the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Transaksi::findOne($id)) !== null) {
            return $model;
        }
        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }
    
}
