<?php
namespace admin\controllers;

use admin\models\LoginForm;
use common\models\Order;
use common\models\Pelanggan;
use Yii;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;


/**
 * Site controller
 */
class SiteController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['login', 'error'],
                        'allow' => true,
                    ],
                    [
                        'actions' => ['logout', 'index'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        $dataChart = [];

        for ($y = date('Y')-1;$y <= date('Y');$y++){
            for ($i = 1;$i<=12;$i++){
                $countPelanggan = Pelanggan::find()->where('DATE_FORMAT(tgl_registrasi,\'%Y-%c\')=:tanggal',[':tanggal' => $y . '-' .$i])->count();
                $countOrder = Order::find()->where('DATE_FORMAT(tgl_order,\'%Y-%c\')=:tanggal',[':tanggal' => $y . '-' .$i])->count();
                $countOrderValidate = Order::find()->where('DATE_FORMAT(tanggal_validasi,\'%Y-%c\')=:tanggal',[':tanggal' => $y . '-' .$i])->count();
                $dataChart[]= [
                    'period' => $y . '-' . $i,
                    'pelanggan' => $countPelanggan,
                    'order' => $countOrder,
                    'pembayaran'=> $countOrderValidate
                ];
            }
        }

        $countPelanggan = Pelanggan::find()->where('DATE_FORMAT(tgl_registrasi,\'%Y\')=:tanggal',[':tanggal' => date('Y')])->count();
        $countOrder = Order::find()->where('DATE_FORMAT(tgl_order,\'%Y\')=:tanggal',[':tanggal' => date('Y')])->count();
        $countOrderValidate = Order::find()->where('DATE_FORMAT(tanggal_validasi,\'%Y\')=:tanggal',[':tanggal' => date('Y')])->count();
        return $this->render('index',[
            'dataChart' =>$dataChart,
            'countPelanggan' => $countPelanggan,
            'countOrder' => $countOrder,
            'countOrderValidate' => $countOrderValidate
        ]);
    }

    /**
     * Login action.
     *
     * @return string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        } else {
            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Logout action.
     *
     * @return string
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }
}
