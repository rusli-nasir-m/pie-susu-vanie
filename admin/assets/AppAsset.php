<?php

namespace admin\assets;

use yii\web\AssetBundle;

/**
 * Main backend application asset bundle.
 */
class AppAsset extends AssetBundle
{

    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/sb-admin.css',
        'css/site.css',
        'css/plugins/morris.css',
        'js/plugins/datepicker/datepicker3.css',
        'font-awesome/css/font-awesome.min.css',
    ];
    public $js = [
        'js/plugins/datepicker/bootstrap-datepicker.js',
        'js/plugins/morris/raphael.min.js',
        'js/plugins/morris/morris.min.js',
        'js/app.js'
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapPluginAsset',
    ];
}
